import simplefix
from pyfixtest import FixSessionServer, FixServer

class TestSession(FixSessionServer):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket):
        FixSessionServer.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket)


class TestServer(FixServer):
    def __init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id):
        FixServer.__init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id)

    def create_new_server_session(self, socket):
        session = TestSession( self._fix_version_string, self._sender_comp_id, self._target_comp_id, socket)
        session.set_expected_tag_in_new_orders_for_fills('44', '1999')
        session.set_highlighted_tags([44, 11])
        return session

server = TestServer( '127.0.0.1', 1234, 'FIX.4.2', 'SENDER', 'TARGET')
server.start()


