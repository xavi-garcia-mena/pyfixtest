#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import FixServer,FixSessionServer
import simplefix


# this class will specialize some of the methods in FixSessionServer
class ExampleSessionServer(FixSessionServer):
    def __init__(self, fix_version_str, sender_comp_id, target_comp_id, client_socket, outgoing_seq_num):
        super(ExampleSessionServer, self).__init__(fix_version_str, sender_comp_id, target_comp_id, client_socket, outgoing_seq_num)

    # ACKs and FILLS ENRICHMENT -----------------------------------------------------------------------------
    #
    # we are going to enrich the ack message
    def get_ack_message(self, order):
        # get the one implemented in the base class
        ack = super(ExampleSessionServer, self).get_ack_message(order)

        # and add some extra tags
        ack.append_pair('8013', '8013_test')
        ack.append_pair('8014', '8014_test')

        return ack

    def get_full_fill_message(self, order):
        # get the one implemented in the base class
        fill = super(ExampleSessionServer, self).get_full_fill_message(order)

        # and add some extra tags
        fill.append_pair('12345', '12345_test')
        fill.append_pair('1999', '1999_test')

        return fill

    # PROCESS SPECIFIC MESSAGE FOR A SESSION  ----------------------------------------------------------------
    #
    def get_quote_reply(self):
        # returns a basic message with sender_comp_id, target_comp_it, begin_string set
        reply = self.get_msg_header_message()
        reply.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE_RESPONSE) # msgtype = AJ
        reply.append_pair('693', 'QUOTE_ID') # tag 693 = QUOTE_ID
        reply.append_pair('694', '6') # tag 694 = 6
        return reply

    def on_quote(self):
        reply = self.get_quote_reply()
        self.send_message(reply)

    def process_message(self, msg):
        msgtype = msg.get(simplefix.TAG_MSGTYPE)
        if msgtype == simplefix.MSGTYPE_QUOTE:
            self.on_quote()
            return True # we return True to flag this message as processed
        else:
            # if the message is not handled by this session let's process in the base class
            return super(ExampleSessionServer, self).process_message(msg)


class ExampleServer(FixServer):
    def __init__(self, ip, port, fix_version_str, sender_comp_id, target_comp_id):
        super(ExampleServer, self).__init__(ip, port, fix_version_str, sender_comp_id, target_comp_id)
        self.set_fix_session_class(ExampleSessionServer)

# uncomment the following lines to run the server
# server = ExampleServer('127.0.0.1', 12345, 'FIX.4.2', 'Serder', 'Target')
# server.start()

