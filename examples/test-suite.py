import simplefix
import socket
import sys
from pyfixtest import TestCase, TestCaseUtils, TestSuite, FixSessionClient, HtmlLogger

def swap_comp_ids(message):
  sender = message.get(simplefix.TAG_SENDER_COMPID)
  target = message.get(simplefix.TAG_TARGET_COMPID)
  message.remove(simplefix.TAG_SENDER_COMPID)
  message.remove(simplefix.TAG_TARGET_COMPID)
  message.append_pair(simplefix.TAG_SENDER_COMPID, target, header=True)
  message.append_pair(simplefix.TAG_TARGET_COMPID, sender, header=True)
  return message


# Create a HTML logger
# Important the Third parameter is the definition of how the report should be generated.
# But pyfixtest is not using it. 
# It's a json file describing the tags to be shown in the report and used for other tools that use
# pyfixtest as their base engine.
# For this example, we just pass a basic definition (provided with the example)
logger = HtmlLogger('./test.log', './test.html', 'examples/html_logger_definition.json')

# Connect to the server
client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
  client_sock.connect(('127.0.0.1', 1234))
except:
  # CHANGE THIS TO WHATEVER YOU NEED TO DO IN CASE OF CONNECTION ERROR
  print('***** CONNECTION ERROR *****\n')
  sys.exit(2)
print('Connected')

# Create the client session, passing the socket
session = FixSessionClient('FIX.4.2', 'TARGET', 'SENDER', client_sock)

# Using pyfixtest get_logon_message
logon = session.get_logon_message()

logon_response = session.get_msg_header_message()
logon_reponse = swap_comp_ids(logon_response)
logon_response.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON, header=True)

# Create the TestCase... passing the logger (HTMLLogger), otherwise defaults to StdOutLogger
test_case_logon = TestCase('logon', logon, [logon_response], True, session, logger)

# We use TestCaseUtils to parse raw fix messages
utils = TestCaseUtils()

# Create new order single test case
new_order_1_raw = b'8=FIX.4.2|9=173|35=D|49=TARGET|56=SENDER|34=2|52=20211209-09:39:49.947|1=test_1|11=20211209-09:39:49.947410|21=1|54=1|44=200|38=1000|45=VOD.L|55=VOD.L|40=2|59=3|60=20211209-09:39:49.947453|10=141|'
new_order_1 = utils.parse_fix_message_from_raw(new_order_1_raw, True, True)

# We're just going to check for a few tags
# 150=0
# 39=0
# 11=order's 11
# 54=order's 54
ack_1 = simplefix.FixMessage()
ack_1.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EXECUTION_REPORT)
ack_1.append_pair('150', '0')
ack_1.append_pair('39', '4') # This will report an error!!
ack_1.append_pair('11', new_order_1.get('11'))
ack_1.append_pair('54', new_order_1.get('54'))


# Create the TestCase... passing the logger (HTMLLogger), otherwise defaults to StdOutLogger
test_case_new_order_ack = TestCase('new-order-test', new_order_1, [ack_1], True, session, logger)

# Create the TestSuite (Passing the HTMLLogger instance)
suite = TestSuite('test-suite-example', logger)
suite.add_test(test_case_logon)
suite.add_test(test_case_new_order_ack)
suite.run()

