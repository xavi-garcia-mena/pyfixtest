from pyfixtest import FixClient, FixSessionClient

class TestClientSession(FixSessionClient):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket):
        FixSessionClient.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket)
        self.set_verify_logon_message(True)
        self.set_highlighted_tags([11, 44])

client = FixClient('127.0.0.1', 1234, 'FIX.4.2', 'TARGET', 'SENDER')
client.set_client_session_class(TestClientSession)

client.start()

