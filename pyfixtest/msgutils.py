#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import simplefix
import sys
import re

class bcolors:
    HEADER = b'\033[95m'
    OKBLUE = b'\033[94m'
    OKGREEN = b'\033[92m'
    WARNING = b'\033[93m'
    FAIL = b'\033[91m'
    ENDC = b'\033[0m'
    BOLD = b'\033[1m'
    UNDERLINE = b'\033[4m'

def fix_val(value): #pragma nocover
    # not covering this function because it's just a copy/paste from simplefix
    """Make a FIX value from a string, bytes, or number."""
    if type(value) == bytes:
        return value

    if sys.version_info[0] == 2: 
        return bytes(value)
    elif type(value) == str:
        return bytes(value, 'UTF-8')
    else:
        return bytes(str(value), 'ASCII')

def get_soh_str():
    return b'\x01'

def get_soh_byte():
    return b'\x01'

class FixMessageWithHighlightedTags(simplefix.FixMessage):
    def get_highlighted_encoding(self, highlighted_tags):
        """Convert message to on-the-wire FIX format.

        :param raw: If True, encode pairs exactly as provided.

        Unless 'raw' is set, this function will calculate and
        correctly set the BodyLength (9) and Checksum (10) fields, and
        ensure that the BeginString (8), Body Length (9), Message Type
        (35) and Checksum (10) fields are in the right positions.

        This function does no further validation of the message content."""

        buf = b""
        buf_highlighted = b""

        # Cooked.
        for tag, value in self.pairs:
            if int(tag) in (8, 9, 35, 10):
                continue
            highlight_start = b''
            highlight_end = b''
            if int(tag) in highlighted_tags:
                highlight_start = bcolors.OKGREEN 
                highlight_end = bcolors.ENDC
            buf_highlighted += highlight_start + tag + b'=' + value + highlight_end + get_soh_str()
            buf += tag + b'=' + value + get_soh_str()

        # Prepend the message type.
        if self.message_type is None:
            raise ValueError("No message type set")

        buf = b"35=" + self.message_type + get_soh_str() + buf
        buf_highlighted = b"35=" + self.message_type + get_soh_str() + buf_highlighted

        # Calculate body length.
        #
        # From first byte after body length field, to the delimiter
        # before the checksum (which shouldn't be there yet).
        body_length = len(buf)

        # Prepend begin-string and body-length.
        if not self.begin_string:
            raise ValueError("No begin string set")

        buf = b"8=" + self.begin_string + get_soh_str() + \
              b"9=" + fix_val("%u" % body_length) + get_soh_str() + \
              buf

        buf_highlighted = b"8=" + self.begin_string + get_soh_str() + \
              b"9=" + fix_val("%u" % body_length) + get_soh_str() + \
              buf_highlighted

        # Calculate and append the checksum.
        checksum = 0
        for c in buf:
            checksum += ord(c) if sys.version_info[0] == 2 else c
        buf += b"10=" + fix_val("%03u" % (checksum % 256,)) + get_soh_str()
        buf_highlighted += b"10=" + fix_val("%03u" % (checksum % 256,)) + get_soh_str()

        return buf_highlighted.replace(get_soh_byte(), b'|')

class BufferFixParser(object):
    def split_messages_in_buffer(self, raw_buffer):
        msgs = []
        i = 0
        while i < len(raw_buffer):
            initial_pos = i
            soh_pos = self._find_next_ending_soh(raw_buffer, initial_pos)
            if soh_pos == -1:
                msgs.append(raw_buffer[initial_pos:])
                break
            raw_message = raw_buffer[initial_pos:soh_pos+1]
            msgs.append(raw_message)
            i = soh_pos + 1
        return msgs


    def is_message_truncated(self, raw_buffer):
        if len(raw_buffer) == 0:
            return False
        expr = re.compile("^.+10=(\\d)+\x01$")
        if expr.match(raw_buffer) != None:
            return False
        return True

    def _find_next_ending_soh(self, raw_buffer, pos):
        checksum_pos = raw_buffer.find(b'\x0110=', pos)
        if checksum_pos == -1:
            return -1
        soh_pos = raw_buffer.find(get_soh_str(), checksum_pos + 1)
        return soh_pos

