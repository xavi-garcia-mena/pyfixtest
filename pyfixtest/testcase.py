#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import select
import simplefix
import socket
import time
from pyfixtest import BufferFixParser
from .logger import StdOutLogger, HtmlLogger

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_print_value(val):
    if type(val) == bytes:
        return val.decode()
    else:
        return val

class TestCaseUtils(object):
    def get_expected_tag_non_existing_value(self):
        return 'pyfixtest$$NONE'

    def get_expected_tag_non_empty(self):
        return 'pyfixtest$$NOT_EMPTY'

    def parse_fix_message_from_raw(self, raw_message, convert_separator_to_soh, remove_tags_34_and_52=False):
        if convert_separator_to_soh:
            raw_message = raw_message.replace(b'|', simplefix.SOH_STR)
        parser = simplefix.FixParser()
        parser.append_buffer(raw_message)
        msg = parser.get_message()
        if msg != None and remove_tags_34_and_52:
            msg.remove(simplefix.TAG_SENDING_TIME)
            msg.remove(simplefix.TAG_MSGSEQNUM)
        return msg


class TestSuite(object):
    def __init__(self, name, logger=StdOutLogger()):
        self._name = name
        self._test_cases = []
        self._generate_new_clordids = False
        self._logger = logger
        self._html_logger = None
        
    def add_test(self, test):
        self._test_cases.append(test)

    def set_generate_new_clordids(self, generate_new_clordids):
        self._generate_new_clordids = generate_new_clordids 

    def run(self, exclude=[]):
        tests_ok = 0
        tests_excluded = 0
        tests_failed = 0
        self._logger.add (self.get_header())
        if isinstance(self._logger, HtmlLogger):
            self._logger.add_suite_header(self._name)
        self._logger
        for test in self._test_cases:
            test.set_generate_new_clordids(self._generate_new_clordids)
            if test._name in exclude:
                self._logger.add (bcolors.BOLD + 'Test %s excluded' % test._name + bcolors.ENDC)
                tests_excluded += 1
            else:
                if test.run():
                    tests_ok += 1
                else:
                    tests_failed += 1
                    if test._abort_on_failure:
                        self._logger.add (bcolors.FAIL + '*** Aborting test suite after fatal test case failed ***' + bcolors.ENDC)
                        break
        self._logger.add (self.get_footer(tests_ok, tests_failed, tests_excluded, len(self._test_cases)))
        if isinstance(self._logger, HtmlLogger):
            self._logger.add_suite_footer()
        if tests_failed > 0:
            return False
        else:
            return True

    def get_header(self):
        return ( bcolors.OKBLUE + 
                '********************************\n' + 
                ' TEST SUITE  %s\n' % self._name + \
                '********************************\n' + 
                bcolors.ENDC 
                )

    def get_footer(self, ok, failed, excluded, total):
        return (bcolors.OKGREEN + 'Tests OK       (%d/%d)' % (ok, total) + bcolors.ENDC + '\n' +
                bcolors.FAIL + 'Tests FAILED   (%d/%d)' % (failed, total) + bcolors.ENDC + '\n' +
                bcolors.OKBLUE + 'Tests EXCLUDED (%d/%d)' % (excluded, total) + '\n' +
                '********************************\n' + bcolors.ENDC  
                )

                
class TestCase(object):
    def __init__(self, name, initial_message, expected_reply, initiate_test, session, logger=StdOutLogger(), timeout=2):
        self._name = name
        self._initial_message = initial_message
        self._expected_reply = expected_reply
        self._session = session
        self._timeout = timeout
        self._initiate_test = initiate_test
        self._utils = TestCaseUtils()
        self._abort_on_failure = False
        self._ignore_tags = [ simplefix.TAG_BODYLENGTH, simplefix.TAG_CHECKSUM ]
        self._generate_new_clordids = False
        self._messages_buffer = []
        self._logger = logger

    def set_abort_on_failure(self, abort_on_failure):
        self._abort_on_failure = abort_on_failure

    def set_ignore_tags(self, ignore_tags):
        self._ignore_tags = []
        for tag in ignore_tags:
            if type(tag) == str or type(tag) == int:
                tag = tag.encode()
                self._ignore_tags.append(tag)

    def set_generate_new_clordids(self, generate_new_clordids):
        self._generate_new_clordids = generate_new_clordids

    def run(self):
        if self._generate_new_clordids:
            self._set_new_clordids()

        self._logger.add (self.get_test_header_string_highlighted())
        if isinstance(self._logger, HtmlLogger):
            self._logger.add_test_name(self._name)
        if (self._initiate_test):
            result = self._run_active()
        else:
            result = self._run_passive() #pragma: no cover

        self._logger.add (self.get_test_footer_string_highlighted(result))
        if isinstance(self._logger, HtmlLogger):
            if result:
                self._logger.add_ok_result()
            else:
                self._logger.add_error_result()
        return result
    
    def get_test_header_string_highlighted(self):
        return ( bcolors.OKBLUE + 
                '--------------------------------\n' + 
                ' TEST %s\n' % self._name + \
                '--------------------------------\n' + 
                bcolors.ENDC 
                )

    def get_test_footer_string_highlighted(self, result):
        ret_string = ( bcolors.OKBLUE +
                        '-------------------------------- ' +
                       bcolors.ENDC
                     )
        if result:
            return ret_string + self.get_ok_string_highlighted()
        else:
            return ret_string + self.get_error_string_highlighted()

    def get_ok_string_highlighted(self):
        return bcolors.OKGREEN + 'OK' + bcolors.ENDC

    def get_error_string_highlighted(self):
        return bcolors.FAIL + 'ERROR' + bcolors.ENDC

    def get_expected_tag_non_existing_value(self):
        return self._utils.get_expected_tag_non_existing_value()

    def get_expected_tag_non_empty(self):
        return self._utils.get_expected_tag_non_empty()

    def _get_test_str_header(self):
        return 'TEST --%s--' % (self._name)

    def _run_active(self):
        if isinstance(self._logger, HtmlLogger):
            self._logger.begin_test_log()
            self._logger.add_sent_message(self._initial_message)
        sent_message = self._session.send_message(self._initial_message)
        result = True
        for reply in self._expected_reply:
            msg = self._wait_for_message()
            if msg is None:
                self._logger.add ('%s *** ERROR *** expected reply for message %s was not received' % (self._get_test_str_header(), self._session.get_msg_str(self._initial_message)))
                if isinstance(self._logger, HtmlLogger):
                    self._logger.add_error_message("Not received : %s " % self._session.get_msg_str(self._initial_message))
                    self._logger.add_test_messages(False, self._name, self._initial_message, reply, None)
                return False
            result = self._check_messages(reply, msg) and result
            if isinstance(self._logger, HtmlLogger):
                if msg != None:
                    self._logger.add_test_messages(result, self._name, self._initial_message, reply, msg)

        if isinstance(self._logger, HtmlLogger):
            self._logger.end_test_log()
        return result

    def _run_passive(self):
        msg = self._wait_for_message() #pragma: no cover
        if msg is None: #pragma: no cover
            self._logger.add ('%s *** ERROR *** waiting for message %s' % (self._get_test_str_header(), self._session.get_msg_str(self._initial_message))) #pragma: no cover
            return False #pragma: no cover

    def _check_tag_equal(self, expected, actual):
        if expected[1] != actual[1]:
            self._logger.add ('%s *** ERROR *** value mismatch for tag %s: expected value [%s], actual is [%s]' % (self._get_test_str_header(), get_print_value(expected[0]), get_print_value(expected[1]), get_print_value(actual[1])))
            if isinstance(self._logger, HtmlLogger):
                self._logger.add_error_message("<b>Value mismatch for tag %s</b>: expected value [%s], actual is [%s]" % (expected[0], expected[1], actual[1]))
            return False
        return True

    def _check_tag_not_empty(self, expected, actual):
        if actual[1] is None or len(actual[1]) == 0:
            self._logger.add ('%s *** ERROR *** value mismatch for tag %s: expected non empty value' % (self._get_test_str_header(), get_print_value(expected[0])))
            if isinstance(self._logger, HtmlLogger):
                self._logger.add_error_message("<b>Value mismatch for tag %s</b>: expected non empty value" % expected[0])
            return False
        return True

    def _check_tag_does_not_exist(self, expected, actual):
        if actual[1] != None:
            self._logger.add ('%s *** ERROR *** value mismatch for tag %s: expected non existing tag has value [%s]' % (self._get_test_str_header(), get_print_value(expected[0]), get_print_value(actual[1])))
            if isinstance(self._logger, HtmlLogger):
                self._logger.add_error_message("<b>Value mismatch for tag %s</b>: expected non existing tag has value [%s]" % (expected[0], actual[1]))
            return False
        return True

    def _check_tag(self, expected, actual):
        expected_value = expected[1]
        if type(expected[1]) == bytes:
            expected_value = expected[1].decode()
        if expected_value == self.get_expected_tag_non_existing_value():
            return self._check_tag_does_not_exist(expected, actual)
        elif expected_value == self.get_expected_tag_non_empty():
            return self._check_tag_not_empty(expected, actual)
        else:
            return self._check_tag_equal(expected, actual)

    def _check_messages(self, expected, actual):
        result = True
        for expected_tag in expected.pairs:
            if expected_tag[0] not in self._ignore_tags:
                actual_value = actual.get(expected_tag[0])
                if not self._check_tag(expected_tag, (expected_tag[0], actual_value)):
                    result = False
        return result

    def _parse_raw_message(self, raw_msg):
        parser = simplefix.FixParser()
        parser.append_buffer(raw_msg)
        msg = parser.get_message()
        return msg

    def _wait_for_message(self):
        buffer_parser = BufferFixParser()
        if len(self._messages_buffer) == 0:
            # no messages buffered, read from the socket
            ready_to_read, _, _ = select.select([self._session.get_socket()], [], [], self._timeout)
            if len(ready_to_read) == 0:
                return None
            raw_buffer = self._session.read_buffer()
            self._messages_buffer = buffer_parser.split_messages_in_buffer(raw_buffer)
        raw_msg = self._messages_buffer.pop(0)
        msg = self._parse_raw_message(raw_msg)
        if msg != None:
            if isinstance(self._logger, HtmlLogger):
                self._logger.add_received_message('%s' % self._session.get_msg_str(msg).decode())
            self._logger.add ('RECV: %s    %s' % (msg.get(simplefix.TAG_MSGTYPE).decode(), self._session.get_message_highlighted_string(msg).decode()))
        else:
            if isinstance(self._logger, HtmlLogger):
                self._logger.add_received_message('%s' % raw_msg)
            self._logger.add ('RECV: %s' % raw_msg)

        return msg

    def _set_new_clordids(self):
        if self._initial_message.get(simplefix.TAG_CLORDID) != None:
            self._initial_message.remove(simplefix.TAG_CLORDID)
        self._initial_message.append_utc_timestamp(simplefix.TAG_CLORDID, precision=6)
        for msg in self._expected_reply:
            if msg.get(simplefix.TAG_CLORDID) != None:
                msg.remove(simplefix.TAG_CLORDID)
                msg.append_pair(simplefix.TAG_CLORDID, self._initial_message.get(simplefix.TAG_CLORDID))

class TestCaseServerEcho(TestCase):
    def __init__(self, name, initial_message, expected_reply, port, session, logger=StdOutLogger(), timeout=2):
        super(TestCaseServerEcho, self).__init__(name, initial_message, expected_reply, True, session, timeout)
        self._test_port = port
        self._logger = logger

    def _run_active(self):
        self._test_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._test_socket.bind(('127.0.0.1', self._test_port))
        result = super(TestCaseServerEcho, self)._run_active()
        self._test_socket.close()
        self._read_tcp_data()
        return result

    def _read_tcp_data(self):
        # wait half a second to let the server send all of it's replies
        time.sleep(0.5)
        # finally read the socket so next test doesn't read it
        ready_to_read, _, _ = select.select([self._session.get_socket()], [], [], self._timeout)
        if len(ready_to_read) != 0:
            self._session.read_buffer()
        
    def _wait_for_message(self):
        self._test_socket.settimeout(self._timeout)
        try:
            ( reply, _ ) = self._test_socket.recvfrom(4096)
            msg = self._parse_raw_message(reply)
            if msg != None:
                self._logger.add ('TEST SOCKET RECV: %s    %s' % (msg.get(simplefix.TAG_MSGTYPE).decode(), self._session.get_message_highlighted_string(msg).decode()))
            else:
                self._logger.add ('TEST SOCKET RECV: %s' % reply)
            return msg
        except socket.timeout:
            return None


