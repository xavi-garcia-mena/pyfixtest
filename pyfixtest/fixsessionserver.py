#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from .fixsessionbase import FixSessionBase, FixSessionHandler
from .logger import StdOutLogger
import simplefix
import socket
import threading
import time
import select
from .msgutils import BufferFixParser

class FixSessionServer(FixSessionBase):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        super(FixSessionServer, self).__init__(fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)
        self._open_orders = []
        self._test_socket = None
        self._test_port = None

    def set_test_socket(self, socket, port):
        self._test_socket = socket
        self._test_port = port

    def _get_execution_report_common_tags(self, order):
        er = super(FixSessionServer, self).get_msg_header_message()
        er.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EXECUTION_REPORT)
        er.append_pair(simplefix.TAG_ORDERID, 'tag37-%s' % self._outgoing_seq_num)
        er.append_pair(simplefix.TAG_EXECID, 'tag17-%s' % self._outgoing_seq_num)
        er.append_pair(simplefix.TAG_EXECTRANSTYPE, '0') # tag 20
        er.append_pair(simplefix.TAG_SYMBOL, order.get(simplefix.TAG_SYMBOL)) # tag 55
        er.append_pair(simplefix.TAG_SIDE, order.get(simplefix.TAG_SIDE)) # tag 54
        er.append_pair(simplefix.TAG_CLORDID, order.get(simplefix.TAG_CLORDID)) # tag 11
        return er

    def _get_default_ack_message(self, order):
        ack = self._get_execution_report_common_tags(order)
        ack.append_pair(simplefix.TAG_EXECTYPE, '0') # tag 150
        ack.append_pair(simplefix.TAG_ORDSTATUS, '0') # tag 39
        ack.append_pair('151', order.get(simplefix.TAG_ORDERQTY)) 
        ack.append_pair(simplefix.TAG_CUMQTY, '0')
        ack.append_pair(simplefix.TAG_AVGPX, '0')
        return ack

    def get_ack_message(self, order):
        if self._on_ack_reply_tags is None:
            return self._get_default_ack_message(order)
        else:
            if self._on_ack_reply_tags_enhance_default:
                ack = self._get_default_ack_message(order)
            else:
                ack = super(FixSessionServer, self).get_msg_header_message()
            ack = self._fill_tags_from_reply(self._on_ack_reply_tags, ack, order)
            return ack

    def _get_default_full_fill_message(self, order):
        fill = self._get_execution_report_common_tags(order)
        if self._fix_version_string == 'FIX.4.2' or self._fix_version_string == b'FIX.4.2':
            fill.append_pair(simplefix.TAG_EXECTYPE, '2') # tag 150
        else:
            fill.append_pair(simplefix.TAG_EXECTYPE, 'F') # tag 150
        fill.append_pair(simplefix.TAG_ORDSTATUS, '2') # tag 39
        fill.append_pair('151', '0') 
        fill.append_pair(simplefix.TAG_CUMQTY, order.get(simplefix.TAG_ORDERQTY))
        fill.append_pair(simplefix.TAG_LASTQTY, order.get(simplefix.TAG_ORDERQTY))
        fill.append_pair(simplefix.TAG_AVGPX, order.get(simplefix.TAG_PRICE))
        return fill

    def get_full_fill_message(self, order):
        if self._on_fill_reply_tags is None:
            return self._get_default_full_fill_message(order)
        else:
            if self._on_fill_reply_tags_enhance_default:
                fill = self._get_default_full_fill_message(order)
            else:
                fill = super(FixSessionServer, self).get_msg_header_message()
            fill = self._fill_tags_from_reply(self._on_fill_reply_tags, fill, order)
            return fill

    def _get_default_quote_full_fill_message(self, quote, side):
        fill = super(FixSessionServer, self).get_msg_header_message()
        fill.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EXECUTION_REPORT)
        fill.append_pair(simplefix.TAG_ORDERID, 'tag37-%s' % self._outgoing_seq_num)
        fill.append_pair(simplefix.TAG_EXECID, 'tag17-%s' % self._outgoing_seq_num)
        fill.append_pair(simplefix.TAG_EXECTRANSTYPE, '0') # tag 20
        fill.append_pair(simplefix.TAG_SYMBOL, quote.get(simplefix.TAG_SYMBOL)) # tag 55
        fill.append_pair(simplefix.TAG_SIDE, '%s' % side)
        fill.append_pair(simplefix.TAG_CLORDID, quote.get(simplefix.TAG_QUOTEID)) # tag 11
        fill.append_pair(simplefix.TAG_EXECTYPE, '2') # tag 150
        fill.append_pair(simplefix.TAG_ORDSTATUS, '2') # tag 39
        fill.append_pair('151', '0') 
        if side == 1 or side == '1':
            fill.append_pair(simplefix.TAG_LASTPX, quote.get('132'))
            fill.append_pair(simplefix.TAG_PRICE, quote.get('132'))
            fill.append_pair(simplefix.TAG_AVGPX, quote.get('132'))
            fill.append_pair(simplefix.TAG_LASTQTY, quote.get('134'))
            fill.append_pair(simplefix.TAG_CUMQTY, quote.get('134'))
            fill.append_pair(simplefix.TAG_ORDERQTY, quote.get('134'))
        else:
            fill.append_pair(simplefix.TAG_LASTPX, quote.get('133'))
            fill.append_pair(simplefix.TAG_PRICE, quote.get('133'))
            fill.append_pair(simplefix.TAG_AVGPX, quote.get('133'))
            fill.append_pair(simplefix.TAG_LASTQTY, quote.get('135'))
            fill.append_pair(simplefix.TAG_CUMQTY, quote.get('135'))
            fill.append_pair(simplefix.TAG_ORDERQTY, quote.get('135'))
        return fill

    def get_quote_full_fill_message(self, quote, side):
        if side != 1 and side != '1' and side != 2 and side != '2' and side != None:
            raise ValueError('ERROR: Quote side [%s] is not valid' % side)
        if self._on_quote_fill_reply_tags is None:
            return self._get_default_quote_full_fill_message(quote, side)
        else:
            if self._on_quote_fill_reply_tags_enhance_default:
                fill = self._get_default_quote_full_fill_message(quote, side)
            else:
                fill = super(FixSessionServer, self).get_msg_header_message()
            fill = self._fill_tags_from_reply(self._on_quote_fill_reply_tags, fill, quote)
            return fill
  
    def _get_default_cancel_accept_message(self, order, request):
        cancel = self._get_execution_report_common_tags(order)
        cancel.append_pair(simplefix.TAG_EXECTYPE, '4') # tag 150
        cancel.append_pair(simplefix.TAG_ORDSTATUS, '4') # tag 39
        cancel.append_pair('151', order.get(simplefix.TAG_ORDERQTY)) 
        cancel.append_pair(simplefix.TAG_CUMQTY, '0')
        cancel.append_pair(simplefix.TAG_AVGPX, '0')
        cancel.remove(simplefix.TAG_CLORDID)
        cancel.append_pair(simplefix.TAG_CLORDID, request.get(simplefix.TAG_CLORDID))
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, order.get(simplefix.TAG_CLORDID))
        return cancel

    def get_cancel_accept_message(self, order, request):
        if self._on_cancel_reply_tags is None:
            return self._get_default_cancel_accept_message(order, request)
        else:
            if self._on_cancel_reply_tags_enhance_default:
                cancel = self._get_default_cancel_accept_message(order, request)
            else:
                cancel = super(FixSessionServer, self).get_msg_header_message()
            cancel = self._fill_tags_from_reply(self._on_cancel_reply_tags, cancel, order)
            return cancel

    def _get_default_replace_accept_message(self, order, request):
        replace = self._get_execution_report_common_tags(order)
        replace.append_pair(simplefix.TAG_EXECTYPE, '5') # tag 150
        replace.append_pair(simplefix.TAG_ORDSTATUS, '5') # tag 39
        replace.append_pair('151', order.get(simplefix.TAG_ORDERQTY)) 
        replace.append_pair(simplefix.TAG_CUMQTY, '0')
        replace.append_pair(simplefix.TAG_AVGPX, '0')
        replace.remove(simplefix.TAG_CLORDID)
        replace.append_pair(simplefix.TAG_CLORDID, request.get(simplefix.TAG_CLORDID))
        replace.append_pair(simplefix.TAG_ORIGCLORDID, order.get(simplefix.TAG_CLORDID))
        return replace

    def get_replace_accept_message(self, order, request):
        if self._on_replace_reply_tags is None:
            return self._get_default_replace_accept_message(order, request)
        else:
            if self._on_replace_reply_tags_enhance_default:
                replace = self._get_default_replace_accept_message(order, request)
            else:
                replace = super(FixSessionServer, self).get_msg_header_message()
            replace = self._fill_tags_from_reply(self._on_replace_reply_tags, replace, order)
            return replace

    def get_cancel_reject_message(self, request):
        reject = self.get_msg_header_message()
        reject.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REJECT)
        reject.append_pair(simplefix.TAG_ORDERID, 'NONE')
        reject.append_pair(simplefix.TAG_CLORDID, request.get(simplefix.TAG_CLORDID)) # tag 11
        reject.append_pair(simplefix.TAG_ORIGCLORDID, request.get(simplefix.TAG_ORIGCLORDID)) # tag 41
        reject.append_pair(simplefix.TAG_ORDSTATUS, '8')
        if request.get(simplefix.TAG_MSGTYPE) == b'F':
            reject.append_pair('434', '1')
        else:
            reject.append_pair('434', '2')
        return reject

    def _get_default_quote_status_report_message(self, quote):
        ai = self.get_msg_header_message()
        ai.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE_STATUS_REPORT)
        ai.append_pair(simplefix.TAG_QUOTEID, quote.get(simplefix.TAG_QUOTEID))
        ai.append_pair(simplefix.TAG_SYMBOL, quote.get(simplefix.TAG_SYMBOL))
        ai.append_pair(simplefix.TAG_SECURITY_ID, quote.get(simplefix.TAG_SECURITY_ID))
        ai.append_utc_timestamp(simplefix.TAG_TRANSACTTIME, precision=6)
        ai.append_pair(simplefix.TAG_ACCOUNT, quote.get(simplefix.TAG_ACCOUNT))
        ai.append_pair(simplefix.TAG_CURRENCY, quote.get(simplefix.TAG_CURRENCY))
        ai.append_pair('297', '0') # QuoteStatus = Accepted
        return ai

    def get_quote_status_report_message(self, quote):
        if self._on_quote_ack_reply_tags is None:
            return self._get_default_quote_status_report_message(quote)
        else:
            if self._on_quote_ack_reply_tags_enhance_default:
                ai = self._get_default_quote_status_report_message(quote)
            else:
                ai = super(FixSessionServer, self).get_msg_header_message()
            ai = self._fill_tags_from_reply(self._on_quote_ack_reply_tags, ai, quote)
            return ai

    def on_logon(self):
        logon_reply = self.get_logon_message()
        self.send_message(logon_reply)

    def on_new_order_single(self, order):
        ack = self.get_ack_message(order)
        self.send_message(ack)
        if self._expected_tag_value_for_fills != None and self._has_expected_tag_value(order, self._expected_tag_value_for_fills):
            fill = self.get_full_fill_message(order)
            self.send_message(fill)
            self._last_cl_order_id = None
        else:
            self._open_orders.append(order)
            self._last_cl_order_id = order.get(simplefix.TAG_CLORDID)
    
    def on_cancel_request(self, request):
        for order in self._open_orders:
            # only accept cancel request for open orders
            if order.get(simplefix.TAG_CLORDID) == request.get(simplefix.TAG_ORIGCLORDID):
                cancel_accept = self.get_cancel_accept_message(order, request)
                self.send_message(cancel_accept)
                self._last_cl_order_id = None
                return 
        reject = self.get_cancel_reject_message(request)
        self.send_message(reject)

    def on_cancel_replace_request(self, request):
        for order in self._open_orders:
            # only accept cancel request for open orders
            if order.get(simplefix.TAG_CLORDID) == request.get(simplefix.TAG_ORIGCLORDID):
                if request.get(simplefix.TAG_PRICE) != order.get(simplefix.TAG_PRICE):
                    order.remove(simplefix.TAG_PRICE)
                    order.append_pair(simplefix.TAG_PRICE, request.get(simplefix.TAG_PRICE))
                if request.get(simplefix.TAG_ORDERQTY) != order.get(simplefix.TAG_ORDERQTY):
                    order.remove(simplefix.TAG_ORDERQTY)
                    order.append_pair(simplefix.TAG_ORDERQTY, request.get(simplefix.TAG_ORDERQTY))
                replace_accept = self.get_replace_accept_message(order, request)
                order.remove(simplefix.TAG_CLORDID)
                order.append_pair(simplefix.TAG_CLORDID, request.get(simplefix.TAG_CLORDID))
                self.send_message(replace_accept)
                return 
        reject = self.get_cancel_reject_message(request)
        self.send_message(reject)

    def on_quote(self, quote):
        msg = self.get_quote_status_report_message(quote)
        self.send_message(msg)
        if self._expected_tag_value_for_fills != None and self._has_expected_tag_value(quote, self._expected_tag_value_for_fills):
            if self._quotes_fill_only_this_side != None:
                msg = self.get_quote_full_fill_message(quote, self._quotes_fill_only_this_side)
                self.send_message(msg)
            else:
                msg = self.get_quote_full_fill_message(quote, 1)
                self.send_message(msg)
                msg = self.get_quote_full_fill_message(quote, 2)
                self.send_message(msg)

    def on_message(self,raw_message):
        if self._test_socket != None and len(raw_message) > 0:
            self._logger.add ('TEST SOCKET SENT: %s' % (raw_message.replace(simplefix.SOH_STR, b'|')).decode())
            self._test_socket.sendto(raw_message, ('127.0.0.1', self._test_port))
        return super(FixSessionServer, self).on_message(raw_message)

    def process_message(self, msg):
       msg_type = msg.get(simplefix.TAG_MSGTYPE)
       if msg_type == simplefix.MSGTYPE_LOGON:
           self.on_logon()
           return True
       elif msg_type == simplefix.MSGTYPE_NEW_ORDER_SINGLE:
           self.on_new_order_single(msg)
           return True
       elif msg_type == simplefix.MSGTYPE_ORDER_CANCEL_REQUEST:
           self.on_cancel_request(msg)
           return True
       elif msg_type == simplefix.MSGTYPE_ORDER_CANCEL_REPLACE_REQUEST:
           self.on_cancel_replace_request(msg)
           return True
       elif msg_type == simplefix.MSGTYPE_QUOTE:
           self.on_quote(msg)
           return True
       else:
           return super(FixSessionServer, self).process_message(msg) 

    def _has_expected_tag_value(self, msg, tag_value_pair):
        value = msg.get(tag_value_pair[0])
        return value == tag_value_pair[1]

class FixServer(FixSessionHandler):
    def __init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id, outgoing_seq_num=1, logger=StdOutLogger()):
        FixSessionHandler.__init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id, outgoing_seq_num, logger)
        self._server = None
        self._test_connection = None
        self._create_test_connection = False
        self._reuse_session = False
        self._stop_event = threading.Event()
        self.set_fix_session_class(FixSessionServer)

    def set_create_test_connection(self, create_test_connection):
        self._create_test_connection = create_test_connection

    def set_reuse_session(self, reuse_session):
        self._reuse_session = reuse_session

    def start(self):
        if self.is_running():
            return
        try:
            if self._create_test_connection:
                self._test_connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        
            self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self._server.bind((self._ip, self._port))
            self._server.listen(1)

            self._is_running.set()
            while not self._stop_event.is_set():
                self._logger.add ('Listening on %s:%s' % (self._ip, self._port))
                try:
                    client_socket, client_address = self._server.accept()
                except: #pragma nocover
                    # listening socket was close or fatal error
                    break
                self._logger.add ('Accepted connection from %s:%s' % (client_address[0], client_address[1]))

                if self._session != None and self._reuse_session:
                    self._session._socket = client_socket
                else:
                    self._session = self._create_new_fix_session(client_socket)
                if self._create_test_connection:
                    self._session.set_test_socket(self._test_connection, self._port)

                self._start_commands_thread()
                self._start_session_thread()
                self._wait_until_session_ends()
                self._terminate_commands_thread()
            
            self._logger.add ('Stopping server')
            self._terminate_session_thread()
            self._terminate_commands_thread()
            self._wait_socket_closed(self._server)
            self._wait_socket_closed(self._session._socket)
            self._is_running.clear()

        except KeyboardInterrupt: #pragma nocover
            self._logger.add ('\nStopping...')
            self._stop_event.set()
            self._terminate_session_thread()
            self._terminate_commands_thread()
            self._wait_socket_closed(self._server)
            self._wait_socket_closed(self._session._socket)
            self._is_running.clear()

    def stop(self):
        self._logger.add ('Stopping')
        self._stop_event.set()
        #self._terminate_session_thread()
        try:
            self._logger.add ('Shutdown')
            self._server.shutdown(2)
            self._session.get_socket().shutdown(2)
        except:
            # possibly the socket was not open yet
            pass


