import simplefix

class FixMessageExtended(simplefix.FixMessage):
    """Fix Protocol message extended with repeating groups.

    Adds repeating groups to simplefix.FixMessage type.
    
    Repeating groups are defined with RepeatingGroup class, once defined they can be added to the message
    with the add_repeating_group method. 
    With this the message will have that repeating group registered and will understand how to parse the tags
    required for it.

    Values for a particular repeating group can be added using the append_repeating_group_pairs. That method
    will check in the internal repeating groups registered to that message and if the list of pairs match the
    definition of any of the registered repeating groups it will add the values to the message.

    Once message is filled with the values for a repeating group and it has the repeating group registered
    you can query the values for a repeating group with get_repeating_group.
    """

    def __init__(self):
        super(FixMessageExtended, self).__init__()
        self._repeating_groups = []

    def add_repeating_group_definition(self, repeating_group):
        """Adds a repeating group definition.
        
        In order to understand how a repeating group is composed you need to register it using this method.
        :param repeating_group: the RepeatingGroup object defining the repeating group
        :type repeating_group: RepeatingGroup
        """
        if not self.is_repeating_group(repeating_group._no_items_tag):
            self._repeating_groups.append(repeating_group)

    def append_repeating_group_pairs(self, repeating_group_pairs):
        """Appends the values for a repeating group already registered.

        Similar to append_pair in simplefix.FixMessage this method appends a list of pairs composed by 
        (tag, value) to the message.
        In this case the pairs passed must follow the definition of a registered repeating group.
        If the pairs passed don't fit any repeating group definition they will be ignored.
        :param repeating_group_pairs: list of pairs (tag, value) that will be added to the message.
        """
        if len(repeating_group_pairs) == 0:
            return
        for repeating_group in self._repeating_groups:
            if repeating_group._no_items_tag == int(repeating_group_pairs[0][0]):
                values_parsed = repeating_group._get_repeating_group_from_i_item(repeating_group_pairs, 0)
                if values_parsed != None: # pairs are correct
                    for tag, value in repeating_group_pairs:
                        self.append_pair( tag, value )

    def get_repeating_group(self, tag):
        """Gets all the values in the message for the given repeating group.

        Returns a list of pairs (tag, value) with the values for the given repeating group.
        The functionality is similar to simplefix.FixMessage get method, but for repeating groups.

        :param tag: tag number
        :return list of pairs (tag, value)
        """
        for repeating_group in self._repeating_groups:
            if int(tag) == int(repeating_group._no_items_tag):
                if not repeating_group.fill_from_message(self, error_if_not_found=True):
                    return None
                return repeating_group.get_values()
        return None

    def is_repeating_group(self, tag):
        """Returns True if the given tag is the main (first) tag of a registered repeating group.

        :param tag: tag number
        :return True if tag is first tag of a registered repeating group, False otherwise.
        """
        for repeating_group in self._repeating_groups:
            if repeating_group._no_items_tag == int(tag):
                return True
        return False

    def tag_belongs_to_repeating_group(self, tag):
        """ Returns true if the given tag is part of the definition of a repeating group.

        This method returns True for ANY tag that is part of a repeating group definition.

        :param tag: tag number
        :return True if the tag is part of a registered repeating group, False otherwise
        """
        if self.is_repeating_group(tag):
            return True
        for repeating_group in self._repeating_groups:
            if repeating_group.is_sub_tag(tag):
                return True
        return False

    def tag_equal(self, msg, tag):
        """Returns True if the given tag's value from given message is equal to the value in the current message.

        This method checks if a tag in a given message has the same value than in the current message, it
        looks internally if the tag is a repeating group or it's a basic tag and compares the single
        or multiple values.

        :param msg: message to compare
        :param tag: tag to compare

        :return True if tag is equal in given message, False otherwise
        """
        if not isinstance(msg, FixMessageExtended):
            if self.is_repeating_group(tag):
                return False
            else:
                return msg.get(tag) == self.get(tag)
        else:
            if not msg.is_repeating_group(tag):
                if self.is_repeating_group(tag):
                    return False
                return msg.get(tag) == self.get(tag)
            else:
                if not self.is_repeating_group(tag):
                    return False
                msg_values = msg.get_repeating_group(tag)
                this_values = self.get_repeating_group(tag)
                return msg_values == this_values

class RepeatingGroup(object):
    def __init__(self, no_items_tag, sub_tags, allow_optional_tags=False):
        self._no_items_tag = no_items_tag
        self._sub_tags = sub_tags
        self._values = []
        self._allow_optional_tags = allow_optional_tags

    def get_values(self):
        return self._values

    def set_values(self, values):
        self._values = self._get_repeating_group_from_i_item(values, 0)
        return self._values != None

    def is_sub_tag(self, tag):
        for sub_tag in self._sub_tags:
            if isinstance(sub_tag, RepeatingGroup):
                if sub_tag._no_items_tag == int(tag):
                    return True
                else:
                    return sub_tag.is_sub_tag(tag)
            else:
                if int(tag) == int(sub_tag):
                    return True
        return False

    def fill_from_message(self, msg, error_if_not_found=False):
        i = 0
        found = False
        while i < len(msg.pairs) and not found:
            if int(msg.pairs[i][0]) == self._no_items_tag:
                found = True
            else:
                i += 1
        if found:
            self._values = self._get_repeating_group_from_i_item(msg.pairs, i)
            return self._values != None
        else:
            if error_if_not_found:
                print ("Error parsing repeating group: tag [%d] was not found" % self._no_items_tag)
                return False

    def _get_tag_from_i_item(self, msg_pairs, i, tag, check_int=False):
        if i >= len(msg_pairs):
            print ("Error parsing repeating group: index [%d] is out of range" % i)
            return None
        if int(msg_pairs[i][0]) != tag:
            if not self._allow_optional_tags:
                print ("Error parsing repeating group: tag in position [%d] was expected to be [%d] and [%d] was found" % ( i, tag, int(msg_pairs[i][0])))
                return None
            else:
                # ensure that the tag is in sequence
                # if not self._tag_is_defined_after(int(msg_pairs[i][0]), tag):
                #    print "Error parsing repeating group: tag in position [%d] , [%d] was not expected" % (i, int(msg_pairs[i][0]))
                #else:
                tag_str = '%s' % tag
                return (tag_str.encode(), None)

        if check_int:
            try:
                _ = int(msg_pairs[i][1])
            except:
                print ("Error parsing repeating group: tag [%d] must be an integer" % tag)
                return None
        return msg_pairs[i]

    def _get_repeating_group_from_i_item(self, msg_pairs, i):
        values = []
        nb_items_tag_value = self._get_tag_from_i_item(msg_pairs, i, self._no_items_tag, True) # check that it's an integer
        if nb_items_tag_value is None:
            return None
        values.append(nb_items_tag_value)
        i += 1
        for _ in range(int(nb_items_tag_value[1])):
            for tag in self._sub_tags:
                if i >= len(msg_pairs) and self._allow_optional_tags:
                    tag_str = '%s' % tag
                    values.append((tag_str.encode(), None))
                else:
                    if isinstance(tag, RepeatingGroup):
                        sub_repeating_group_values = tag._get_repeating_group_from_i_item(msg_pairs, i)
                        if sub_repeating_group_values is None:
                            return None
                        values.extend(sub_repeating_group_values)
                        i += len(sub_repeating_group_values)
                    else:
                        tag_value = self._get_tag_from_i_item(msg_pairs, i, tag)
                        if tag_value is None:
                            return None
                        values.append(tag_value)
                        i += 1
                        if self._allow_optional_tags:
                            if tag_value[1] is None: #it could be an optional tag
                                i -= 1 # move i back, as the value was not found for that optional tag
        return values

    def __str__(self):
        ret = ""
        for tag, value in self._values:
            ret += '%s=%s|' % (tag.decode(), value.decode())
        return ret

    def __eq__(self, other):
        if len(self._values) != len(other._values):
            return False
        for i in range(len(self._values)):
            if self._values[i] != other._values[i]:
                return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)


