#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import sys, os
import json
import simplefix

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class LoggerEntryType(object):
    BASIC = 1
    BOLD = 2

class LoggerEntry(object):
    def __init__(self, text):
        self._terminal_content = text
        self._type = LoggerEntryType.BASIC
        self._clean_content = self._remove_special_chars(text)

    def _remove_special_chars(self, text):
        entries = []
        text = text.replace('%s' % bcolors.HEADER, '')
        text = text.replace('%s' % bcolors.OKBLUE, '')
        text = text.replace('%s' % bcolors.OKGREEN, '')
        text = text.replace('%s' % bcolors.WARNING, '')
        text = text.replace('%s' % bcolors.FAIL, '')
        text = text.replace('%s' % bcolors.ENDC, '')
        text = text.replace('%s' % bcolors.BOLD, '')
        text = text.replace('%s' % bcolors.UNDERLINE, '')
        for line in text.split('\n'):
            entries.append(line)
        return entries

    def getContent(self):
        return self._clean_content

    def print_to_stdout(self):
        print ("%s" % self._terminal_content)

class LoggerEntryBold(LoggerEntry):
    def __init__(self, text):
        LoggerEntry.__init__(self, text)
        self._type = LoggerEntryType.BOLD

class Logger(object):
    def __init__(self):
        self._entries = []

    def add(self, text, style=LoggerEntryType.BASIC):
        self._entries.append(self._create_entry(text, style))

    def getEntries(self):
        return self._entries

    def _create_entry(self, text, style):
        if style == LoggerEntryType.BOLD:
            logger_entry = LoggerEntryBold(text)
        else:
            logger_entry = LoggerEntry(text)
        return logger_entry

class StdOutLogger(Logger):
    def __init__(self):
        Logger.__init__(self)

    def add(self, text):
        entry = Logger._create_entry(self, text, LoggerEntryType.BASIC)
        Logger.add(self, text)
        entry.print_to_stdout()

class FileLogger(Logger):
    def __init__(self, file_path):
        Logger.__init__(self)
        self._file_path = file_path
        self._fd = open(file_path, "w")

    def add(self, text):
        entry = Logger._create_entry(self, text, LoggerEntryType.BASIC)
        Logger.add(self, text)
        self._log(entry)

    def _log(self, entry):
        for line in entry.getContent():
            self._fd.write('%s\n' % line)
            self._fd.flush()


class FileStdOutLogger(FileLogger):
    def __init__(self, file_path):
        FileLogger.__init__(self, file_path)

    def add(self, text):
        entry = Logger._create_entry(self, text, LoggerEntryType.BASIC)
        Logger.add(self, text)
        self._log(entry)
        entry.print_to_stdout()

class HtmlLogger(FileLogger):
    def __init__(self, file_path, html_path, definition_path, cssPath=None):
        FileLogger.__init__(self, html_path)
        self._plain_logger = FileStdOutLogger(file_path)
        self._css = cssPath
        html_header = Logger._create_entry(self, "%s" % self._get_html_header(), LoggerEntryType.BASIC)
        self._log(html_header)
        definition_parser = HTMLReportParser(definition_path, self._plain_logger)
        self._definition_items = definition_parser.parse()
        if self._definition_items is None:
            sys.exit(2)

    def _add_item_row(self, classType, value, row):
        row += '<th id=\"%s\">%s</th>\n' % (classType, value)
        return row

    def add_test_messages(self, result, test_name, initial, expected, actual):
        pass
        
    def add_test(self, msgtype, id, qty, side, symbol, price, orderID, expectedStatus, expectedRej, time, actualStatus, actualRej):
        pass

    def finish(self):
        self._log(Logger._create_entry(self, "</div>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "</body>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "</html>", LoggerEntryType.BASIC))

    def add(self, text):
        self._plain_logger.add(text)

    def _get_css(self):
        if self._css is None:
            return 'html *\n \
                {\n \
                    font-family: Arial !important;\n \
                }\n \
                #gateway {\n \
                text-align: left;\n \
                color: white;\n \
                border: 2px solid black;\n \
                font-size: 200%;\n \
                background-color: rgb(100, 149, 237);\n \
                margin-top: 1em;\n \
                }\n \
                #tests-table {\n \
                text-align: left;\n \
                color: black;\n \
                border: 2px solid black;\n \
                font-size: 80%;\n \
                width: 100%;\n \
                background-color: rgb(255, 255, 255);\n \
                }\n \
                #tests-table-header {\n \
	            background-color: rgb(100, 149, 237);\n \
                color: white;\n \
                }\n \
                #row-pass {\n \
                color: green;\n \
                }\n \
                #row-fail {\n \
                color: red;\n \
                }\n \
                #test-name {\n \
                vertical-align: top;\n \
                }\n \
                #test-result-ok {\n \
                vertical-align:bottom;\n \
                color: black;\n \
                background-color: green;\n \
                }\n \
                #test-result-error {\n \
                vertical-align:bottom;\n \
                color: black;\n \
                background-color: red;\n \
                }\n \
                #log-entry-sent {\n \
	            font-weight: bold;\n \
                }\n \
                #log-entry-received {\n \
                color: rgb(72, 61, 139);\n \
                }\n \
                #log-entry-error {\n \
                color: rgb(178, 34, 34);\n \
                }\n \
                table, th, td {\n \
                border: 1px solid black;\n \
                }\n \
                table {\n \
                border-collapse: collapse;\n \
                }\n'
        else:
            css_file = open(self._css,"r")
            css = ''
            for line in css_file.readlines():
                css += line
            return css

    def _get_html_header(self):
        return '<!DOCTYPE html>\n \
                <html>\n \
                <head>\n \
                <style>\n \
                %s \
                </style>\n \
                </head>\n \
                <body>' % self._get_css()

    def _get_suite_table_header(self):
        return '<div>\n \
                <table id=\"tests-table\">\n \
                <tr id=\"tests-table-header\">\n \
                <th style=\"width:10%\">Venue</th>\n \
                <th style=\"width:85%\">Messages</th>\n \
                <th style=\"width:5%\">Result</th>\n \
                </tr>'

    def add_suite_footer(self):
        self._log(Logger._create_entry(self, "</table>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "</div>", LoggerEntryType.BASIC))

    def add_suite_header(self, text):
        entry = Logger._create_entry(self, "<div id=\"gateway\">%s</div>" % text, LoggerEntryType.BASIC)
        self._log(entry)
        table_header = Logger._create_entry(self, "%s" % self._get_suite_table_header(), LoggerEntryType.BASIC)
        self._log(table_header)

    def add_test_name(self, text):
        self._log(Logger._create_entry(self, "<tr>", LoggerEntryType.BASIC))
        entry = Logger._create_entry(self, "<td id=\"test-name\">%s</td>" % text, LoggerEntryType.BASIC)
        self._log(entry)

    def begin_test_log(self):
        self._log(Logger._create_entry(self, "<td>", LoggerEntryType.BASIC))

    def end_test_log(self):
        self._log(Logger._create_entry(self, "</td>", LoggerEntryType.BASIC))

    def add_sent_message(self, message):
        self._log(Logger._create_entry(self, "<div id=\"log-entry-sent\">%s</div>" % message, LoggerEntryType.BASIC))

    def add_received_message(self, message):
        self._log(Logger._create_entry(self, "<hr>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "<div id=\"log-entry-received\">%s</div>" % message, LoggerEntryType.BASIC))

    def add_error_message(self, message):
        self._log(Logger._create_entry(self, "<hr>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "<div id=\"log-entry-error\">%s</div>" % message, LoggerEntryType.BASIC))

    def add_ok_result(self):
        self._log(Logger._create_entry(self, "<td id=\"test-result-ok\">OK</td>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "</tr>", LoggerEntryType.BASIC))

    def add_error_result(self):
        self._log(Logger._create_entry(self, "<td id=\"test-result-error\">ERROR</td>", LoggerEntryType.BASIC))
        self._log(Logger._create_entry(self, "</tr>", LoggerEntryType.BASIC))

class HTMLReportField (object):
    def __init__(self, column_name, field_id, default_value=''):
        self._column_name = column_name
        self._field_id = field_id
        self._default_value = default_value

    def getName(self):
        return self._column_name

    def getID(self):
        return self._field_id

    def getDefaultValue(self):
        return self._default_value

    def getValueFromMsg(self, msg):
        return msg.get(self._field_id)

class HTMLReportFields (object):
    def __init__(self):
        self._description_fields = []
        self._expected_fields = []
        self._actual_fields = []

    def addDescriptionField(self, field):
        self._description_fields.append(field)

    def addExpectedActualField(self, field):
        self._expected_fields.append(field)
        self._actual_fields.append(field)

    def getDescriptionColumns(self):
        columns = []
        for field in self._description_fields:
            columns.append(field.getName())
        return columns
    
    def getDescriptionItems(self):
        return self._description_fields

    def getExpectedColumns(self):
        columns = []
        for field in self._expected_fields:
            columns.append(field.getName())
        return columns 

    def getExpectedItems(self):
        return self._expected_fields

    def getActualColumns(self):
        columns = []
        for field in self._actual_fields:
            columns.append(field.getName())
        return columns

    def getActualItems(self):
        return self._actual_fields

    def getColumns(self):
        return self.getDescriptionColumns() + self.getExpectedColumns() + self.getActualColumns()

class HTMLReportFieldJsonTags(object):
    TAG_REPORT = "report"
    TAG_DESCRIPTION_FIELDS = "description_fields"
    TAG_EXPECTED_ACTUAL_FIELDS = "expected_actual_fields"
    TAG_NAME = "name"
    TAG_ID = "id"

class HTMLReportParserBase(object):
    def __init__(self, logger=StdOutLogger()):
        self._logger = logger

    def tag_exists(self, tag, json_data):
        return tag in json_data

    def check_expected_tag(self, tag, json_data):
        if not self.tag_exists(tag, json_data):
            self._logger.add ('Required tag %s not found in %s' % (tag, json_data))
            return False
        return True

    def check_expected_tags(self, tags, json_data):
        result = True
        for tag in tags:
            result = result and self.check_expected_tag(tag, json_data)
        return result

    def check_expected_mutal_exclusive_tags(self, tags, json_data):
        nb_tags_present = 0
        for tag in tags:
            if self.tag_exists(tag, json_data):
                nb_tags_present += 1
        if nb_tags_present != 1:
            self._logger.add ('Value for one (and only one) of the items in [%s] should be found in %s' % (tags, json_data))
            return False
        return True

class HTMLReportParser (HTMLReportParserBase):
    def __init__(self, path, logger=StdOutLogger()):
        self._path = path
        self._logger = logger

    def parse(self):
        if not os.path.exists(self._path):
            self._logger.add("ERROR parsing report definition. File [%s] does not exist." % self._path)
            return None
        with open(self._path, 'r') as read_file:
            data = json.load(read_file)
            return self._parse_from_data(data)
    
    def _validate(self, json_data):
        if not self.check_expected_tags( [HTMLReportFieldJsonTags.TAG_REPORT], json_data):
            return False

        message = json_data[HTMLReportFieldJsonTags.TAG_REPORT]
        if not self.check_expected_tags( [HTMLReportFieldJsonTags.TAG_DESCRIPTION_FIELDS, HTMLReportFieldJsonTags.TAG_EXPECTED_ACTUAL_FIELDS], message):
            return False

        for tag in message[HTMLReportFieldJsonTags.TAG_DESCRIPTION_FIELDS]:
            if not self.check_expected_tags( [HTMLReportFieldJsonTags.TAG_NAME, HTMLReportFieldJsonTags.TAG_ID], tag):
                return False
        return True

    def _parse_from_data(self, json_data):
        if not self._validate(json_data):
            return None

        report = HTMLReportFields()
        report_json = json_data[HTMLReportFieldJsonTags.TAG_REPORT]

        for field in report_json[HTMLReportFieldJsonTags.TAG_DESCRIPTION_FIELDS]:
            name = field[HTMLReportFieldJsonTags.TAG_NAME]
            id = field[HTMLReportFieldJsonTags.TAG_ID]
            report_field = HTMLReportField(name, id)
            report.addDescriptionField(report_field)

        for field in report_json[HTMLReportFieldJsonTags.TAG_EXPECTED_ACTUAL_FIELDS]:
            name = field[HTMLReportFieldJsonTags.TAG_NAME]
            id = field[HTMLReportFieldJsonTags.TAG_ID]
            report_field = HTMLReportField(name, id)
            report.addExpectedActualField(report_field)
        return report