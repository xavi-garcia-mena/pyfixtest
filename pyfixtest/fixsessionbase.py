#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import simplefix
import select
from threading import Lock, Event
from .stopablethread import StopableThread
from .msgutils import BufferFixParser, FixMessageWithHighlightedTags
from .jsonmsgparser import JsonMsgParser
from .logger import StdOutLogger
import sys
import time
import os.path

class CommandsThread(StopableThread):
    def __init__(self, session, read_commands_from, logger=StdOutLogger()):
        self._session = session
        self._read_commands_from = read_commands_from
        self._logger = logger
        self._number_of_messages_per_stage = 10
        self._multiple_messages_interval = 0.1
        StopableThread.__init__(self, self._read_commands, args=())

    def set_multiple_orders_parameters(self, number_of_messages_per_stage, interval):
        self._number_of_messages_per_stage = number_of_messages_per_stage
        self._multiple_messages_interval = interval

    def _send_stage(self, parser, interval):
        if parser != None:
            msg = parser.recalculate_dynamic_tags()
            if msg != None:
                # msgseqnum and sending time are added everytime we send a message
                msg.remove(simplefix.TAG_MSGSEQNUM)
                msg.remove(simplefix.TAG_SENDING_TIME)
                self._session.send_message(msg)
                time.sleep(interval)

    def _send_multiple_times(self):
        stage1_file = 'pyfixtest_multiple_stage1.json'
        stage2_file = 'pyfixtest_multiple_stage2.json'
        stage3_file = 'pyfixtest_multiple_stage3.json'
        interval = self._multiple_messages_interval
        number_of_messages = self._number_of_messages_per_stage
        if os.path.isfile(stage1_file):
            parser_stage1 = JsonMsgParser(stage1_file, self._session)
            msg_stage1 = parser_stage1.parse()
            parser_stage2 = None
            msg_stage2 = None
            if os.path.isfile(stage2_file):
                parser_stage2 = JsonMsgParser(stage2_file, self._session)
                msg_stage2 = parser_stage2.parse()
            parser_stage3 = None
            msg_stage3 = None
            if os.path.isfile(stage3_file):
                parser_stage3 = JsonMsgParser(stage3_file, self._session)
                msg_stage3 = parser_stage3.parse()
            for i in range(number_of_messages):
                self._send_stage(parser_stage1, interval)
                self._send_stage(parser_stage2, interval)
                self._send_stage(parser_stage3, interval)

    def _read_commands(self):
        while not self.asked_to_terminate():
            ready_to_read, _, _ = select.select([self._read_commands_from], [], [], 1)
            if self._read_commands_from in ready_to_read:
                command = self._read_commands_from.readline().strip()
                if command != '' and self._session != None:
                    if command == 'multiple' or command == b'multiple':
                        self._send_multiple_times()
                    else:
                        filepath = './message_%s.json' % command.decode()
                        if not os.path.isfile(filepath):
                            print ('ERROR file [%s] does not exist' % filepath)
                        else:
                            parser = JsonMsgParser(filepath, self._session)
                            msg = parser.parse()
                            if msg != None:
                                self._session.send_message(msg)

class SessionThread(StopableThread):
    def __init__(self, session, ready_flag, logger=StdOutLogger()):
        self._session = session
        self._logger = logger
        StopableThread.__init__(self, self._handle_connection, args=())
        self._running = Event()
        self._ready_flag = ready_flag

    def _handle_connection(self):
        buffer_parser = BufferFixParser()
        is_connection_valid = True
        self._running.set()
        self._ready_flag.set()
        while not self.asked_to_terminate() and is_connection_valid:
            try:
                ready_to_read, _, _ = select.select([self._session.get_socket()], [], [], 1)
            except KeyboardInterrupt: #pragma nocover
                raise
            except Exception as e: #pragma nocover
                is_connection_valid = False
                print (e)
                break
            if len(ready_to_read) > 0:
                try:
                    raw_buffer = self._session.read_buffer()
                    if raw_buffer == b'':
                        print ('Connection closed')
                        is_connection_valid = False
                        break
                    if self._session.is_not_fix_protocol():
                        self._logger.add ('RAW IN:    %s' % (self._session.get_hex(raw_buffer)))
                        request = self._session.convert_incoming(raw_buffer)
                        if request != None:
                            self._session.on_message(request.encode())
                    else:
                        for request in buffer_parser.split_messages_in_buffer(raw_buffer):
                            self._session.on_message(request)
                except KeyboardInterrupt: #pragma nocover
                    raise
                except Exception as e: #pragma nocover
                    is_connection_valid = False
                    print (e)
        if self.asked_to_terminate():
            logout = self._session.get_logout_message('LOGOUT')
            self._session.send_message(logout)
        self._running.clear()
        print ('Session stopped')

class FixSessionHandler(object):
    def __init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id, outgoing_seq_num=1, logger=StdOutLogger()):
        self._ip = ip
        self._port = port
        self._fix_version_string = fix_version_string
        self._sender_comp_id = sender_comp_id
        self._target_comp_id = target_comp_id
        self._outgoing_seq_num = outgoing_seq_num
        self._session = None
        self._is_running = Event()
        self._commands_thread = None
        self._session_thread = None
        self._session_class = None
        self._read_commands_from = sys.stdin
        self._highlighted_tags = None
        self._expected_tag_value_for_fills = None
        self._on_ack_reply_tags = None
        self._on_ack_reply_tags_enhance_default = False
        self._on_fill_reply_tags = None
        self._on_fill_reply_tags_enhance_default = False
        self._on_cancel_reply_tags = None
        self._on_cancel_reply_tags_enhance_default = False
        self._on_replace_reply_tags = None
        self._on_replace_reply_tags_enhance_default = False
        self._on_quote_ack_reply_tags = None
        self._on_quote_ack_reply_tags_enhance_default = False
        self._on_quote_fill_reply_tags = None
        self._on_quote_fill_reply_tags_enhance_default = False
        self._quotes_fill_only_this_side = None
        self._logger = logger
        self._multiple_nb_orders = 10
        self._multiple_interval = 0.1
        self._protocol_file = None
   
    def set_protocol_file(self, protocol_file):
        self._protocol_file = protocol_file

    def is_running(self):
        return self._is_running.is_set()

    def set_highlighted_tags(self, highlighted_tags):
        self._highlighted_tags = highlighted_tags
    
    def set_multiple_orders_params(self, nb_orders, interval):
        self._multiple_nb_orders = nb_orders
        self._multiple_interval = interval

    def set_expected_tag_in_new_orders_for_fills(self, tag, expected_value):
        self._expected_tag_value_for_fills = (tag,expected_value)

    def set_on_ack_reply_tags(self, reply_tags, enhance_default=False):
        self._on_ack_reply_tags = reply_tags
        self._on_ack_reply_tags_enhance_default = enhance_default

    def set_on_fill_reply_tags(self, reply_tags, enhance_default=False):
        self._on_fill_reply_tags = reply_tags
        self._on_fill_reply_tags_enhance_default = enhance_default

    def set_on_cancel_reply_tags(self, reply_tags, enhance_default=False):
        self._on_cancel_reply_tags = reply_tags
        self._on_cancel_reply_tags_enhance_default = enhance_default

    def set_on_replace_reply_tags(self, reply_tags, enhance_default=False):
        self._on_replace_reply_tags = reply_tags
        self._on_replace_reply_tags_enhance_default = enhance_default
    
    def set_on_quote_fill_reply_tags(self, reply_tags, enhance_default=False):
        self._on_quote_fill_reply_tags = reply_tags
        self._on_quote_fill_reply_tags_enhance_default = enhance_default

    def set_on_quote_ack_reply_tags(self, reply_tags, enhance_default=False):
        self._on_quote_ack_reply_tags = reply_tags
        self._on_quote_ack_reply_tags_enhance_default = enhance_default
    
    def set_quote_fill_only_side(self, side):
        self._quotes_fill_only_this_side = side

    def _create_new_fix_session(self, socket):
        if self._session_class is None:
            session = FixSessionBase(self._fix_version_string, self._sender_comp_id, self._target_comp_id, socket, self._outgoing_seq_num)
        else:
            session = self._session_class(self._fix_version_string, self._sender_comp_id, self._target_comp_id, socket, self._outgoing_seq_num)
            if self._protocol_file is not None:
                session.set_protocol_file(self._protocol_file)
                session.load_protocol()
        if self._highlighted_tags != None:
            session.set_highlighted_tags(self._highlighted_tags)
        if self._expected_tag_value_for_fills != None:
            session.set_expected_tag_in_new_orders_for_fills(self._expected_tag_value_for_fills[0], self._expected_tag_value_for_fills[1])
        if self._on_ack_reply_tags != None:
            session.set_on_ack_reply_tags(self._on_ack_reply_tags, self._on_ack_reply_tags_enhance_default)
        if self._on_fill_reply_tags != None:
            session.set_on_fill_reply_tags(self._on_fill_reply_tags, self._on_fill_reply_tags_enhance_default)
        if self._on_cancel_reply_tags != None:
            session.set_on_cancel_reply_tags(self._on_cancel_reply_tags, self._on_cancel_reply_tags_enhance_default)
        if self._on_replace_reply_tags != None:
            session.set_on_replace_reply_tags(self._on_replace_reply_tags, self._on_replace_reply_tags_enhance_default)
        if self._on_quote_ack_reply_tags != None:
            session.set_on_quote_ack_reply_tags(self._on_quote_ack_reply_tags, self._on_quote_ack_reply_tags_enhance_default)
        if self._on_quote_fill_reply_tags != None:
            session.set_on_quote_fill_reply_tags(self._on_quote_fill_reply_tags, self._on_quote_fill_reply_tags_enhance_default)
        if self._quotes_fill_only_this_side != None:
            session.set_quote_fill_only_side(self._quotes_fill_only_this_side)
        return session

    def set_fix_session_class(self, session_creator):
        self._session_class = session_creator

    def start(self):
        print ('Plese reimplement the FixSessionHandler::start() method!')
        
    def stop(self):
        print ('Please reimplement the FixSessionHandler::stop() method')

    def _start_commands_thread(self, nb_multiple_messages=10, multiple_messages_interval=0.1): 
        self._commands_thread = CommandsThread(self._session, self._read_commands_from)
        self._commands_thread.set_multiple_orders_parameters(nb_multiple_messages, multiple_messages_interval)
        self._commands_thread.start()

    def _start_session_thread(self):
        ready_flag = Event()
        self._session_thread = SessionThread(self._session, ready_flag)
        self._session_thread.start()
        # wait until session thread is ready
        while not ready_flag.is_set():
            time.sleep(0.1)

    def _wait_until_session_ends(self):
        while self._session_thread._running.is_set():
            time.sleep(0.1)

    def _terminate_commands_thread(self):
        if self._commands_thread != None and self._commands_thread.is_alive():
            self._commands_thread.terminate()
            self._commands_thread.join()

    def _terminate_session_thread(self):
        if self._session_thread != None and self._session_thread.is_alive():
            self._session_thread.terminate()
            self._session_thread.join()
            print ('JOINED')
    
    def _wait_socket_closed(self, sock, timeout = 5):
        sock.close()
        time_elapsed = 0
        try:
            id = sock.fileno()
        except:
            id = -1
        while time_elapsed < timeout and id != -1:
            try:
                id = sock.fileno()
            except:
                id = -1
            time.sleep(0.1)
            time_elapsed += 0.1


class FixSessionBase(object):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        self._fix_version_string = fix_version_string 
        self._sender_comp_id = sender_comp_id
        self._target_comp_id = target_comp_id
        self._socket = socket
        self._outgoing_seq_num = outgoing_seq_num
        self._highlighted_tags = []
        self._mutex = Lock()
        self._expected_tag_value_for_fills = None
        self._on_ack_reply_tags = None
        self._on_ack_reply_tags_enhance_default = False
        self._on_fill_reply_tags = None
        self._on_fill_reply_tags_enhance_default = False
        self._on_cancel_reply_tags = None
        self._on_cancel_reply_tags_enhance_default = False
        self._on_replace_reply_tags = None
        self._on_replace_reply_tags_enhance_default = False
        self._on_quote_ack_reply_tags = None
        self._on_quote_ack_reply_tags_enhance_default = False
        self._on_quote_fill_reply_tags = None
        self._on_quote_fill_reply_tags_enhance_default = False
        self._quotes_fill_only_this_side = None # if none it will fill both sides
        self._logger = logger
        self._last_cl_order_id = None
        self._protocol_file = None

    def is_not_fix_protocol(self):
        return False

    def set_protocol_file(self, protocol_file):
        # reimplemented in BinarySession
        pass

    def load_protocol(self):
        # reimplemented in BinarySession
        pass

    def set_expected_tag_in_new_orders_for_fills(self, tag, expected_value):
        if (type(expected_value) == str):
            expected_value = expected_value.encode()
        self._expected_tag_value_for_fills = (tag,expected_value)

    def set_on_ack_reply_tags(self, reply_tags, enhance_default):
        self._on_ack_reply_tags = reply_tags
        self._on_ack_reply_tags_enhance_default = enhance_default

    def set_on_fill_reply_tags(self, reply_tags, enhance_default):
        self._on_fill_reply_tags = reply_tags
        self._on_fill_reply_tags_enhance_default = enhance_default

    def set_on_cancel_reply_tags(self, reply_tags, enhance_default):
        self._on_cancel_reply_tags = reply_tags
        self._on_cancel_reply_tags_enhance_default = enhance_default

    def set_on_replace_reply_tags(self, reply_tags, enhance_default):
        self._on_replace_reply_tags = reply_tags
        self._on_replace_reply_tags_enhance_default = enhance_default

    def set_on_quote_ack_reply_tags(self, reply_tags, enhance_default):
        self._on_quote_ack_reply_tags = reply_tags
        self._on_quote_ack_reply_tags_enhance_default = enhance_default

    def set_on_quote_fill_reply_tags(self, reply_tags, enhance_default):
        self._on_quote_fill_reply_tags = reply_tags
        self._on_quote_fill_reply_tags_enhance_default = enhance_default

    def set_quote_fill_only_side(self, side):
        self._quotes_fill_only_this_side = side

    def get_logon_message(self):
        logon = self.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon.append_pair(simplefix.TAG_ENCRYPTMETHOD, '0')
        return logon

    def get_heartbeat_message(self):
        hb = self.get_msg_header_message()
        hb.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_HEARTBEAT)
        return hb
        
    def get_logout_message(self, reason):
        logout = self.get_msg_header_message()
        logout.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGOUT)
        logout.append_pair(simplefix.TAG_TEXT, reason)
        return logout

    def get_sequence_reset_message(self, request):
        seq_reset = self.get_msg_header_message()
        seq_reset.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SEQUENCE_RESET)
        seq_reset.append_pair(simplefix.TAG_NEWSEQNO, self._outgoing_seq_num)
        seq_reset.append_pair(simplefix.TAG_GAPFILLFLAG, 'Y')
        return seq_reset

    def on_heartbeat(self):
        hb_reply = self.get_heartbeat_message()
        self.send_message(hb_reply)

    def on_test_request(self, test_request):
        hb_reply = self.get_heartbeat_message()
        hb_reply.append_pair(simplefix.TAG_TESTREQID, test_request.get(simplefix.TAG_TESTREQID))
        self.send_message(hb_reply)

    def on_logout(self):
        logout_reply = self.get_logout_message("Logout")
        self.send_message(logout_reply)
        self.close()

    def on_resend_request(self, request):
        if request.get(simplefix.TAG_ENDSEQNO) == b'0':
            seq_reset = self.get_sequence_reset_message(request)
            change_outgoing_seq_num_when_sending = False
            outgoing_seq_num_bk = self._outgoing_seq_num 
            if (self._outgoing_seq_num > int(request.get(simplefix.TAG_BEGINSEQNO))):
                change_outgoing_seq_num_when_sending = True
                self._outgoing_seq_num = int(request.get(simplefix.TAG_BEGINSEQNO))
            self.send_message(seq_reset)
            if change_outgoing_seq_num_when_sending:
                self._outgoing_seq_num = outgoing_seq_num_bk

    def on_message(self,raw_message):
        parser = simplefix.FixParser()
        parser.append_buffer(raw_message.decode())
        msg = parser.get_message()
        if msg != None:
            self._logger.add ('RECV: %s    %s' % (msg.get(simplefix.TAG_MSGTYPE).decode(), self.get_message_highlighted_string(msg).decode()))
            return self.process_message(msg)
        return False

    def get_last_cl_order_id(self):
        return self._last_cl_order_id

    def process_message(self, msg):
       msg_type = msg.get(simplefix.TAG_MSGTYPE)
       if msg_type == simplefix.MSGTYPE_HEARTBEAT:
           self.on_heartbeat()
           return True
       elif msg_type == simplefix.MSGTYPE_TEST_REQUEST:
           self.on_test_request(msg)
           return True
       elif msg_type == simplefix.MSGTYPE_LOGOUT:
           self.on_logout()
           return True
       elif msg_type == simplefix.MSGTYPE_RESEND_REQUEST:
           self.on_resend_request(msg)
           return True
       return False

    def set_highlighted_tags(self, highlighted_tags):
        self._highlighted_tags = highlighted_tags

    def get_message_highlighted_string(self, message):
        new_msg = FixMessageWithHighlightedTags()
        new_msg.pairs = message.pairs
        new_msg.begin_string = message.begin_string
        new_msg.message_type = message.message_type
        new_msg.header_index = message.header_index
        return new_msg.get_highlighted_encoding(self._highlighted_tags)

    def get_msg_str(self, msg, soh_byte=b'|'):
        return msg.encode().replace(simplefix.SOH_STR, soh_byte)

    def get_msg_header_message(self):
        msg = simplefix.FixMessage()
        msg.append_pair(simplefix.TAG_BEGINSTRING, self._fix_version_string, header=True)
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id, header=True)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id, header=True)
        return msg

    def get_msg_header_reverse_credentials_message(self):
        msg = simplefix.FixMessage()
        msg.append_pair(simplefix.TAG_BEGINSTRING, self._fix_version_string, header=True)
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._target_comp_id, header=True)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._sender_comp_id, header=True)
        return msg

    def convert_outgoing(self, msg):
        raise Exception('Please reimplement th FixSessionBase.convert_outgoing() method')

    def convert_incoming(self, msg):
        raise Exception('Please reimplement th FixSessionBase.convert_incoming() method')

    def get_hex(self, binary_message):
        if sys.version_info[0] == 2:
            return ":".join("{0:02x}".format(ord(c)) for c in binary_message)
        else:
            return ":".join("{0:02x}".format(c) for c in binary_message)
        

    def send_message(self, msg):
        try:
            self._mutex.acquire()

            if self.is_not_fix_protocol():
                self._logger.add ('SENT: %s    %s' % (msg.get(simplefix.TAG_MSGTYPE).decode(), self.get_message_highlighted_string(msg).decode()))
                outgoing_msg = self.convert_outgoing(msg)
                self._logger.add ('RAW OUT:   %s\n' % (self.get_hex(outgoing_msg)))
                if outgoing_msg != None and outgoing_msg != b'':
                    self._socket.send(outgoing_msg)
            else:
                msg.append_pair(simplefix.TAG_MSGSEQNUM, self._outgoing_seq_num, header=True)
                msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME, header=True)
                self._logger.add ('SENT: %s    %s' % (msg.get(simplefix.TAG_MSGTYPE).decode(), self.get_message_highlighted_string(msg).decode()))
                self._outgoing_seq_num += 1
                self._socket.send(msg.encode())
            return msg
        finally:
            self._mutex.release()

    def close(self):
        self._socket.close()

    def get_socket(self):
        return self._socket

    def read_buffer(self):
        raw_buffer = b''
        buff = self._socket.recv(1024)
        while buff != b'':
            raw_buffer += buff
            ready_to_read, _, _ = select.select([self._socket], [], [], 0)
            if len(ready_to_read) > 0:
                buff = self._socket.recv(1024)
            else:
                buff = b''
        return raw_buffer

    def _fill_tags_from_reply(self, reply_tags, msg, request):
        parser = JsonMsgParser(None, self)
        for tag, value in reply_tags:
            if type(value) == bytes:
                value = value.decode()
            if value.startswith(parser.get_tag_from_preffix()):
                value_from = value.replace(parser.get_tag_from_preffix(), '')
                msg.remove(tag)
                msg.append_pair(tag, request.get(value_from))
            else:
                msg.remove(tag)
                msg.append_pair(tag, value)
        return msg

