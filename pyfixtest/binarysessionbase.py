#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2021, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import json, sys, os, struct, simplefix
from .fixsessionserver import FixSessionServer
from .logger import StdOutLogger

class BinaryType(object):
    def __init__(self, name, size, pack_name):
        self._name = name
        self._size = size
        self._pack_name = pack_name
    
    def get_name(self):
        return self._name

class BinaryProtocolParser(object):
    def __init__(self):
        self._data = None
        self._incoming = []
        self._outgoing = []

    def parse(self, file_path):
        if not os.path.isfile(file_path):
            print ('ERROR: Protocol file [%s] does not exist.' % file_path)
            sys.exit(2)
        protocol_file = open(file_path, 'r')
        data = json.load(protocol_file)
        if not self._verify(data):
            return False
        self._data = data
        return True
    
    def _verify(self, data):
        if not isinstance(data, dict):
            print ('ERROR: procotol file seems to be incorrect. Was expecting a unique json object as the top element')
            return False
        if len(data) != 1:
            print ('ERROR: procotol file seems to be incorrect. Was expecting a unique json object as the top element')
            return False
        # Protocol have a protocol object
        if not 'protocol' in data:
            print ('ERROR: procotol file seems to be incorrect. It should have a "protocol" item.')
            return False
        protocol = data['protocol']
        if not 'name' in protocol:
            print ('ERROR: "protocol" object should have a "name".')
            return False
        if not 'types' in protocol:
            print ('ERROR: "protocol" object should have a "types" description.')
            return False
        if not 'messages' in protocol:
            print ('ERROR: "protocol" object should have an "messages" object.')
            return False
        if not self._verify_messages(protocol, 'messages'):
            return False
        if not self._verify_types(protocol['types']):
            return False
        return True

    def _verify_messages(self, protocol, messages_name):
        messages = protocol[messages_name]
        if not isinstance(messages, list):
            print ('ERROR: "%s" should be a list.')
            return False
        for message in messages:
            if not self._verify_message(message):
                return False
        return True

    def _check_required_field_in_message(self, field, message):
        if not field in message:
            print ('ERROR: "%s" not found for message:\n%s' % (field, message))
            return False
        return True

    def _verify_message(self, message):
        if not isinstance(message, dict):
            print ('ERROR: Message\n%s is not valid.' % message)
            return False
        if not self._check_required_field_in_message('msgname', message):
            return False
        if not self._check_required_field_in_message('msgtype', message):
            return False
        if not self._check_required_field_in_message('msgtype', message):
            return False
        if not self._check_required_field_in_message('fields', message):
            return False
        return self._verify_fields(message['fields'])

    def _verify_fields(self, fields):
        if not isinstance(fields, list):
            print ('ERROR: fields [%s] should be a list.' % fields)
            return False
        for field in fields:
            if not self._verify_field(field):
                return False
        return True

    def _check_required_field_in_field(self, field_name, field):
        if not field_name in field:
            print ('ERROR: "%s" not found for field:\n%s' % (field_name, field))
            return False
        return True
    
    def _verify_field(self, field):
        if not isinstance(field, dict):
            print ('ERROR: Field [%s] is not valid.' % field)
            return False
        if not self._check_required_field_in_field('field', field):
            return False
        if not self._check_required_field_in_field('type', field):
            return False
        type_name = field['type']
        if type_name == 'left_justified_string':
            if not self._check_required_field_in_field('size', field):
                return False
        return True

    def _verify_types(self, types):
        if not isinstance(types, list):
            print ('ERROR: types should be a list.')
            return False
        for t in types:
            if not self._verify_type(t):
                return False
        return True

    def _check_field_in_type(self, field, t):
        if not field in t:
            print ('ERROR: Field [%s] not found in type: [%s]' % (field, t))
            return False
        return True

    def _verify_type(self, t):
        if not isinstance(t, dict):
            print ('ERROR: type [%s] is not correct.' % t)
            return False
        if not self._check_field_in_type('name', t):
            return False
        if not self._check_field_in_type('pack_as', t):
            return False
        pack_as = t['pack_as']
        if pack_as == 'left_justify':
            if not self._check_field_in_type('justify_char', t):
                return False
        return True

    def get_protocol_name(self):
        return self._data['protocol']['name']

    def get_types(self):
        return self._data['protocol']['types']
    
    def get_type_by_name(self, type_name):
        for t in self._data['protocol']['types']:
            if t['name'] == type_name:
                return t
        return None

    def get_messages(self):
        return self._data['protocol']['messages']

    def get_message_by_name(self, name):
        for message in self._data['protocol']['messages']:
            if message['msgname'] == name:
                return message
        return None
    
    def get_message_by_msgtype(self, msgtype):
        for message in self._data['protocol']['messages']:
            if message['msgtype'] == msgtype or message['msgtype'].encode() == msgtype:
                return message
        return None

    def get_message_fields(self, message):
        fields = []
        fields_base = []
        if 'extends' in message:
            msg_base = self.get_message_by_name(message['extends'])
            if msg_base is None:
                print ('ERROR: base message [%s] was not found' % message['extends'])
                return []
            fields_base = self.get_message_fields(msg_base)
        for field in fields_base:
            fields.append(field)
        for field in message['fields']:
            fields.append(field)
        return fields

    def get_message_fields_by_name(self, msgname):
        message = self.get_message_by_name(msgname)
        if message is None:
            return []
        return self.get_message_fields(message)

    def get_message_fields_by_msgtype(self, msgtype):
        message = self.get_message_by_msgtype(msgtype)
        if message is None:
            return []
        return self.get_message_fields(message)


class BinaryMessage(object):
    def __init__(self, fields):
        self._values = dict()
        self._fields = fields

    def add_value(self, name, value):
        self._values[name] = value

    def get_value(self, name):
        if name in self._values:
            return self._values[name]
        return None

    def get_values(self):
        return self._values

    def __str__(self):
        res = ''
        for field in self._fields:
            res += '%s=%s|' % (field['field'], self.get_value(field['field']))
        return res

class BinaryConverter(object):
    def __init__(self, parser, session):
        self._parser = parser
        self._session = session
        self._justify_char = b'\x00'

    def get_fix_from_binary(self, raw_message):
        msgname = self.get_binary_message_name(raw_message)
        message = self._get_binary_message_by_msgname(raw_message, msgname)
        if self._session._debug:
            print ('DECODED:   %s' % message)
        if message is None:
            return None
        fix = self._session.get_msg_header_message()
        msg_fields = self._parser.get_message_fields_by_name(msgname)
        for field in msg_fields:
            if 'tag' in field:
                value = message.get_value(field['field'])
                fix.append_pair(field['tag'], value)
        message_spec = self._parser.get_message_by_name(msgname)
        if 'fixmsgtype' in message_spec:
            fix.append_pair(simplefix.TAG_MSGTYPE, message_spec['fixmsgtype'], header=True)
        return fix

    def get_binary_from_fix(self, fix):
        raw = b''
        msgtype = fix.get('10035')
        if msgtype is None:
            print ('ERROR: please set tag 10035 with binary msgtype')
            return None
        message_fields_spec = self._parser.get_message_fields_by_msgtype(msgtype)
        for field in message_fields_spec:
            if not 'tag' in field:
                print ('WARNING: field %s does not specify a tag to get the value from' % field['field'])
            else:
                value = fix.get(field['tag'])
                if value is None:
                    print ('WARNING: Assigning None to field: %s, please review tag %s' % (field['field'], field['tag']))
                raw = self._add_field_value_to_binary_chunk(raw, field, value)
        if self._session._debug:
            msgname = self.get_binary_message_name(raw)
            message = self._get_binary_message_by_msgname(raw, msgname)
            print ('ENCODED:   %s' % message)
        return raw

    def _get_binary_message_by_msgname(self, binary_message, msgname):
        msg_fields = self._parser.get_message_fields_by_name(msgname)
        message = BinaryMessage(msg_fields)
        chunk_index = 0
        for field in msg_fields:
            type_spec = self._parser.get_type_by_name(field['type'])
            if type_spec is None:
                print ('ERROR: could not find type spec for type: [%s]' % field['type'])
                return None
            if type_spec['pack_as'] == 'left_justify':
                length = int(field['size'])
                total_value = binary_message[chunk_index:chunk_index+length]
                index_filler = total_value.find(self._justify_char)
                value = total_value[0:index_filler]
                chunk_index += length
            else:
                field_size = struct.calcsize(type_spec['pack_as'])
                value = struct.unpack(type_spec['pack_as'], binary_message[chunk_index:chunk_index+field_size])[0]
                #print ('Adding... Field: %s, size = %s, value = %d' % (field['field'], struct.calcsize(type_spec['pack_as']), value))
                chunk_index += field_size
            message.add_value(field['field'], value)
        return message

    def get_binary_message_name(self, binary_message):
        message = self._get_binary_message_by_msgname(binary_message, 'header')
        if not 'msgtype' in message.get_values():
            print ('ERROR: could not find "msgtype" in message: %s' % message)
            return None
        # convert msgtype to string because values in json are strings
        msgtype = '%s' % message.get_value('msgtype')
        #print ('Msgtype = %s' % msgtype)
        message_by_message_type = self._parser.get_message_by_msgtype(msgtype)
        if message_by_message_type is None:
            print ('ERROR: could not find specs for msgtype = %s' % msgtype)

        return message_by_message_type['msgname']

    def get_hex(self, binary_message):
        return ":".join("{0:02x}".format(ord(c)) for c in binary_message)

    def _add_field_value_to_binary_chunk(self, binary_chunk, field, value):
        type_spec = self._parser.get_type_by_name(field['type'])
        if type_spec['pack_as'] == 'left_justify':
            if not isinstance(value, str):
                print ('ERROR: adding value for field [%s]. Value provided [%s] must be a string. Assuming empty string.' % (field, value))
                value = ''
            length = int(field['size'])
            justified_value = value.ljust(length, self._justify_char).encode()
            binary_chunk += value.ljust(length, self._justify_char).encode()
        else:
            try:
                if isinstance(value, bytes) and sys.version_info[0] != 2: #python3 will be bytes
                    value = value.decode()
                if isinstance(value, str):
                    value = int(value)
                binary_chunk += struct.pack(type_spec['pack_as'], value)
            except:
                print ('ERROR: adding %s=%s to binary message' % (field['field'], value))
        return binary_chunk

class BinarySession(FixSessionServer):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        FixSessionServer.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)
        self._protocol_file = None
        self._parser = None
        self._converter = None
        self._debug = False

    def set_debug(self, debug):
        self._debug = debug

    def set_protocol_file(self, protocol_file):
        self._protocol_file = protocol_file

    def load_protocol(self):
        self._parser = BinaryProtocolParser()
        if not self._parser.parse(self._protocol_file):
            print ('Protocol file: %s is not valid.' % self._protocol_file)
            sys.exit(2)
        self._converter = BinaryConverter(self._parser, self)

    def is_not_fix_protocol(self):
        return True

    def convert_outgoing(self, msg):
        # sends binary messages to client
        return self._converter.get_binary_from_fix(msg)

    def convert_incoming(self, msg):
        # converts incoming binary messages to FIX
        return self._converter.get_fix_from_binary(msg)