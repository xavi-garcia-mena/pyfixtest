#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import json
import simplefix
from .logger import StdOutLogger

class MsgJsonTags(object):
    TAG_MESSAGE = "message"
    TAG_MSGTYPE = "msg_type"
    TAG_ENHANCE_DEFAULT = "enhance_default"
    TAG_TAGS = "tags"
    TAG_TAG = "tag"
    TAG_VALUE = "value"
    TAG_VALUE_FROM = "value_from"
    VALUE_UNIQUE = "##UNIQUE##"
    VALUE_UNIQUE_SHORT = '##UNIQUE_SHORT##'
    VALUE_LAST_CL_ORDID = '##LAST_CLORDID##'

class JsonParserBase(object):
    def __init__(self, logger=StdOutLogger()):
        self._logger = logger

    def tag_exists(self, tag, json_data):
        return tag in json_data

    def check_expected_tag(self, tag, json_data):
        if not self.tag_exists(tag, json_data):
            self._logger.add ('Required tag %s not found in %s' % (tag, json_data))
            return False
        return True

    def check_expected_tags(self, tags, json_data):
        result = True
        for tag in tags:
            result = result and self.check_expected_tag(tag, json_data)
        return result

    def check_expected_mutal_exclusive_tags(self, tags, json_data):
        nb_tags_present = 0
        for tag in tags:
            if self.tag_exists(tag, json_data):
                nb_tags_present += 1
        if nb_tags_present != 1:
            self._logger.add ('Value for one (and only one) of the items in [%s] should be found in %s' % (tags, json_data))
            return False
        return True


class JsonMsgParser(JsonParserBase):
    def __init__(self, path, session, logger=StdOutLogger()):
        self._path = path
        self._session = session
        self._enhance_default = False
        self._logger = logger
        self._last_msg_parsed = None
        self._tags_to_recalculate = dict()

    def get_tag_from_preffix(self):
        return 'PYFIXTEST_VALUE_FROM=='

    def parse(self):
        self._tags_to_recalculate = dict()
        self._last_msg_parsed = None
        with open(self._path, 'r') as read_file:
            data = json.load(read_file)
            return self._parse_from_data(data)

    def get_tags(self):
        self._tags_to_recalculate = dict()
        self._last_msg_parsed = None
        with open(self._path, 'r') as read_file:
            data = json.load(read_file)
            return self._retrieve_tags(data)

    def recalculate_dynamic_tags(self):
        if self._last_msg_parsed is None:
            return None
        for tag, value in self._tags_to_recalculate.items():
            self._last_msg_parsed.remove(tag)
            if value == MsgJsonTags.VALUE_UNIQUE:
                self._last_msg_parsed.append_utc_timestamp(tag, precision=6)
            elif value == MsgJsonTags.VALUE_UNIQUE_SHORT:
                self._last_msg_parsed.append_utc_time_only(tag, precision=6)
            elif value == MsgJsonTags.VALUE_LAST_CL_ORDID:
                if self._session.get_last_cl_order_id() != None:
                    self._last_msg_parsed.append_pair(tag, self._session.get_last_cl_order_id())
        return self._last_msg_parsed

    def get_enhance_flag(self):
        return self._enhance_default
    
    def _validate(self, json_data):
        if not self.check_expected_tags( [MsgJsonTags.TAG_MESSAGE], json_data):
            return False

        message = json_data[MsgJsonTags.TAG_MESSAGE]
        if not self.check_expected_tags( [MsgJsonTags.TAG_TAGS, MsgJsonTags.TAG_MSGTYPE], message):
            return False

        for tag in message[MsgJsonTags.TAG_TAGS]:
            if not self.check_expected_tags( [MsgJsonTags.TAG_TAG], tag):
                return False
            if not self.check_expected_mutal_exclusive_tags([MsgJsonTags.TAG_VALUE, MsgJsonTags.TAG_VALUE_FROM], tag):
                return False
        return True

    def _parse_from_data(self, json_data):
        self._tags_to_recalculate = dict()
        if not self._validate(json_data):
            return None
        msgtype = None
        message = json_data[MsgJsonTags.TAG_MESSAGE]
        msgtype = message[MsgJsonTags.TAG_MSGTYPE]
        msg = self._session.get_msg_header_message()
        msg.append_pair(simplefix.TAG_MSGTYPE, msgtype)
        if self.tag_exists(MsgJsonTags.TAG_ENHANCE_DEFAULT, message):
            if message[MsgJsonTags.TAG_ENHANCE_DEFAULT].upper() == "TRUE":
                self._enhance_default = True

        for tag in message[MsgJsonTags.TAG_TAGS]:
            if self.tag_exists(MsgJsonTags.TAG_VALUE, tag):
                if tag[MsgJsonTags.TAG_VALUE] == MsgJsonTags.VALUE_UNIQUE:
                    msg.append_utc_timestamp(tag[MsgJsonTags.TAG_TAG], precision=6)
                    self._tags_to_recalculate[tag[MsgJsonTags.TAG_TAG]] = MsgJsonTags.VALUE_UNIQUE
                elif tag[MsgJsonTags.TAG_VALUE] == MsgJsonTags.VALUE_UNIQUE_SHORT:
                    msg.append_utc_time_only(tag[MsgJsonTags.TAG_TAG], precision=6)
                    self._tags_to_recalculate[tag[MsgJsonTags.TAG_TAG]] = MsgJsonTags.VALUE_UNIQUE_SHORT
                elif tag[MsgJsonTags.TAG_VALUE] == MsgJsonTags.VALUE_LAST_CL_ORDID:
                    if self._session.get_last_cl_order_id() != None:
                        msg.append_pair(tag[MsgJsonTags.TAG_TAG], self._session.get_last_cl_order_id())
                    self._tags_to_recalculate[tag[MsgJsonTags.TAG_TAG]] = MsgJsonTags.VALUE_LAST_CL_ORDID
                else:
                    msg.append_pair(tag[MsgJsonTags.TAG_TAG], tag[MsgJsonTags.TAG_VALUE])
            else:
                msg.append_pair(tag[MsgJsonTags.TAG_TAG], '%s%s' % (self.get_tag_from_preffix(), tag[MsgJsonTags.TAG_VALUE_FROM]))
        self._last_msg_parsed = msg
        return msg

    def _retrieve_tags(self, json_data):
        if not self._validate(json_data):
            return None
        msgtype = None
        tags = []
        message = json_data[MsgJsonTags.TAG_MESSAGE]
        msgtype = message[MsgJsonTags.TAG_MSGTYPE]
        tags.append((simplefix.TAG_MSGTYPE, msgtype))
        if self.tag_exists(MsgJsonTags.TAG_ENHANCE_DEFAULT, message):
            if message[MsgJsonTags.TAG_ENHANCE_DEFAULT].upper() == "TRUE":
                self._enhance_default = True

        aux_msg = simplefix.FixMessage()
        
        for tag in message[MsgJsonTags.TAG_TAGS]:
            if self.tag_exists(MsgJsonTags.TAG_VALUE, tag):
                if tag[MsgJsonTags.TAG_VALUE] == MsgJsonTags.VALUE_UNIQUE:
                    aux_msg.append_utc_timestamp(tag[MsgJsonTags.TAG_TAG], precision=6)
                    self._tags_to_recalculate[tag[MsgJsonTags.TAG_TAG]] = MsgJsonTags.VALUE_UNIQUE
                elif tag[MsgJsonTags.TAG_VALUE] == MsgJsonTags.VALUE_UNIQUE_SHORT:
                    aux_msg.append_utc_time_only(tag[MsgJsonTags.TAG_TAG], precision=6)
                    self._tags_to_recalculate[tag[MsgJsonTags.TAG_TAG]] = MsgJsonTags.VALUE_UNIQUE_SHORT
                elif tag[MsgJsonTags.TAG_VALUE] == MsgJsonTags.VALUE_LAST_CL_ORDID:
                    if self._session.get_last_cl_order_id() != None:
                        aux_msg.append_pair(tag[MsgJsonTags.TAG_TAG], self._session.get_last_cl_order_id())
                    self._tags_to_recalculate[tag[MsgJsonTags.TAG_TAG]] = MsgJsonTags.VALUE_LAST_CL_ORDID
                else:
                    aux_msg.append_pair(tag[MsgJsonTags.TAG_TAG], tag[MsgJsonTags.TAG_VALUE])
            else:
                aux_msg.append_pair(tag[MsgJsonTags.TAG_TAG], '%s%s' % (self.get_tag_from_preffix(), tag[MsgJsonTags.TAG_VALUE_FROM]))
            tags.append((tag[MsgJsonTags.TAG_TAG], aux_msg.get(tag[MsgJsonTags.TAG_TAG])))
        return tags
