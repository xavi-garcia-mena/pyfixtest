#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import sys, getopt, socket, re
from pyfixtest import JsonMsgParser

class ScriptUtils(object):
    def __init__(self, bin_name):
        self._bin_name = bin_name
        self._valid_fix_strings = ['FIX.4.2', 'FIX.4.3', 'FIX.4.4', 'FIXT.1.1']
        self.highlight = None
        self.tag_value_for_fills = None

    def usage(self):
        print ('%s --ip=<IP ADDRESS> --port=<PORT> --fix=<FIX VERSION STRING> --sender_comp_id=<VALUE> --target_comp_id=<VALUE> --outgoing_seq_num=<NUMBER>' % self._bin_name)
        print ('    example: %s --ip=127.0.0.1 --port 6575 --fix=FIX.4.2 --sender_comp_id=SENDER --target_comp_id=TARGET --outgoing_seq_num=14' % self._bin_name)
        print ('    OPTIONAL FLAGS:')
        print ('      --tag_value_for_fills=[TAG,VALUE]')
        print ('      --highlight=[TAG1,TAG2,...,TAGN]')
        print ('      --quote_fill_this_side=1 or 2')
        print ('    TAGS FOR REPLIES DEFINITION:')
        print ('      --on-ack-reply-tags=[FILE_PATH]')
        print ('      --on-fill-reply-tags=[FILE_PATH]')
        print ('      --on-cancel-reply-tags=[FILE_PATH]')
        print ('      --on-replace-reply-tags=[FILE_PATH]')
        print ('      --on-quote-ack-reply-tags=[FILE_PATH]')
        print ('      --on-quote-fill-reply-tags=[FILE_PATH]')
        print ('    PARAMETERS FOR MULTIPLE ORDERS:')
        print ('      --multiple_nb_orders=NUMBER_OF_MULTIPLE_ORDERS_TO_SEND')
        print ('      --multiple_interval=INTERVAL_IN_BETWEEN_ORDERS (in seconds)')

    def verify_ip(self, ip):
        try:
            socket.inet_aton(ip)
        except socket.error:
            print ('ERROR: %s is not a valid IP Address' % ip)
            sys.exit(2)

    def check_ip_provided(self, ip):
        if ip is None:
            print ('ERROR: Listen IP is required')
            self.usage()
            sys.exit(2)
        self.verify_ip(ip)

    def check_provided_fix_string(self, fix):
        if fix is None:
            print ('ERROR: Fix version string is required')
            self.usage()
            sys.exit(2)
        if not fix in self._valid_fix_strings:
            print ('ERROR: Fix version string is not valid')
            print ('     Valid values are: %s' % self._valid_fix_strings)
            sys.exit(2)

    def check_port_provided(self, port):
        if port is None:
            print ('ERROR: Port is required')
            self.usage()
            sys.exit(2)
        try:
            _ = int(port)
        except:
            print ('ERROR: Port %s is not a valid port' % port)
            sys.exit(2)

    def check_sender_comp_id(self, sender_comp_id):
        if sender_comp_id is None:
            print ('ERROR: sender_comp_id is required')
            self.usage()
            sys.exit(2)

    def check_target_comp_id(self, target_comp_id):
        if target_comp_id is None:
            print ('ERROR: target_comp_id is required')
            self.usage()
            sys.exit(2)

    def check_outgoing_seq_num(self, outgoing_seq_num):
        if outgoing_seq_num != None:
            try:
                _ = int(outgoing_seq_num)
            except:
                print ('ERROR: outgoing_seq_num must be an integer')
                sys.exit(2)

    def check_quote_fill_side(self, side):
        if side != 1 and side != 2 and side != '1' and side != '2' and side != None:
            print ('ERROR: quote side [%s] is invalid. Valid values are 1 and 2' % side)
            sys.exit(2)
    
    def check_multiple_params(self, nb_orders, interval):
        try:
            self.multiple_nb_orders = int(nb_orders)
        except:
            print ('ERROR: multiple_nb_orders should be an integer.')
            sys.exit(2)
        try:
            self.multiple_interval = float(interval)
        except:
            print ('ERROR: multiple_interval should be a float number.')
            sys.exit(2)

    def get_multiple_nb_orders(self):
        return self.multiple_nb_orders

    def get_multiple_interval(self):
        return self.multiple_interval

    def get_expected_tag_value_for_fills(self, value):
        if value is None:
            return None
        # value must be [NUMBER,VALUE]
        expr = re.compile(r"^\[\d+,.+\]$")
        if expr.match(value) is None:
            print ('ERROR: expected tag for fills should match the format: [NUMBER,VALUE] where VALUE is an integer')
            self.usage()
            sys.exit(2)
        value = value.replace('[', '')
        value = value.replace(']', '')
        values = value.split(',')
        return (values[0], values[1])
    
    def get_highlighted_tags(self, value):
        if value is None:
            return None
        # value must be [NUMBER,VALUE]
        expr = re.compile(r"^\[(\d+,)*\d+]$")
        if expr.match(value) is None:
            print ('ERROR: expected highlighted tags should match the format: [NUMBER,NUMBER,NUMBER]')
            self.usage()
            sys.exit(2)
        value = value.replace('[', '')
        value = value.replace(']', '')
        values = value.split(',')
        ret_values = []
        for i in values:
            ret_values.append(int(i))
        return ret_values

    def get_msg_tags(self, file_path):
        parser = JsonMsgParser(file_path, None)
        tags = parser.get_tags()
        if tags is None:
            sys.exit(2)
        return (tags, parser.get_enhance_flag())

    def parse_options(self):
        try:
            opts, _ = getopt.getopt(sys.argv[1:], \
                            "h", ["help", \
                                "ip=", \
                                "port=", \
                                "fix=", \
                                "sender_comp_id=", \
                                "target_comp_id=", \
                                "outgoing_seq_num=", \
                                "highlight=", \
                                "tag_value_for_fills=", \
                                "on-ack-reply-tags=", \
                                "on-fill-reply-tags=", \
                                "on-cancel-reply-tags=", \
                                "on-replace-reply-tags=", \
                                "quote_fill_this_side=", \
                                "on-quote-ack-reply-tags=", \
                                "on-quote-fill-reply-tags=", \
                                "multiple_nb_orders=", \
                                "multiple_interval="])
        except getopt.GetoptError:
            self.usage()
            sys.exit(2)

        ip = None
        port = None
        fix = None
        sender_comp_id = None
        target_comp_id = None
        outgoing_seq_num = None
        self.highlight = None
        self.tag_value_for_fills = None
        self.on_ack_reply_tags = None
        self.on_ack_reply_tags_enhance_default = False
        self.on_fill_reply_tags = None
        self.on_fill_reply_tags_enhance_default = False
        self.on_cancel_reply_tags = None
        self.on_cancel_reply_tags_enhance_default = False
        self.on_replace_reply_tags = None
        self.on_replace_reply_tags_enhance_default = False
        self.quote_side = None
        self.on_quote_ack_reply_tags = None
        self.on_quote_ack_reply_tags_enhance_default = False
        self.on_quote_fill_reply_tags = None
        self.on_quote_fill_reply_tags_enhance_default = False
        self.multiple_nb_orders = 10 # default
        self.multiple_interval = 0.1 # default

        for opt, arg in opts:
            if opt in ( '-h', '--help'):
                self.usage()
                sys.exit()
            elif opt == '--ip':
                ip = arg
            elif opt == '--port':
                port = arg
            elif opt == '--fix':
                fix = arg
            elif opt == '--sender_comp_id':
                sender_comp_id = arg
            elif opt == '--target_comp_id':
                target_comp_id = arg
            elif opt == '--outgoing_seq_num':
                outgoing_seq_num = arg
            elif opt == '--highlight':
                self.highlight = self.get_highlighted_tags(arg)
            elif opt == '--tag_value_for_fills':
                self.tag_value_for_fills = self.get_expected_tag_value_for_fills(arg)
            elif opt == '--on-ack-reply-tags':
                tags = self.get_msg_tags(arg)
                self.on_ack_reply_tags = tags[0]
                self.on_ack_reply_tags_enhance_default = tags[1]
            elif opt == '--on-fill-reply-tags':
                tags = self.get_msg_tags(arg)
                self.on_fill_reply_tags = tags[0]
                self.on_fill_reply_tags_enhance_default = tags[1]
            elif opt == '--on-cancel-reply-tags':
                tags = self.get_msg_tags(arg)
                self.on_cancel_reply_tags = tags[0]
                self.on_cancel_reply_tags_enhance_default = tags[1]
            elif opt == '--on-replace-reply-tags':
                tags = self.get_msg_tags(arg)
                self.on_replace_reply_tags = tags[0]
                self.on_replace_reply_tags_enhance_default = tags[1]
            elif opt == '--quote_fill_this_side':
                self.quote_side = arg
            elif opt == '--on-quote-fill-reply-tags':
                tags = self.get_msg_tags(arg)
                self.on_quote_fill_reply_tags = tags[0]
                self.on_quote_fill_reply_tags_enhance_default = tags[1]
            elif opt == '--on-quote-ack-reply-tags':
                tags = self.get_msg_tags(arg)
                self.on_quote_ack_reply_tags = tags[0]
                self.on_quote_ack_reply_tags_enhance_default = tags[1]
            elif opt == '--multiple_nb_orders':
                self.multiple_nb_orders = arg
            elif opt == '--multiple_interval':
                self.multiple_interval = arg
                
        # check parameters
        self.check_ip_provided(ip)
        self.check_port_provided(port)
        self.check_provided_fix_string(fix)
        self.check_sender_comp_id(sender_comp_id)
        self.check_target_comp_id(target_comp_id)
        self.check_outgoing_seq_num(outgoing_seq_num)
        self.check_quote_fill_side(self.quote_side)
        self.check_multiple_params(self.multiple_nb_orders, self.multiple_interval)

        if outgoing_seq_num is None:
            outgoing_seq_num = 1
        else:
            outgoing_seq_num = int(outgoing_seq_num)

        return (ip, int(port), fix, sender_comp_id, target_comp_id, outgoing_seq_num)
