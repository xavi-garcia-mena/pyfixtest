#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from simplefix.constants import TAG_CLORDID, TAG_EXECTRANSTYPE
from .fixsessionbase import FixSessionBase, FixSessionHandler
from .logger import StdOutLogger
import simplefix
import socket

class FixSessionClient(FixSessionBase):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        super(FixSessionClient, self).__init__(fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)
        self._logged_in = False
        self._verify_logon_message = False

    def set_verify_logon_message(self, verify):
        self._verify_logon_message = verify

    def on_logon(self, logon):
        self._logged_in = True
        if self._verify_logon_message:
            if logon.get(simplefix.TAG_SENDER_COMPID) != self._target_comp_id or logon.get(simplefix.TAG_TARGET_COMPID) != self._sender_comp_id:
                self._logger.add ('ERROR: Bad credentials on received logon message.')
                self._logger.add ('       EXPECTED SENDER_COMP_ID : [%s], ACTUAL: [%s]' % (self._target_comp_id, logon.get(simplefix.TAG_SENDER_COMPID)))
                self._logger.add ('       EXPECTED TARGET_COMP_ID : [%s], ACTUAL: [%s]' % (self._sender_comp_id, logon.get(simplefix.TAG_TARGET_COMPID)))
                self._logged_in = False
                self._socket.close()

    def process_message(self, msg):
        msg_type = msg.get(simplefix.TAG_MSGTYPE)
        if msg_type == simplefix.MSGTYPE_EXECUTION_REPORT:
            if msg.get(TAG_EXECTRANSTYPE) == simplefix.EXECTYPE_NEW or msg.get(TAG_EXECTRANSTYPE) == simplefix.EXECTYPE_REPLACE:
                self._last_cl_order_id = msg.get(TAG_CLORDID)
        if msg_type == simplefix.MSGTYPE_LOGON:
            self.on_logon(msg)
            return True
        else:
            return super(FixSessionClient, self).process_message(msg)

class FixClient(FixSessionHandler):
    def __init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id, outgoing_seq_num=1, logger=StdOutLogger()):
        FixSessionHandler.__init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id, outgoing_seq_num, logger)
        self.set_fix_session_class(FixSessionClient)

    def start(self, send_logon=True):
        if self.is_running():
            return
        try:
            self._is_running.set()
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.connect((self._ip, self._port))

            self._session = self._create_new_fix_session(client_socket)

            self._start_commands_thread(self._multiple_nb_orders, self._multiple_interval)
            self._start_session_thread()

            if send_logon:
                # send the logon message
                logon = self._session.get_logon_message()
                self._session.send_message(logon)
            
            self._wait_until_session_ends()

            self._logger.add ('Client session finished')
            self._terminate_commands_thread()
        except KeyboardInterrupt: #pragma nocover
            self._logger.add ('\nStopping...')
            self._terminate_session_thread()
            self._terminate_commands_thread()
            self._is_running.clear()

    def stop(self):
        self._terminate_session_thread()
        self._wait_until_session_ends()
        self._terminate_commands_thread()

