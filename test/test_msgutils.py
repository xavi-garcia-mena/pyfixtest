#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import FixMessageWithHighlightedTags, BufferFixParser
import simplefix
import unittest

class MsgUtilsTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_no_tags(self):
        msg = FixMessageWithHighlightedTags()
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EMAIL)
        self.assertEqual(msg.get_highlighted_encoding([]).decode(), '8=FIX.4.2|9=5|35=C|10=180|')

    def test_tag_highlighted(self):
        msg = FixMessageWithHighlightedTags()
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EMAIL)
        msg.append_pair('44', 'TEST')
        self.assertEqual(msg.get_highlighted_encoding([44]).decode(), '8=FIX.4.2|9=13|35=C|\x1b[92m44=TEST\x1b[0m|10=201|')

    def test_raise_no_msg_type(self):
        msg = FixMessageWithHighlightedTags()
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')
        msg.append_pair('44', 'TEST')
        with self.assertRaises(ValueError) as catched_exception:
            msg.get_highlighted_encoding([])
        self.assertEqual(str(catched_exception.exception) , 'No message type set')

    def test_raise_no_begin_str(self):
        msg = FixMessageWithHighlightedTags()
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EMAIL)
        msg.append_pair('44', 'TEST')
        with self.assertRaises(ValueError) as catched_exception:
            msg.get_highlighted_encoding([])
        self.assertEqual(str(catched_exception.exception) , 'No begin string set')

    def test_body_length_and_checksum(self):
        msg = FixMessageWithHighlightedTags()
        msg_2 = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')
        msg_2.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EMAIL)
        msg_2.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EMAIL)

        msg.append_pair('44', 'test')
        msg_2.append_pair('44', 'test')

        string_1 = msg.get_highlighted_encoding([44]).replace(simplefix.SOH_STR, b'|')
        string_2 = msg_2.encode().replace(simplefix.SOH_STR, b'|')

        checksum_1 = string_1.split(b'10=')[1]
        checksum_2 = string_2.split(b'10=')[1]

        self.assertEqual(checksum_1, checksum_2)

        string_split_1 = msg.get_highlighted_encoding([44]).split(b'|')
        string_split_2 = msg_2.encode().replace(simplefix.SOH_STR, b'|').split(b'|')

        self.assertTrue(len(string_split_1) > 0)
        self.assertTrue(len(string_split_2) > 0)

        self.assertEqual(string_split_1[1], string_split_2[1])
        self.assertTrue(string_split_1[1].startswith(b'9='))

class BufferFixParserTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_find_next_ending_soh(self):
        raw_buffer = b'ONE%(0)sTWO%(0)sTHREE%(0)s10=123%(0)sONE_B%(0)sTWO_B%(0)s10=1234%(0)s' % { b'0' : simplefix.SOH_STR}
        parser = BufferFixParser()

        # find first ending soh
        pos = parser._find_next_ending_soh(raw_buffer, 0)
        self.assertEqual(pos, 20)

        # find second sending soh
        pos = parser._find_next_ending_soh(raw_buffer, pos + 1)
        self.assertEqual(pos, 40)

        # test we get -1 when finding beyond the buffer length
        self.assertEqual(parser._find_next_ending_soh(raw_buffer, pos + 1), -1)

        # test when there's no soh...
        raw_buffer = b'ONE%(0)sTWO%(0)sTHREE10=123%(0)sONE_B%(0)sTWO_B%(0)s10=1234%(0)s' % { b'0' : b'|' }
        self.assertEqual(parser._find_next_ending_soh(raw_buffer, 0), -1)

        # test message has no checksum
        raw_buffer = b'ONE%(0)sTWO%(0)sTHREEXX=123%(0)sONE_B%(0)sTWO_B%(0)sXX=1234%(0)s' % { b'0' : simplefix.SOH_STR}
        self.assertEqual(parser._find_next_ending_soh(raw_buffer, 0), -1)

    def _parse_message_from_raw(self, raw_message):
        parser = simplefix.FixParser()
        parser.append_buffer(raw_message)
        return parser.get_message()

    def test_parse_messages_from_raw(self):
        parser = BufferFixParser()
        fix_message_1 = simplefix.FixMessage()
        fix_message_1.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        fix_message_1.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')
        fix_message_1.append_pair('1', '11')
        fix_message_1.append_pair('2', '22')
        raw_buffer = fix_message_1.encode()

        # split the buffer
        msgs = parser.split_messages_in_buffer(raw_buffer)
        self.assertEqual(len(msgs), 1)
        fix_message_parsed = self._parse_message_from_raw(msgs[0])
        self.assertTrue(fix_message_parsed != None)

        # check that all tags match
        tags_to_check = [simplefix.TAG_MSGTYPE, simplefix.TAG_BEGINSTRING, '1', '2' ]
        for tag in tags_to_check:
            self.assertEqual(fix_message_parsed.get(tag), fix_message_1.get(tag))
        
        # add another message
        fix_message_2 = simplefix.FixMessage()
        fix_message_2.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        fix_message_2.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        fix_message_2.append_pair('1', '111')
        fix_message_2.append_pair('2', '222')
        raw_buffer += fix_message_2.encode()

        # split messages in the buffer
        msgs = parser.split_messages_in_buffer(raw_buffer)
        self.assertEqual(len(msgs), 2)
        fix_message_parsed_1 = self._parse_message_from_raw(msgs[0])
        fix_message_parsed_2 = self._parse_message_from_raw(msgs[1])
        self.assertTrue(fix_message_parsed_1 != None)
        self.assertTrue(fix_message_parsed_2 != None)

        # check tags match
        for tag in tags_to_check:
            self.assertEqual(fix_message_parsed_1.get(tag), fix_message_1.get(tag))
            self.assertEqual(fix_message_parsed_2.get(tag), fix_message_2.get(tag))

        # now add some garbage, so the third message is not valid
        raw_buffer_error = raw_buffer + b'THIS IS NOT FIX'

        # split messages in the buffer
        msgs = parser.split_messages_in_buffer(raw_buffer_error)
        self.assertEqual(len(msgs), 3)
        # parse the last message (which is not valid)
        msg = self._parse_message_from_raw(msgs[2])
        self.assertEqual(msg, None)

        # add an invalid fix message
        fix_message_3 = b'8=BLAH\x019=BLAHBLAH\x0110=12\x01'
        raw_buffer_error = raw_buffer + fix_message_3

        # split messages in the buffer
        msgs = parser.split_messages_in_buffer(raw_buffer_error)
        self.assertEqual(len(msgs), 3)
        # try to encode the last message (which is not valid)
        msg = self._parse_message_from_raw(msgs[2])
        self.assertRaises(ValueError, msg.encode)

        # add an invalid fix message with no = in it
        fix_message_4 = b'8BLAH\x019BLAHBLAH\x0110=12\x01'
        raw_buffer_error = raw_buffer + fix_message_4

        # split messages in the buffer
        msgs = parser.split_messages_in_buffer(raw_buffer_error)
        self.assertEqual(len(msgs), 3)
        # try to encode the last message (which is not valid)
        msg = self._parse_message_from_raw(msgs[2])
        self.assertEqual(msg, None)

    def test_is_message_truncated(self):
        parser = BufferFixParser()
        buff = ''
        self.assertFalse(parser.is_message_truncated(buff))

        buff = 'asasdasdasds10=123\x01'
        self.assertFalse(parser.is_message_truncated(buff))

        buff = '12=123\x0144=12.2\x0155=AB'
        self.assertTrue(parser.is_message_truncated(buff))
   
        buff = '12=123\x0144=12.2\x01'
        self.assertTrue(parser.is_message_truncated(buff))

if __name__ == "__main__":
    unittest.main()

