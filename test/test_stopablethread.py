#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import sys
sys.path.insert(0, '../pyfixtest')

from pyfixtest import StopableThread
import unittest
import time
import threading

class TestThread(StopableThread):
    def __init__(self, number):
        StopableThread.__init__(self, target = self.update_number, args = ())
        self.number = number
        self.running = False

    def update_number(self):
        self.running = True
        while not self.asked_to_terminate():
            self.number += 1
            print ('running')
            self.stop.wait(1)

class StopableThreadTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_thread(self):
        test_thread = TestThread(0)
        test_thread.start()
        timeout = 2.0
        while (not test_thread.running and timeout > 0.0):
            time.sleep(0.1)
            timeout -= 0.1
        self.assertTrue(test_thread.running)
        time.sleep(0.5)
        self.assertTrue(test_thread.number > 0)
        test_thread.terminate()
        test_thread.join()


if __name__ == "__main__":
    unittest.main()
