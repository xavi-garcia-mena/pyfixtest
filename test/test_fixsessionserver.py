#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import FixSessionServer, FixServer, JsonMsgParser, BufferFixParser
import unittest
import simplefix
import threading
import time
import socket
import select
from random import randint
import helper_serverrunner
from contextlib import contextmanager
import sys, os
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO


SENDER_COMP_ID=b"Sender"
TARGET_COMP_ID=b"Target"
FIX_VERSION_STR=b"FIX.4.2"
PORTS_BEGIN = 49152
PORTS_END = 65535

class TestSocket(object):
    def __init__(self):
        self._buffer = None
        self._status = "OPEN"

    def send(self, buff):
        self._buffer = buff

    def get_buffer(self):
        return self._buffer

    def close(self):
        self._status = "CLOSED"

class FixSessionServerTests(unittest.TestCase):
    TEST_REPLY_PATH = "./test_reply.json"
    TEST_FILL_REPLY_PATH = "./test_fill_reply.json"
    TEST_REPLY_QUOTES_AI_PATH = "./test_quotes_ai_reply.json"
    TEST_REPLY_QUOTES_FILL_PATH = "./test_quotes_fill_reply.json"

    def setUp(self):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        self._socket = TestSocket()
        self._session = FixSessionServer(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, self._socket)
        self._session_4_4 = FixSessionServer(b'FIX.4.4', SENDER_COMP_ID, TARGET_COMP_ID, self._socket)
        self._server = None

        json_reply = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value_from" : "1" }, \
                                    { "tag" : "21",   "value_from" : "21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(FixSessionServerTests.TEST_REPLY_PATH, 'w')
        file.write(json_reply)
        file.close()

        json_reply_quotes_ai = '{ "message": \
                            { \
                            "msg_type": "AI", \
                            "enhance_default": "true", \
                            "tags" : \
                            [ \
                                    { "tag" : "12",    "value" : "test_12" }, \
                                    { "tag" : "13",    "value_from" : "1" }, \
                                    { "tag" : "21",    "value_from" : "117" }, \
                                    { "tag" : "1234",  "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        file = open(FixSessionServerTests.TEST_REPLY_QUOTES_AI_PATH, 'w')
        file.write(json_reply_quotes_ai)
        file.close()

        json_reply_quotes_fill = '{ "message": \
                            { \
                            "msg_type": "8", \
                            "enhance_default": "true", \
                            "tags" : \
                            [ \
                                    { "tag" : "12",    "value" : "test_12" }, \
                                    { "tag" : "13",    "value_from" : "1" }, \
                                    { "tag" : "21",    "value_from" : "117" }, \
                                    { "tag" : "1234",  "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        file = open(FixSessionServerTests.TEST_REPLY_QUOTES_FILL_PATH, 'w')
        file.write(json_reply_quotes_fill)
        file.close()

    def tearDown(self):
        os.remove(FixSessionServerTests.TEST_REPLY_PATH)
        os.remove(FixSessionServerTests.TEST_REPLY_QUOTES_AI_PATH)
        os.remove(FixSessionServerTests.TEST_REPLY_QUOTES_FILL_PATH)

    def _get_msg_from_socket(self):
        parser = simplefix.FixParser()
        parser.append_buffer(self._socket.get_buffer())
        msg_sent = parser.get_message()
        return msg_sent

    def test_on_logon(self):
        self._session._outgoing_seq_num = 1999
        self._socket._buffer = None
        self._session.on_logon()
        msg_sent = self._get_msg_from_socket()
        self.assertNotEqual(msg_sent, None)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGSEQNUM), b'1999')
        self.assertEqual(msg_sent.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_LOGON)
        self.assertTrue(len(msg_sent.get(simplefix.TAG_SENDING_TIME)) > 0)

    def _test_basic_execution_report_message(self, execution_report, order, msgtype, fix_version, extra=None):
        self.assertTrue(msgtype in ['ack', 'fill', 'cancel-accept', 'replace-accept'])
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_EXECUTION_REPORT)
        self.assertEqual(execution_report.get(simplefix.TAG_ORDERID), b'tag37-1')
        self.assertEqual(execution_report.get(simplefix.TAG_EXECID), b'tag17-1')
        self.assertEqual(execution_report.get(simplefix.TAG_EXECTRANSTYPE), b'0')
        self.assertEqual(execution_report.get(simplefix.TAG_SYMBOL), order.get(simplefix.TAG_SYMBOL))
        self.assertEqual(execution_report.get(simplefix.TAG_SIDE), order.get(simplefix.TAG_SIDE))
        if msgtype == 'ack':
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'0')
            self.assertEqual(execution_report.get('151'), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), b'0')
        elif msgtype == 'fill':
            if fix_version == 'FIX.4.2':
                self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'2')
            else:
                self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'F')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'2')
            self.assertEqual(execution_report.get('151'), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), order.get(simplefix.TAG_PRICE))
        if msgtype == 'cancel-accept':
            self.assertTrue(extra != None)
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'4')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'4')
            self.assertEqual(execution_report.get('151'), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_CLORDID), extra.get(simplefix.TAG_CLORDID))
            self.assertEqual(execution_report.get(simplefix.TAG_ORIGCLORDID), order.get(simplefix.TAG_CLORDID))
        if msgtype == 'replace-accept':
            self.assertTrue(extra != None)
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'5')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'5')
            self.assertEqual(execution_report.get('151'), extra.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_CLORDID), extra.get(simplefix.TAG_CLORDID))
            self.assertEqual(execution_report.get(simplefix.TAG_ORIGCLORDID), order.get(simplefix.TAG_CLORDID))

    def test_ack_message(self):
        order = simplefix.FixMessage()
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        execution_report = self._session.get_ack_message(order)
        self._test_basic_execution_report_message(execution_report, order, 'ack', 'FIX.4.2')

    def test_ack_message_json_reply(self):
        parser = JsonMsgParser(FixSessionServerTests.TEST_REPLY_PATH, self._session)
        self._session.set_on_ack_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        order = simplefix.FixMessage()
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(1, 'TEST_FROM_1')
        order.append_pair(21, 'TEST_FROM_21')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        execution_report = self._session.get_ack_message(order)
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(execution_report.get('1'), b'test_1')
        self.assertEqual(execution_report.get('11'), order.get('1'))
        self.assertEqual(execution_report.get('21'), order.get('21'))
        self.assertEqual(execution_report.get('1234'), b'test_1234')

    def test_full_fill_message(self):
        order = simplefix.FixMessage()
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        execution_report = self._session.get_full_fill_message(order)
        self._test_basic_execution_report_message(execution_report, order, 'fill', 'FIX.4.2')

    def test_full_fill_message_fix_4_4(self):
        order = simplefix.FixMessage()
        order = self._session_4_4.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        execution_report = self._session_4_4.get_full_fill_message(order)
        self._test_basic_execution_report_message(execution_report, order, 'fill', 'FIX.4.4')

    def test_full_fill_message_json_reply(self):
        parser = JsonMsgParser(FixSessionServerTests.TEST_REPLY_PATH, self._session)
        self._session.set_on_fill_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        order = simplefix.FixMessage()
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(1, 'TEST_FROM_1')
        order.append_pair(21, 'TEST_FROM_21')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        execution_report = self._session.get_full_fill_message(order)
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(execution_report.get('1'), b'test_1')
        self.assertEqual(execution_report.get('11'), order.get('1'))
        self.assertEqual(execution_report.get('21'), order.get('21'))
        self.assertEqual(execution_report.get('1234'), b'test_1234')

    def test_cancel_accept_message(self):
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        cancel = self._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, 'order-id')

        execution_report = self._session.get_cancel_accept_message(order, cancel)
        self._test_basic_execution_report_message(execution_report, order, 'cancel-accept', 'FIX.4.2', cancel)

    def test_cancel_message_json_reply(self):
        parser = JsonMsgParser(FixSessionServerTests.TEST_REPLY_PATH, self._session)
        self._session.set_on_cancel_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        order = simplefix.FixMessage()
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(1, 'TEST_FROM_1')
        order.append_pair(21, 'TEST_FROM_21')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        cancel = self._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, 'order-id')

        execution_report = self._session.get_cancel_accept_message(order, cancel)
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(execution_report.get('1'), b'test_1')
        self.assertEqual(execution_report.get('11'), order.get('1'))
        self.assertEqual(execution_report.get('21'), order.get('21'))
        self.assertEqual(execution_report.get('1234'), b'test_1234')

    def test_replace_accept_message_json_reply(self):
        parser = JsonMsgParser(FixSessionServerTests.TEST_REPLY_PATH, self._session)
        self._session.set_on_replace_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')
        order.append_pair(simplefix.TAG_PRICE, '10')

        replace = self._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_CLORDID, 'cancel-id')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, 'order-id')
        replace.append_pair(simplefix.TAG_ORDERQTY, '1')
        replace.append_pair(simplefix.TAG_PRICE, '1234')

        order.remove(simplefix.TAG_PRICE)
        order.remove(simplefix.TAG_ORDERQTY)
        order.append_pair(simplefix.TAG_PRICE, replace.get(simplefix.TAG_PRICE))
        order.append_pair(simplefix.TAG_ORDERQTY, replace.get(simplefix.TAG_ORDERQTY))

        execution_report = self._session.get_replace_accept_message(order, replace)
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(execution_report.get('1'), b'test_1')
        self.assertEqual(execution_report.get('11'), order.get('1'))
        self.assertEqual(execution_report.get('21'), order.get('21'))
        self.assertEqual(execution_report.get('1234'), b'test_1234')

    def test_replace_accept_message(self):
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')
        order.append_pair(simplefix.TAG_PRICE, '10')

        replace = self._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_CLORDID, 'cancel-id')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, 'order-id')
        replace.append_pair(simplefix.TAG_ORDERQTY, '1')
        replace.append_pair(simplefix.TAG_PRICE, '1234')

        order.remove(simplefix.TAG_PRICE)
        order.remove(simplefix.TAG_ORDERQTY)
        order.append_pair(simplefix.TAG_PRICE, replace.get(simplefix.TAG_PRICE))
        order.append_pair(simplefix.TAG_ORDERQTY, replace.get(simplefix.TAG_ORDERQTY))

        execution_report = self._session.get_replace_accept_message(order, replace)
        self._test_basic_execution_report_message(execution_report, order, 'replace-accept', 'FIX.4.2', replace)

    def test_replace_accept_message_and_cancel(self):
        order = self._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')
        order.append_pair(simplefix.TAG_PRICE, '10')

        replace = self._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_CLORDID, 'replace-id')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, 'order-id')
        replace.append_pair(simplefix.TAG_ORDERQTY, '1')
        replace.append_pair(simplefix.TAG_PRICE, '1234')

        order.remove(simplefix.TAG_PRICE)
        order.remove(simplefix.TAG_ORDERQTY)
        order.append_pair(simplefix.TAG_PRICE, replace.get(simplefix.TAG_PRICE))
        order.append_pair(simplefix.TAG_ORDERQTY, replace.get(simplefix.TAG_ORDERQTY))

        execution_report = self._session.get_replace_accept_message(order, replace)
        self._test_basic_execution_report_message(execution_report, order, 'replace-accept', 'FIX.4.2', replace)

        # cancel
        cancel = self._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, 'replace-id')

        execution_report = self._session.get_cancel_accept_message(order, cancel)
        self._test_basic_execution_report_message(execution_report, order, 'cancel-accept', 'FIX.4.2', cancel)
    
    def _check_quote_status_report(self, quote, ai):
        self.assertEqual(ai.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_QUOTE_STATUS_REPORT)
        self.assertNotEqual(ai.get(simplefix.TAG_TRANSACTTIME), None)
        self.assertNotEqual(ai.get(simplefix.TAG_TRANSACTTIME), b'')
        self.assertEqual(ai.get(simplefix.TAG_CURRENCY), quote.get(simplefix.TAG_CURRENCY))
        self.assertEqual(ai.get(simplefix.TAG_ACCOUNT), quote.get(simplefix.TAG_ACCOUNT))
        self.assertEqual(ai.get(simplefix.TAG_QUOTEID), quote.get(simplefix.TAG_QUOTEID))
        self.assertEqual(ai.get('297'), b'0')

    def test_quote_status_report_message(self):
        quote = self._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')
        ai = self._session.get_quote_status_report_message(quote)
        self._check_quote_status_report(quote, ai)

    def test_quote_status_report_message_json_reply(self):
        parser = JsonMsgParser(FixSessionServerTests.TEST_REPLY_QUOTES_AI_PATH, self._session)
        self._session.set_on_quote_ack_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        quote = self._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        ai = self._session.get_quote_status_report_message(quote)
        self._check_quote_status_report(quote, ai)

        self.assertEqual(ai.get('12'), b'test_12')
        self.assertEqual(ai.get('13'), quote.get('1'))
        self.assertEqual(ai.get('21'), quote.get('117'))
        self.assertEqual(ai.get('1234'), b'test_1234')

    def _compare_quote_fills_tags(self, quote, fill, side):
        self.assertEqual(fill.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_EXECUTION_REPORT)
        self.assertEqual(fill.get(simplefix.TAG_ORDERID), b'tag37-1')
        self.assertEqual(fill.get(simplefix.TAG_EXECID), b'tag17-1')
        self.assertEqual(fill.get(simplefix.TAG_EXECTRANSTYPE), b'0')
        self.assertEqual(fill.get(simplefix.TAG_SYMBOL), quote.get(simplefix.TAG_SYMBOL))
        self.assertEqual(fill.get(simplefix.TAG_CLORDID), quote.get(simplefix.TAG_QUOTEID))
        self.assertEqual(fill.get(simplefix.TAG_EXECTYPE), b'2')
        self.assertEqual(fill.get(simplefix.TAG_ORDSTATUS), b'2')
        self.assertEqual(fill.get('151'), b'0')
        self.assertIn(side, [1, '1', 2, '2'])
        if side == 1 or side == '1':
            self.assertEqual(fill.get('54'), b'1')
            self.assertEqual(fill.get(simplefix.TAG_LASTPX), quote.get('132'))
            self.assertEqual(fill.get(simplefix.TAG_PRICE), quote.get('132'))
            self.assertEqual(fill.get(simplefix.TAG_AVGPX), quote.get('132'))
            self.assertEqual(fill.get(simplefix.TAG_LASTQTY), quote.get('134'))
            self.assertEqual(fill.get(simplefix.TAG_CUMQTY), quote.get('134'))
            self.assertEqual(fill.get(simplefix.TAG_ORDERQTY), quote.get('134'))
        else:
            self.assertEqual(fill.get('54'), b'2')
            self.assertEqual(fill.get(simplefix.TAG_LASTPX), quote.get('133'))
            self.assertEqual(fill.get(simplefix.TAG_PRICE), quote.get('133'))
            self.assertEqual(fill.get(simplefix.TAG_AVGPX), quote.get('133'))
            self.assertEqual(fill.get(simplefix.TAG_LASTQTY), quote.get('135'))
            self.assertEqual(fill.get(simplefix.TAG_ORDERQTY), quote.get('135'))

    def test_quote_full_fill_message(self):
        quote = self._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        side = 1
        fill = self._session.get_quote_full_fill_message(quote, side)
        self._compare_quote_fills_tags(quote, fill, side)
        side = 2
        fill = self._session.get_quote_full_fill_message(quote, side)
        self._compare_quote_fills_tags(quote, fill, side)

    def test_quote_full_fill_message_json_reply(self):
        parser = JsonMsgParser(FixSessionServerTests.TEST_REPLY_QUOTES_FILL_PATH, self._session)
        self._session.set_on_quote_fill_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        quote = self._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        side = 1
        fill = self._session.get_quote_full_fill_message(quote, side)
        self._compare_quote_fills_tags(quote, fill, side)
        side = 2
        fill = self._session.get_quote_full_fill_message(quote, side)
        self._compare_quote_fills_tags(quote, fill, side)

        self.assertEqual(fill.get('12'), b'test_12')
        self.assertEqual(fill.get('13'), quote.get('1'))
        self.assertEqual(fill.get('21'), quote.get('117'))
        self.assertEqual(fill.get('1234'), b'test_1234')

    def test_quote_full_fill_message_bad_side(self):
        quote = self._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        side = 'BAD_SIDE_VAL'
        with self.assertRaises(ValueError) as cm:
            _ = self._session.get_quote_full_fill_message(quote, side)

class FixSessionServerSyntaxError(FixSessionServer):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, client_socket, outgoing_seq_num):
        super(FixSessionServerSyntaxError, self).__init__(fix_version_string, sender_comp_id, target_comp_id, client_socket, outgoing_seq_num)

    def on_logon(msg, blah):
        print ('will never be executed as this method only accepts 1 parameter')

class FixServerSyntaxError(FixServer):
    def __init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id):
        super(FixServerSyntaxError, self).__init__(ip, port, fix_version_string, sender_comp_id, target_comp_id)
        self.set_fix_session_class(FixSessionServerSyntaxError)

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class FixServerTests(helper_serverrunner.HelperServerRunner):
    TEST_REPLY_PATH = './json_reply.json'
    TEST_REPLY_ENHANCE_PATH = './json_reply_enhance.json'
    TEST_REPLY_AI_ENHANCE_PATH = './json_reply_ai_enhance.json'
    TEST_REPLY_QUOTE_FILL_ENHANCE_PATH = './json_reply_quote_fill_enhance.json'
    def setUp(self):
        json_reply = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "111",   "value_from" : "1" }, \
                                    { "tag" : "21",   "value_from" : "1234" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(FixServerTests.TEST_REPLY_PATH, 'w')
        file.write(json_reply)
        file.close()

        json_reply_enhance = '{ "message": \
                            { \
                            "msg_type": "8", \
                            "enhance_default" : "true", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "111",   "value_from" : "1" }, \
                                    { "tag" : "21",   "value_from" : "1234" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(FixServerTests.TEST_REPLY_ENHANCE_PATH, 'w')
        file.write(json_reply_enhance)
        file.close()

        json_reply_ai_enhance = '{ "message": \
                            { \
                            "msg_type": "AI", \
                            "enhance_default": "true", \
                            "tags" : \
                            [ \
                                    { "tag" : "12",    "value" : "test_12" }, \
                                    { "tag" : "13",    "value_from" : "1" }, \
                                    { "tag" : "21",    "value_from" : "117" }, \
                                    { "tag" : "1234",  "value" : "test_1234" } \
                            ] \
                            } \
                         }'
        file = open(FixServerTests.TEST_REPLY_AI_ENHANCE_PATH, 'w')
        file.write(json_reply_ai_enhance)
        file.close()

        json_reply_quote_fill_enhance = '{ "message": \
                            { \
                            "msg_type": "8", \
                            "enhance_default": "true", \
                            "tags" : \
                            [ \
                                    { "tag" : "12",    "value" : "test_12" }, \
                                    { "tag" : "13",    "value_from" : "1" }, \
                                    { "tag" : "21",    "value_from" : "117" }, \
                                    { "tag" : "1234",  "value" : "test_1234" } \
                            ] \
                            } \
                         }'
        file = open(FixServerTests.TEST_REPLY_QUOTE_FILL_ENHANCE_PATH, 'w')
        file.write(json_reply_quote_fill_enhance)
        file.close()

        self.server = None
        self.client = None

    def tearDown(self):
        os.remove(FixServerTests.TEST_REPLY_PATH)
        os.remove(FixServerTests.TEST_REPLY_ENHANCE_PATH)
        os.remove(FixServerTests.TEST_REPLY_AI_ENHANCE_PATH)
        os.remove(FixServerTests.TEST_REPLY_QUOTE_FILL_ENHANCE_PATH)
        if self.client != None:
            self.client.close()
        if self.server != None:
            self.server.stop()


    def _get_server(self, port, expected_tag_fills=None, highlighted_tags=None):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        server = FixServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)
        if expected_tag_fills != None:
            server.set_expected_tag_in_new_orders_for_fills(expected_tag_fills[0], expected_tag_fills[1])
        if highlighted_tags != None:
            server.set_highlighted_tags(highlighted_tags)
        return server

    def _get_server_quote_buy_fill_side(self, port, expected_tag_fills=None, highlighted_tags=None):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        server = FixServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)
        server.set_quote_fill_only_side(1)
        if expected_tag_fills != None:
            server.set_expected_tag_in_new_orders_for_fills(expected_tag_fills[0], expected_tag_fills[1])
        if highlighted_tags != None:
            server.set_highlighted_tags(highlighted_tags)
        return server

    def _get_syntax_error_server(self, port):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        return FixServerSyntaxError('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)

    def test_syntax_error_in_session_code(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_syntax_error_server(port))
        self.client = self._connect_client(port)
        logon = simplefix.FixMessage()
        logon.append_pair(simplefix.TAG_BEGINSTRING, FIX_VERSION_STR, header=True)
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon.append_pair(simplefix.TAG_SENDER_COMPID, TARGET_COMP_ID, header=True)
        logon.append_pair(simplefix.TAG_TARGET_COMPID, SENDER_COMP_ID, header=True)
        logon.append_utc_timestamp(simplefix.TAG_SENDING_TIME, header=True)
        logon.append_pair(simplefix.TAG_MSGSEQNUM, '1', header=True)

        # check that the user gets the error on stdout
        with captured_output() as (out, _):
            self.client.send(logon.encode())
            time.sleep(0.5)
            output = out.getvalue().strip()
        print ('----')
        print (output)
        print ('----')
        if sys.version_info[0] == 2:
            self.assertNotEqual(output.find("on_logon() takes exactly 2 arguments (1 given)"), -1)
        else:
            self.assertNotEqual(output.find("on_logon() missing 1 required positional argument: 'blah'"), -1)


    def test_stop_after_connected(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        time.sleep(0.5)
        client.close()
        self.server.stop()
        self._wait_server_not_running(self.server)
        self.assertEqual(self.server.is_running(), False)
        self.server = None

    def test_run_again(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.server.start() # try to start again... this should just return
        time.sleep(0.5)

    def test_close_client_connection(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self._connect_client(port)
        time.sleep(0.5)
        self.server.stop()
        self._wait_server_not_running(self.server)
        self.assertEqual(self.server.is_running(), False)

    def test_client_closes_connection(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        time.sleep(0.5)
        with captured_output() as (out, _):
            client.close()
            time.sleep(0.5)
            output = out.getvalue().strip()
        print ('----')
        print (output)
        print ('----')
        self.assertNotEqual(output.find("Connection closed"), -1)

    def test_client_sends_garbage(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        time.sleep(0.5)
        with captured_output() as (out, _):
            self.client.send(b'THIS IS A TEST')
            output = out.getvalue().strip()
        self.assertEqual("", output)
        time.sleep(0.5)

    def _test_basic_message(self, msgtype):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        logon = simplefix.FixMessage()
        logon.append_pair(simplefix.TAG_BEGINSTRING, FIX_VERSION_STR, header=True)
        logon.append_pair(simplefix.TAG_MSGTYPE, msgtype)
        logon.append_pair(simplefix.TAG_SENDER_COMPID, TARGET_COMP_ID, header=True)
        logon.append_pair(simplefix.TAG_TARGET_COMPID, SENDER_COMP_ID, header=True)
        logon.append_utc_timestamp(simplefix.TAG_SENDING_TIME, header=True)
        logon.append_pair(simplefix.TAG_MSGSEQNUM, '1', header=True)
        if msgtype == simplefix.MSGTYPE_TEST_REQUEST:
            logon.append_pair(simplefix.TAG_TESTREQID, 'ABCD')
        self.client.send(logon.encode())
        reply_raw = self.client.recv(1024)

        parser = simplefix.FixParser()
        parser.append_buffer(reply_raw)
        reply = parser.get_message()

        self.assertTrue(reply != None)
        if msgtype == simplefix.MSGTYPE_TEST_REQUEST:
            self.assertEqual(reply.get(simplefix.TAG_TESTREQID), b'ABCD')
            self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_HEARTBEAT)
        else:
            self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), msgtype)
        self.assertEqual(reply.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(reply.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(reply.get(simplefix.TAG_MSGSEQNUM), b'1')
        self.assertTrue(len(reply.get(simplefix.TAG_SENDING_TIME)) > 0)

    def _test_quote_status_report(self, quote, ai):
        self.assertEqual(ai.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_QUOTE_STATUS_REPORT)
        self.assertNotEqual(ai.get(simplefix.TAG_TRANSACTTIME), None)
        self.assertNotEqual(ai.get(simplefix.TAG_TRANSACTTIME), b'')
        self.assertEqual(ai.get(simplefix.TAG_CURRENCY), quote.get(simplefix.TAG_CURRENCY))
        self.assertEqual(ai.get(simplefix.TAG_ACCOUNT), quote.get(simplefix.TAG_ACCOUNT))
        self.assertEqual(ai.get(simplefix.TAG_QUOTEID), quote.get(simplefix.TAG_QUOTEID))
        self.assertEqual(ai.get('297'), b'0')

    def _test_quote_full_fill(self, quote, fill, side):
        self.assertEqual(fill.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_EXECUTION_REPORT)
        self.assertTrue(fill.get(simplefix.TAG_ORDERID).startswith(b'tag37-'))
        self.assertTrue(fill.get(simplefix.TAG_EXECID).startswith(b'tag17-'))
        self.assertEqual(fill.get(simplefix.TAG_EXECTRANSTYPE), b'0')
        self.assertEqual(fill.get(simplefix.TAG_SYMBOL), quote.get(simplefix.TAG_SYMBOL))
        self.assertEqual(fill.get(simplefix.TAG_CLORDID), quote.get(simplefix.TAG_QUOTEID))
        self.assertEqual(fill.get(simplefix.TAG_EXECTYPE), b'2')
        self.assertEqual(fill.get(simplefix.TAG_ORDSTATUS), b'2')
        self.assertEqual(fill.get('151'), b'0')
        self.assertIn(side, [1, '1', 2, '2'])
        if side == 1 or side == '1':
            self.assertEqual(fill.get('54'), b'1')
            self.assertEqual(fill.get(simplefix.TAG_LASTPX), quote.get('132'))
            self.assertEqual(fill.get(simplefix.TAG_PRICE), quote.get('132'))
            self.assertEqual(fill.get(simplefix.TAG_AVGPX), quote.get('132'))
            self.assertEqual(fill.get(simplefix.TAG_LASTQTY), quote.get('134'))
            self.assertEqual(fill.get(simplefix.TAG_CUMQTY), quote.get('134'))
            self.assertEqual(fill.get(simplefix.TAG_ORDERQTY), quote.get('134'))
        else:
            self.assertEqual(fill.get('54'), b'2')
            self.assertEqual(fill.get(simplefix.TAG_LASTPX), quote.get('133'))
            self.assertEqual(fill.get(simplefix.TAG_PRICE), quote.get('133'))
            self.assertEqual(fill.get(simplefix.TAG_AVGPX), quote.get('133'))
            self.assertEqual(fill.get(simplefix.TAG_LASTQTY), quote.get('135'))
            self.assertEqual(fill.get(simplefix.TAG_CUMQTY), quote.get('135'))
            self.assertEqual(fill.get(simplefix.TAG_ORDERQTY), quote.get('135'))

    def _get_replies(self, sock, session, num_replies, time_wait=1):
        replies_recv = []
        while len(replies_recv) < num_replies:
            replies = self._get_next_reply(sock, session, num_replies - len(replies_recv), time_wait)
            for reply in replies:
                replies_recv.append(reply)
        # verify that no other reply is sent
        ready_to_read, _, _ = select.select([sock], [], [], 0.2)
        self.assertTrue(len(ready_to_read) == 0)
        self.assertEqual(len(replies_recv), num_replies)
        return replies_recv
            
    def _get_next_reply(self, sock, session, max_replies, time_wait):
        buffer_parser = BufferFixParser()
        time_elapsed = 0.0
        gotData = False
        while (not gotData and time_wait > time_elapsed):
            ready_to_read, _, _ = select.select([sock], [], [], 0.1)
            if len(ready_to_read) > 0:
                gotData = True
            else: 
                time_elapsed += 0.1
        self.assertTrue(gotData)
        reply_raw_buffer = session.read_buffer()
        replies = buffer_parser.split_messages_in_buffer(reply_raw_buffer)
        self.assertTrue(len(replies) > 0)
        self.assertTrue(len(replies) < max_replies + 1)
        return replies

    def test_logon(self):
        self._test_basic_message(simplefix.MSGTYPE_LOGON)

    def test_heartbeat(self):
        self._test_basic_message(simplefix.MSGTYPE_HEARTBEAT)

    def test_logout(self):
        self._test_basic_message(simplefix.MSGTYPE_LOGOUT)

    def test_test_request(self):
        self._test_basic_message(simplefix.MSGTYPE_TEST_REQUEST)

    def test_on_resend_request(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, self.client)
        self._wait_client_connected(self.server)

        resend_request = client_session.get_msg_header_message()
        resend_request.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_RESEND_REQUEST)
        resend_request.append_pair(simplefix.TAG_BEGINSEQNO, '1')
        resend_request.append_pair(simplefix.TAG_ENDSEQNO, '0')
        
        self.client.send(resend_request.encode())

        replies = self._get_replies(self.client,client_session, 1)
        reply = self._parse_fix_msg(replies[0])
        self.assertTrue(reply != None)
        self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_SEQUENCE_RESET)
        self.assertEqual(reply.get(simplefix.TAG_NEWSEQNO), b'1')
        self.assertEqual(reply.get(simplefix.TAG_GAPFILLFLAG), b'Y')
        self.assertEqual(reply.get(simplefix.TAG_MSGSEQNUM), resend_request.get(simplefix.TAG_BEGINSEQNO))
        
        self.client.send(resend_request.encode())

        replies = self._get_replies(self.client,client_session, 1)
        reply = self._parse_fix_msg(replies[0])
        self.assertTrue(reply != None)
        self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_SEQUENCE_RESET)
        self.assertEqual(reply.get(simplefix.TAG_NEWSEQNO), b'2')
        self.assertEqual(reply.get(simplefix.TAG_GAPFILLFLAG), b'Y')
        self.assertEqual(reply.get(simplefix.TAG_MSGSEQNUM), resend_request.get(simplefix.TAG_BEGINSEQNO))

        # more realistic test case. Seq num in server is greater than in client
        self.server._session._outgoing_seq_num = 1999
        self.client.send(resend_request.encode())
        replies = self._get_replies(self.client,client_session, 1)
        reply = self._parse_fix_msg(replies[0])
        self.assertTrue(reply != None)
        self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_SEQUENCE_RESET)
        self.assertEqual(reply.get(simplefix.TAG_NEWSEQNO), b'1999')
        self.assertEqual(reply.get(simplefix.TAG_GAPFILLFLAG), b'Y')
        self.assertEqual(reply.get(simplefix.TAG_MSGSEQNUM), resend_request.get(simplefix.TAG_BEGINSEQNO))

        hb = client_session.get_heartbeat_message()
        self.client.send(hb.encode())
        replies = self._get_replies(self.client,client_session, 1)
        reply = self._parse_fix_msg(replies[0])
        self.assertTrue(reply != None)
        self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_HEARTBEAT)
        self.assertEqual(reply.get(simplefix.TAG_MSGSEQNUM), b'1999')

    def test_not_handled_msgtype(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        logon = simplefix.FixMessage()
        logon.append_pair(simplefix.TAG_BEGINSTRING, FIX_VERSION_STR, header=True)
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_XML_MESSAGE)
        logon.append_pair(simplefix.TAG_SENDER_COMPID, TARGET_COMP_ID, header=True)
        logon.append_pair(simplefix.TAG_TARGET_COMPID, SENDER_COMP_ID, header=True)
        logon.append_utc_timestamp(simplefix.TAG_SENDING_TIME, header=True)
        logon.append_pair(simplefix.TAG_MSGSEQNUM, '1', header=True)
        self.client.send(logon.encode())
        ready_to_read, _, _ = select.select([self.client], [], [], 1)
        self.assertEqual(len(ready_to_read), 0)

    def _parse_fix_msg(self, raw_msg):
        parser = simplefix.FixParser()
        parser.append_buffer(raw_msg)
        msg = parser.get_message()
        return msg

    def _test_basic_execution_report_message(self, execution_report, order, msgtype, fix_version, extra=None):
        self.assertTrue(msgtype in [ 'ack', 'fill', 'cancel-accept', 'replace-accept' ])
        seq_num = execution_report.get(simplefix.TAG_MSGSEQNUM)
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_EXECUTION_REPORT)
        self.assertEqual(execution_report.get(simplefix.TAG_ORDERID), b'tag37-%s' % seq_num)
        self.assertEqual(execution_report.get(simplefix.TAG_EXECID), b'tag17-%s' % seq_num)
        self.assertEqual(execution_report.get(simplefix.TAG_EXECTRANSTYPE), b'0')
        self.assertEqual(execution_report.get(simplefix.TAG_SYMBOL), order.get(simplefix.TAG_SYMBOL))
        self.assertEqual(execution_report.get(simplefix.TAG_SIDE), order.get(simplefix.TAG_SIDE))
        if msgtype == 'ack':
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'0')
            self.assertEqual(execution_report.get('151'), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), b'0')
        elif msgtype == 'fill':
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'2')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'2')
            self.assertEqual(execution_report.get('151'), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_LASTQTY), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), order.get(simplefix.TAG_PRICE))
        if msgtype == 'cancel-accept':
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'4')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'4')
            self.assertEqual(execution_report.get('151'), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_ORIGCLORDID), order.get(simplefix.TAG_CLORDID))
            self.assertTrue(extra != None)
            self.assertEqual(execution_report.get(simplefix.TAG_CLORDID), extra.get(simplefix.TAG_CLORDID))
        if msgtype == 'replace-accept':
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), b'5')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), b'5')
            self.assertEqual(execution_report.get('151'), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), b'0')
            self.assertEqual(execution_report.get(simplefix.TAG_CLORDID), order.get(simplefix.TAG_CLORDID))
            self.assertTrue(extra != None)
            self.assertEqual(execution_report.get(simplefix.TAG_CLORDID), extra.get(simplefix.TAG_CLORDID))
            self.assertEqual(execution_report.get(simplefix.TAG_ORIGCLORDID), extra.get(simplefix.TAG_ORIGCLORDID))

    def test_new_order_single_ack(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

    def test_new_order_single_ack_json_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_PATH, None)
        self.server.set_on_ack_reply_tags(parser.get_tags())
        client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        client.send(order.encode())
        replies = self._get_replies(client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        
        self.assertEqual(ack.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(ack.get('1'), b'test_1')
        self.assertEqual(ack.get('111'), order.get('1'))
        self.assertEqual(ack.get('21'), order.get('11'))
        self.assertEqual(ack.get('1234'), b'test_1234')

    def test_new_order_single_ack_json_enhance_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_ENHANCE_PATH, None)
        self.server.set_on_ack_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')

        client.send(order.encode())
        replies = self._get_replies(client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')
        self.assertEqual(ack.get('1'), b'test_1')
        self.assertEqual(ack.get('111'), order.get('1'))
        self.assertEqual(ack.get('21'), order.get('11'))
        self.assertEqual(ack.get('1234'), b'test_1234')

    def test_cancel_request(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        cancel = simplefix.FixMessage()
        cancel = self.server._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REQUEST)
        cancel.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        cancel.append_pair(simplefix.TAG_ORDERQTY, '100')
        cancel.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, ack.get(simplefix.TAG_CLORDID))
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')

        self.client.send(cancel.encode())
        replies = self._get_replies(self.client,client_session, 1)
        cancel_accept = self._parse_fix_msg(replies[0])
        self.assertTrue(cancel_accept != None)
        
        self._test_basic_execution_report_message(cancel_accept, order, 'cancel-accept', 'FIX.4.2', cancel)

    def test_cancel_request_json_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_PATH, None)
        self.server.set_on_cancel_reply_tags(parser.get_tags())
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        cancel = simplefix.FixMessage()
        cancel = self.server._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REQUEST)
        cancel.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        cancel.append_pair(simplefix.TAG_ORDERQTY, '100')
        cancel.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, ack.get(simplefix.TAG_CLORDID))
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')

        self.client.send(cancel.encode())
        replies = self._get_replies(self.client,client_session, 1)
        cancel_accept = self._parse_fix_msg(replies[0])
        self.assertTrue(cancel_accept != None)
        
        self.assertEqual(cancel_accept.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(cancel_accept.get('1'), b'test_1')
        self.assertEqual(cancel_accept.get('111'), order.get('1'))
        self.assertEqual(cancel_accept.get('21'), order.get('1234'))
        self.assertEqual(cancel_accept.get('1234'), b'test_1234')

    def test_cancel_request_json_enhanced_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_ENHANCE_PATH, None)
        self.server.set_on_cancel_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')
        order.append_pair(simplefix.TAG_ACCOUNT, 'account-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        cancel = simplefix.FixMessage()
        cancel = self.server._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REQUEST)
        cancel.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        cancel.append_pair(simplefix.TAG_ORDERQTY, '100')
        cancel.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, ack.get(simplefix.TAG_CLORDID))
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')
        cancel.append_pair(simplefix.TAG_ACCOUNT, 'account-id')

        self.client.send(cancel.encode())
        replies = self._get_replies(self.client,client_session, 1)
        cancel_accept = self._parse_fix_msg(replies[0])
        self.assertTrue(cancel_accept != None)
        
        self._test_basic_execution_report_message(cancel_accept, order, 'cancel-accept', 'FIX.4.2', cancel)
        self.assertEqual(cancel_accept.get('1'), b'test_1')
        self.assertEqual(cancel_accept.get('111'), order.get('1'))
        self.assertEqual(cancel_accept.get('21'), order.get('1234'))
        self.assertEqual(cancel_accept.get('1234'), b'test_1234')

    def test_replace_request(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_PRICE, '1')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        replace = simplefix.FixMessage()
        replace = self.server._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REPLACE_REQUEST)
        replace.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        replace.append_pair(simplefix.TAG_ORDERQTY, '1000')
        replace.append_pair(simplefix.TAG_PRICE, '123')
        replace.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, ack.get(simplefix.TAG_CLORDID))
        replace.append_pair(simplefix.TAG_CLORDID, 'replace-id')

        self.client.send(replace.encode())
        replies = self._get_replies(self.client,client_session, 1)
        replace_accept = self._parse_fix_msg(replies[0])
        self.assertTrue(replace_accept != None)

        found = False
        for order in self.server._session._open_orders:
            if order.get(simplefix.TAG_CLORDID) == replace.get(simplefix.TAG_CLORDID):
                print ('order id in store: %s' % order.get(simplefix.TAG_CLORDID))
                found = True
                self._test_basic_execution_report_message(replace_accept, order, 'replace-accept', 'FIX.4.2', replace)
        self.assertEqual(found, True)

    def test_replace_request_json_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_PATH, None)
        self.server.set_on_replace_reply_tags(parser.get_tags())
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_PRICE, '1')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        replace = simplefix.FixMessage()
        replace = self.server._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REPLACE_REQUEST)
        replace.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        replace.append_pair(simplefix.TAG_ORDERQTY, '1000')
        replace.append_pair(simplefix.TAG_PRICE, '123')
        replace.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, ack.get(simplefix.TAG_CLORDID))
        replace.append_pair(simplefix.TAG_CLORDID, 'cancel-id')

        self.client.send(replace.encode())
        replies = self._get_replies(self.client,client_session, 1)
        replace_accept = self._parse_fix_msg(replies[0])
        self.assertTrue(replace_accept != None)

        self.assertEqual(replace_accept.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(replace_accept.get('1'), b'test_1')
        self.assertEqual(replace_accept.get('111'), order.get('1'))
        self.assertEqual(replace_accept.get('21'), order.get('1234'))
        self.assertEqual(replace_accept.get('1234'), b'test_1234')

    def test_replace_request_json_enhanced_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_ENHANCE_PATH, None)
        self.server.set_on_replace_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_PRICE, '1')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')
        order.append_pair('1234', 'test_1234')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        replace = simplefix.FixMessage()
        replace = self.server._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REPLACE_REQUEST)
        replace.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        replace.append_pair(simplefix.TAG_ORDERQTY, '1000')
        replace.append_pair(simplefix.TAG_PRICE, '123')
        replace.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, ack.get(simplefix.TAG_CLORDID))
        replace.append_pair(simplefix.TAG_CLORDID, 'cancel-id')

        self.client.send(replace.encode())
        replies = self._get_replies(self.client,client_session, 1)
        replace_accept = self._parse_fix_msg(replies[0])
        self.assertTrue(replace_accept != None)

        found = False
        for order in self.server._session._open_orders:
            if order.get(simplefix.TAG_CLORDID) == replace.get(simplefix.TAG_CLORDID):
                found = True
                self._test_basic_execution_report_message(replace_accept, order, 'replace-accept', 'FIX.4.2', replace)
                self.assertEqual(replace_accept.get('1'), b'test_1')
                self.assertEqual(replace_accept.get('111'), order.get('1'))
                self.assertEqual(replace_accept.get('21'), order.get('1234'))
                self.assertEqual(replace_accept.get('1234'), b'test_1234')
        self.assertEqual(found, True)

    def test_cancel_request_reject(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        cancel = simplefix.FixMessage()
        cancel = self.server._session.get_msg_header_message()
        cancel.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REQUEST)
        cancel.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        cancel.append_pair(simplefix.TAG_ORDERQTY, '100')
        cancel.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        cancel.append_pair(simplefix.TAG_ORIGCLORDID, 'bad-id')
        cancel.append_pair(simplefix.TAG_CLORDID, 'cancel-id')

        self.client.send(cancel.encode())
        replies = self._get_replies(self.client,client_session, 1)
        cancel_reject = self._parse_fix_msg(replies[0])
        self.assertTrue(cancel_reject != None)

        self.assertEqual(cancel_reject.get(simplefix.TAG_CLORDID), b'cancel-id')
        self.assertEqual(cancel_reject.get(simplefix.TAG_ORIGCLORDID), b'bad-id')
        self.assertEqual(cancel_reject.get(simplefix.TAG_ORDERID), b'NONE')
        self.assertEqual(cancel_reject.get(simplefix.TAG_ORDSTATUS), b'8')
        self.assertEqual(cancel_reject.get('434'), b'1')

    def test_replace_request_reject(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_CLORDID, 'order-id')

        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 1)
        ack = self._parse_fix_msg(replies[0])
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')

        replace = simplefix.FixMessage()
        replace = self.server._session.get_msg_header_message()
        replace.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REPLACE_REQUEST)
        replace.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        replace.append_pair(simplefix.TAG_ORDERQTY, '100')
        replace.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        replace.append_pair(simplefix.TAG_ORIGCLORDID, 'bad-id')
        replace.append_pair(simplefix.TAG_CLORDID, 'cancel-id')

        self.client.send(replace.encode())
        replies = self._get_replies(self.client,client_session, 1)
        cancel_reject = self._parse_fix_msg(replies[0])
        self.assertTrue(cancel_reject != None)

        self.assertEqual(cancel_reject.get(simplefix.TAG_CLORDID), b'cancel-id')
        self.assertEqual(cancel_reject.get(simplefix.TAG_ORIGCLORDID), b'bad-id')
        self.assertEqual(cancel_reject.get(simplefix.TAG_ORDERID), b'NONE')
        self.assertEqual(cancel_reject.get(simplefix.TAG_ORDSTATUS), b'8')
        self.assertEqual(cancel_reject.get('434'), b'2')

    def test_new_order_single_fill(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        # set the tag value that we want to find out to send fills (it could be any tag)
        #server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')

        self.client.send(order.encode())

        replies = self._get_replies(self.client,client_session, 2)
        ack = self._parse_fix_msg(replies[0])
        fill = self._parse_fix_msg(replies[1])

        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')
        self._test_basic_execution_report_message(fill, order,'fill', 'FIX.4.2')

    def test_new_order_single_fill_json_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_PATH, None)
        self.server.set_on_fill_reply_tags(parser.get_tags())
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        buffer_parser = BufferFixParser()
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        # set the tag value that we want to find out to send fills (it could be any tag)
        #server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')

        self.client.send(order.encode())

        replies = self._get_replies(self.client,client_session, 2)
        ack = self._parse_fix_msg(replies[0])
        fill = self._parse_fix_msg(replies[1])

        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')
        self.assertEqual(fill.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(fill.get('1'), b'test_1')
        self.assertEqual(fill.get('11'), order.get('1'))
        self.assertEqual(fill.get('21'), order.get('21'))
        self.assertEqual(fill.get('1234'), b'test_1234')

    def test_new_order_single_fill_json_enhance_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_ENHANCE_PATH, None)
        self.server.set_on_fill_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        # set the tag value that we want to find out to send fills (it could be any tag)
        #server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')

        self.client.send(order.encode())

        replies = self._get_replies(self.client,client_session, 2)
        ack = self._parse_fix_msg(replies[0])
        fill = self._parse_fix_msg(replies[1])

        self._test_basic_execution_report_message(ack, order, 'ack', 'FIX.4.2')
        self._test_basic_execution_report_message(fill, order,'fill', 'FIX.4.2')
        self.assertEqual(fill.get('1'), b'test_1')
        self.assertEqual(fill.get('111'), order.get('1'))
        self.assertEqual(fill.get('21'), order.get('11'))
        self.assertEqual(fill.get('1234'), b'test_1234')

    def test_reconnect_reusing_session(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID
    
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
       
        self.server.set_reuse_session(True)
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        buffer_parser = BufferFixParser()
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        self.server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')
        self.client.send(order.encode())
        replies = self._get_replies(self.client,client_session, 2)
        ack = self._parse_fix_msg(replies[0])
        fill = self._parse_fix_msg(replies[1])
       
        self.assertEqual(ack.get(simplefix.TAG_MSGSEQNUM), b'1')
        self.assertEqual(fill.get(simplefix.TAG_MSGSEQNUM), b'2')

        # disconnect the client
        logout = self.server._session.get_logout_message('SHUTDOWN')
        self.client.send(logout.encode())
        self.client.shutdown(2)

        client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, client)
        self._wait_client_connected(self.server)
        client.send(order.encode())
        replies = self._get_replies(client,client_session, 2, 1)
        ack = self._parse_fix_msg(replies[0])
        fill = self._parse_fix_msg(replies[1])
        self.assertEqual(ack.get(simplefix.TAG_MSGSEQNUM), b'4') # seq num 3 is the logout sent by the servers

    def test_highlighted_tags(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # tags 10 and 44 will be highlighted
            self.server = self._start_server(self._get_server(port, None, [12, 44]))
        self.client = self._connect_client(port)
        self._wait_client_connected(self.server)

        order = simplefix.FixMessage()
        order = self.server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, 'T')
        order.append_pair('44',  'HIGHLIGHTED')
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair('12', 'HIGHLIGHTED')
        order.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        output = None
        with captured_output() as (out, _):
            self.client.send(order.encode())
            time.sleep(0.5)
            output = out.getvalue().strip()

        self.assertEqual(output, 'RECV: T    8=FIX.4.2|9=74|35=T|49=Sender|56=Target|\x1b[92m44=HIGHLIGHTED\x1b[0m|38=100|\x1b[92m12=HIGHLIGHTED\x1b[0m|58=GET_FILL|10=235|')

    def test_quote_status_report(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 1)
        ai = self._parse_fix_msg(replies[0])
        self.assertTrue(ai != None)
        self._test_quote_status_report(quote, ai)

    def test_quote_status_report_json_enhanced_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_AI_ENHANCE_PATH, None)
        self.server.set_on_quote_ack_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 1)
        ai = self._parse_fix_msg(replies[0])
        self.assertTrue(ai != None)
        self._test_quote_status_report(quote, ai)
        self.assertEqual(ai.get('12'), b'test_12')
        self.assertEqual(ai.get('13'), quote.get('1'))
        self.assertEqual(ai.get('21'), quote.get('117'))
        self.assertEqual(ai.get('1234'), b'test_1234')

    def test_quote_status_report_json_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        
        parser = JsonMsgParser(FixServerTests.TEST_REPLY_PATH, None)
        self.server.set_on_quote_ack_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')

        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 1)
        ai = self._parse_fix_msg(replies[0])
        self.assertTrue(ai != None)
        self._compare_msg_to_json_quote_fixed_reply(ai, quote)

    def _compare_msg_to_json_quote_fixed_reply(self, msg, original_request):
        self.assertEqual(msg.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(msg.get('1'), b'test_1')
        self.assertEqual(msg.get('111'), original_request.get('1'))
        self.assertEqual(msg.get('21'), original_request.get('21'))
        self.assertEqual(msg.get('1234'), b'test_1234')

    def test_quote_full_fill_json_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))

        parser = JsonMsgParser(FixServerTests.TEST_REPLY_PATH, None)
        self.server.set_on_quote_fill_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')
        quote.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        self.server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')
        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 3) # AI and 2 fills
        self.assertEqual(len(replies), 3)

        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        fill_2 = self._parse_fix_msg(replies[2])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self.assertNotEqual(fill_2, None)
        self._test_quote_status_report(quote, ai)
        self._compare_msg_to_json_quote_fixed_reply(fill_1, quote)
        self._compare_msg_to_json_quote_fixed_reply(fill_2, quote)

        # now only buy side
        side = 1
        self.server._session.set_quote_fill_only_side(side)
        self.client.send(quote.encode())
        replies = self._get_replies(self.client, client_session, 2) # AI and 1 fill
        self.assertEqual(len(replies), 2)
        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._compare_msg_to_json_quote_fixed_reply(fill_1, quote)

        # now only sell side
        side = 2
        self.server._session.set_quote_fill_only_side(side)
        self.client.send(quote.encode())
        replies = self._get_replies(self.client, client_session, 2) # AI and 1 fill
        self.assertEqual(len(replies), 2)
        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._compare_msg_to_json_quote_fixed_reply(fill_1, quote)

    def test_quote_full_fill(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')
        quote.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        self.server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')
        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 3) # AI and 2 fills
        self.assertEqual(len(replies), 3)

        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        fill_2 = self._parse_fix_msg(replies[2])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self.assertNotEqual(fill_2, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, 1)
        self._test_quote_full_fill(quote, fill_2, 2)

        # now only buy side
        side = 1
        self.server._session.set_quote_fill_only_side(side)
        self.client.send(quote.encode())
        replies = self._get_replies(self.client, client_session, 2) # AI and 1 fill
        self.assertEqual(len(replies), 2)
        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, side)

        # now only sell side
        side = 2
        self.server._session.set_quote_fill_only_side(side)
        self.client.send(quote.encode())
        replies = self._get_replies(self.client, client_session, 2) # AI and 1 fill
        self.assertEqual(len(replies), 2)
        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, side)

    def test_quote_full_fill_json_enhanced_reply(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server(port, (simplefix.TAG_TEXT, 'GET_FILL')))

        parser = JsonMsgParser(FixServerTests.TEST_REPLY_QUOTE_FILL_ENHANCE_PATH, None)
        self.server.set_on_quote_fill_reply_tags(parser.get_tags(), parser.get_enhance_flag())

        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')
        quote.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        self.server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')
        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 3) # AI and 2 fills
        self.assertEqual(len(replies), 3)

        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        fill_2 = self._parse_fix_msg(replies[2])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self.assertNotEqual(fill_2, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, 1)
        self._test_quote_full_fill(quote, fill_2, 2)
        self.assertEqual(fill_1.get('12'), b'test_12')
        self.assertEqual(fill_1.get('13'), quote.get('1'))
        self.assertEqual(fill_1.get('21'), quote.get('117'))
        self.assertEqual(fill_1.get('1234'), b'test_1234')
        self.assertEqual(fill_2.get('12'), b'test_12')
        self.assertEqual(fill_2.get('13'), quote.get('1'))
        self.assertEqual(fill_2.get('21'), quote.get('117'))
        self.assertEqual(fill_2.get('1234'), b'test_1234')

        # now only buy side
        side = 1
        self.server._session.set_quote_fill_only_side(side)
        self.client.send(quote.encode())
        replies = self._get_replies(self.client, client_session, 2) # AI and 1 fill
        self.assertEqual(len(replies), 2)
        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, side)
        self.assertEqual(fill_1.get('12'), b'test_12')
        self.assertEqual(fill_1.get('13'), quote.get('1'))
        self.assertEqual(fill_1.get('21'), quote.get('117'))
        self.assertEqual(fill_1.get('1234'), b'test_1234')

        # now only sell side
        side = 2
        self.server._session.set_quote_fill_only_side(side)
        self.client.send(quote.encode())
        replies = self._get_replies(self.client, client_session, 2) # AI and 1 fill
        self.assertEqual(len(replies), 2)
        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, side)
        self.assertEqual(fill_1.get('12'), b'test_12')
        self.assertEqual(fill_1.get('13'), quote.get('1'))
        self.assertEqual(fill_1.get('21'), quote.get('117'))
        self.assertEqual(fill_1.get('1234'), b'test_1234')

    def test_quote_full_fill_only_buy_side(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        self.server = None
        while not self.server:
            port = self._get_port()
            # set the tag value that we want to find out to send fills (it could be any tag)
            self.server = self._start_server(self._get_server_quote_buy_fill_side(port, (simplefix.TAG_TEXT, 'GET_FILL')))
        self.client = self._connect_client(port)
        client_session = FixSessionServer(None, None, None, self.client)
        self._wait_client_connected(self.server)

        quote = self.server._session.get_msg_header_message()
        quote.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)
        quote.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        quote.append_pair(simplefix.TAG_ACCOUNT, 'ACCOUNT')
        quote.append_pair(simplefix.TAG_CURRENCY, 'USD')
        quote.append_pair('132', '0.132') # BidPx
        quote.append_pair('133', '0.133') # AskPx
        quote.append_pair('134', '134') # BidSize
        quote.append_pair('135', '135') # AskSize
        quote.append_pair(simplefix.TAG_SECURITY_ID, 'SECURITY')
        quote.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        quote.append_pair(simplefix.TAG_QUOTEID, 'quote-id')
        quote.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        self.server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')
        self.client.send(quote.encode())

        replies = self._get_replies(self.client, client_session, 2) # AI and only fill for buy side
        self.assertEqual(len(replies), 2)

        ai = self._parse_fix_msg(replies[0])
        fill_1 = self._parse_fix_msg(replies[1])
        self.assertNotEqual(ai, None)
        self.assertNotEqual(fill_1, None)
        self._test_quote_status_report(quote, ai)
        self._test_quote_full_fill(quote, fill_1, 1)

if __name__ == "__main__":
    unittest.main()
