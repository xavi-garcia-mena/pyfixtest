#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import FixSessionClient, FixClient, FixSessionServer, FixServer, BufferFixParser
import unittest
import simplefix
import threading
import time
import socket
import select
from random import randint
import helper_serverrunner
from contextlib import contextmanager
import sys
import os
import signal

from simplefix.message import FixMessage
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO
from datetime import datetime

SENDER_COMP_ID="Sender"
TARGET_COMP_ID="Target"
FIX_VERSION_STR="FIX.4.2"
PORTS_BEGIN = 49152
PORTS_END = 65535

class TestSocket(object):
    def __init__(self):
        self._buffer = None
        self._status = "OPEN"

    def send(self, buff):
        self._buffer = buff

    def get_buffer(self):
        return self._buffer

    def close(self):
        self._status = "CLOSED"

class FixSessionClientTests(unittest.TestCase):
    def setUp(self):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        self._socket = TestSocket()
        self._session = FixSessionClient(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, self._socket)
        self._client = None

    def tearDown(self):
        pass

    def _get_msg_from_socket(self):
        parser = simplefix.FixParser()
        parser.append_buffer(self._socket.get_buffer())
        msg_sent = parser.get_message()
        return msg_sent

    def test_on_logon(self):
        logon = self._session.get_logon_message()
        # exchange the sender and target comp ids like the server would reply
        logon.remove(simplefix.TAG_SENDER_COMPID)
        logon.remove(simplefix.TAG_TARGET_COMPID)
        logon.append_pair(simplefix.TAG_SENDER_COMPID, TARGET_COMP_ID, header=True)
        logon.append_pair(simplefix.TAG_TARGET_COMPID, SENDER_COMP_ID, header=True)
        self._socket._buffer = None

        # verify that the session is not logged in
        self.assertFalse(self._session._logged_in)

        self._session.on_logon(logon)

        # verify that the session is logged in
        self.assertTrue(self._session._logged_in)

        # check that the client didn't send any message back to the server
        msg_sent = self._get_msg_from_socket()
        self.assertEqual(msg_sent, None)

    def test_on_logon_bad_but_not_verifying(self):
        logon = self._session.get_logon_message()
        # logon has the sender and target comp ids of the client, and we expect to get them swapped from the server
        self._socket._buffer = None

        # verify that the session is not logged in
        self.assertFalse(self._session._logged_in)

        self._session.on_logon(logon)

        # verify that the session is logged in
        self.assertTrue(self._session._logged_in)

        # check that the client didn't send any message back to the server
        msg_sent = self._get_msg_from_socket()
        self.assertEqual(msg_sent, None)

    def test_on_logon_bad_verifying(self):
        self._session.set_verify_logon_message(True)
        logon = self._session.get_logon_message()
        # logon has the sender and target comp ids of the client, and we expect to get them swapped from the server
        self._session._outgoing_seq_num = 1999
        self._socket._buffer = None

        # verify that the session is not logged in
        self.assertFalse(self._session._logged_in)

        self.assertEqual(self._socket._status, 'OPEN')

        self._session.on_logon(logon)

        # verify that the session is logged in
        self.assertFalse(self._session._logged_in)

        self.assertEqual(self._socket._status, 'CLOSED')

        # check that the client didn't send any message back to the server
        msg_sent = self._get_msg_from_socket()
        self.assertEqual(msg_sent, None)

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class CommandsSocket(socket.socket):
    def __init__(self, family, type):
        socket.socket.__init__(self, family, type)

    def readline(self):
        return self.recv(1024)

class CommandsSocketHandler(object):
    def __init__(self, port):
        self._port = port
        self._listen_socket = None
        self._connected_socket = None
        self._listening = False
        self._thread = None
        self._stop = threading.Event()

    def is_connected(self):
        return self._connected_socket != None

    def terminate(self):
        self._stop.set()

    def join(self):
        self._thread.join()

    def start(self):
        self._thread = threading.Thread(target=self._handle_connection, args=())
        self._thread.start()
        print ('Commands socket done')

    def _handle_connection(self):
        self._listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._listen_socket.bind(('127.0.0.1', self._port))
        print ('listen')
        self._listen_socket.listen(1)
        print ('accept')
        self._listening = True
        self._connected_socket, _ = self._listen_socket.accept()
        while not self._stop.is_set():
            self._stop.wait(1)

class FixSessionTest(FixSessionClient):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1):
        FixSessionClient.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num)
        self.last_message_sent = None
        self.last_message_received = None
        self.messages_sent = []
        self.messages_received = []
        print ('Init FixSessionTest')

    def send_message(self, msg):
        self.last_message_sent = msg
        FixSessionClient.send_message(self, msg)
        msg_copy = FixMessage()
        for tag, value in msg.pairs:
            msg_copy.append_pair(tag, value)
        self.messages_sent.append(msg_copy)

    def on_message(self, msg):
        self.last_message_received = msg
        self.messages_received.append(msg)
        FixSessionClient.on_message(self, msg)

class FixClientBase(helper_serverrunner.HelperServerRunner):
    def _get_server(self, port):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        return FixServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)

    def _get_client(self, port):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        return FixClient('127.0.0.1', port, FIX_VERSION_STR, TARGET_COMP_ID, SENDER_COMP_ID)

    def _get_client_test(self, port, commands_socket=sys.stdin):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        client = FixClient('127.0.0.1', port, FIX_VERSION_STR, TARGET_COMP_ID, SENDER_COMP_ID)
        client.set_fix_session_class(FixSessionTest)
        client._read_commands_from = commands_socket
        return client

class FixClientTests(FixClientBase):
    def test_client_connected(self):
        server = None 
        port = None
        while not server:
            port = self._get_port()
            server = self._start_server(self._get_server(port))

        client = None
        while not client:
            client = self._start_client(self._get_client(port))

        self._wait_client_connected(client)
        timeout = 2.0
        while not client._session._logged_in and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1
        client.start() # try to run the client again, it should not do anything as it is already running
        server.stop()

    def test_client_connect_disconnect_connect(self):
        server = None 
        port = None
        while not server:
            port = self._get_port()
            server = self._start_server(self._get_server(port))

        client = None
        while not client:
            client = self._start_client(self._get_client(port))

        self._wait_client_connected(client)
        timeout = 2.0
        while not client._session._logged_in and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

        client.stop()

        client = None
        while not client:
            client = self._start_client(self._get_client(port))

        self._wait_client_connected(client)
        self.assertNotEqual(client._session, None)
        timeout = 2.0
        while not client._session._logged_in and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1
        server.stop()

    def test_client_process_message(self):
        server = None 
        port = None
        while not server:
            port = self._get_port()
            server = self._start_server(self._get_server(port))

        client = None
        while not client:
            client = self._start_client(self._get_client_test(port))

        self._wait_client_connected(client)
        timeout = 2.0
        while not client._session._logged_in and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

        msg = server._session.get_msg_header_message()
        msg.append_pair(simplefix.TAG_MSGTYPE, b'R')
        msg.append_pair(simplefix.TAG_ACCOUNT, b'test_1')

        client._session.last_message_received = None
        server._session.send_message(msg)
        timeout = 2.0
        while client._session.last_message_received is None and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1
        self.assertNotEqual(client._session.last_message_received, None)
        parser = simplefix.FixParser()
        parser.append_buffer(client._session.last_message_received.decode())
        msg = parser.get_message()
        self.assertNotEqual(msg, None)
        self.assertEqual(msg.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(msg.get(simplefix.TAG_ACCOUNT), b'test_1')
        server.stop()

class FixClientCommandsTests(FixClientBase):
    def setUp(self):
        commands_socket_port = randint(1230, 2000)
        print ('Commands thread port %d' % commands_socket_port)
        FixClientBase.setUp(self)
        # create a separated thread to listen to the test socket
        self.commands_socket_handler = CommandsSocketHandler(commands_socket_port)
        self.commands_socket_handler.start()

        # wait until thread is listening
        timeout = 3.0
        while not self.commands_socket_handler._listening and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

        self.assertTrue(self.commands_socket_handler._listening)
        print ('Listening...')

        # create a test socket object and connect to the listening port
        self.commands_client_socket = CommandsSocket(socket.AF_INET, socket.SOCK_STREAM)
        self.commands_client_socket.connect(('127.0.0.1', commands_socket_port))
        timeout = 3.0
        while not self.commands_socket_handler._connected_socket and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1
        self.assertTrue(self.commands_socket_handler._connected_socket != None)
        print ('Connected...')

        # now we have a valid socket that we can use to mock syt.stdin
        # start server and client
        self.server = None 
        self.port = None
        while not self.server:
            self.port = self._get_port()
            self.server = self._start_server(self._get_server(self.port))

        self.client = None
        while not self.client:
            # we pass the test socket as the source for reading commands
            self.client = self._start_client(self._get_client_test(self.port, self.commands_client_socket))

        self._wait_client_connected(self.client)
        timeout = 2.0
        while not self.client._session._logged_in and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

    def tearDown(self):
        print ('**********************************')
        self.server.stop()
        #time.sleep(1)
        self.commands_socket_handler.terminate()
        self.commands_socket_handler.join()
        FixClientBase.tearDown(self)
        if os.path.isfile('./pyfixtest_multiple_stage1.json'):
            os.remove('./pyfixtest_multiple_stage1.json')
        if os.path.isfile('./pyfixtest_multiple_stage2.json'):
            os.remove('./pyfixtest_multiple_stage2.json')
        if os.path.isfile('./pyfixtest_multiple_stage3.json'):
            os.remove('./pyfixtest_multiple_stage3.json')

    def test_commands_thread(self):
        # at this point the server and client are connected and exchanged the Logon message
        # try to send 'a\n' the client will try to read the message_a.json file which does not exist
        with captured_output() as (out, _):
            self.commands_socket_handler._connected_socket.send(b'a\n')
            output = out.getvalue().strip()
            timeout = 2.0
            while output == '' and timeout > 0.0:
                output = out.getvalue().strip()
                time.sleep(0.1)
                timeout -= 0.1
            self.assertTrue('ERROR file [./message_a.json] does not exist' in output)
        
        # now try to run the same, but for a existing file
        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        commands_file = open('./message_r.json', 'w')
        commands_file.write(json_data_ok)
        commands_file.close()
        self.client._session.last_message_sent = None

        # send the 'r\n' command, client will read the message_r.json file and will send the message
        # described in the file
        self.commands_socket_handler._connected_socket.send(b'r\n')
        timeout = 1.0
        while self.client._session.last_message_sent is None and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

        self.assertNotEqual(self.client._session.last_message_sent, None)
        self.assertEqual(self.client._session.last_message_sent.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(self.client._session.last_message_sent.get('1'), b'test_1')
        self.assertEqual(self.client._session.last_message_sent.get('11'), b'test_11')
        self.assertEqual(self.client._session.last_message_sent.get('21'), b'test_21')
        self.assertEqual(self.client._session.last_message_sent.get('1234'), b'test_1234')

        self.assertEqual(len(self.client._session.messages_sent), 2)

    def test_commands_thread_multiple(self):
        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "D", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        commands_file = open('./pyfixtest_multiple_stage1.json', 'w')
        commands_file.write(json_data_ok)
        commands_file.close()
        self.client._session.last_message_sent = None

        # send the 'r\n' command, client will read the message_r.json file and will send the message
        # described in the file
        self.commands_socket_handler._connected_socket.send(b'multiple\n')
        timeout = 10.0
        NUMBER_OF_MESSAGES_TO_SEND = self.client._commands_thread._number_of_messages_per_stage + 1
        while len(self.client._session.messages_sent) < NUMBER_OF_MESSAGES_TO_SEND and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

        self.assertEqual(NUMBER_OF_MESSAGES_TO_SEND, len(self.client._session.messages_sent))

        # first message sent should be a LOGON message
        self.assertEqual(simplefix.MSGTYPE_LOGON, self.client._session.messages_sent[0].get(simplefix.TAG_MSGTYPE))
        # the rest should be D
        for i in range(1, NUMBER_OF_MESSAGES_TO_SEND):
            self.assertEqual(simplefix.MSGTYPE_NEW_ORDER_SINGLE, self.client._session.messages_sent[i].get(simplefix.TAG_MSGTYPE))


        # check that they were sent in the specified interval
        if sys.version_info[0] == 2:
            time_before = datetime.strptime(self.client._session.messages_sent[1].get(simplefix.TAG_SENDING_TIME), '%Y%m%d-%H:%M:%S.%f')
        else:
            time_before = datetime.strptime(self.client._session.messages_sent[1].get(simplefix.TAG_SENDING_TIME).decode(), '%Y%m%d-%H:%M:%S.%f')
        for i in range(2, NUMBER_OF_MESSAGES_TO_SEND):
            if sys.version_info[0] == 2:
                time_now = datetime.strptime(self.client._session.messages_sent[i].get(simplefix.TAG_SENDING_TIME), '%Y%m%d-%H:%M:%S.%f')
            else:
                time_now = datetime.strptime(self.client._session.messages_sent[i].get(simplefix.TAG_SENDING_TIME).decode(), '%Y%m%d-%H:%M:%S.%f')
            #print ('time_before %s\n' % time_before)
            #print ('time_now %s\n' % time_now)
            #print ('diff: %f\n' % ((time_now-time_before).total_seconds() * 1000))
            # default is to send a message every 0.1 seconds... which is 100 millis
            # allow a difference of plus/minus 10 millis to consider it correct
            self.assertTrue(((time_now-time_before).total_seconds() * 1000) < 110)
            self.assertTrue(((time_now-time_before).total_seconds() * 1000) > 90)

            time_before = time_now

    def test_commands_thread_multiple_stages(self):
        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "D", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "##UNIQUE##" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        commands_file = open('./pyfixtest_multiple_stage1.json', 'w')
        commands_file.write(json_data_ok)
        commands_file.close()

        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "G", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "##UNIQUE##" }, \
                                    { "tag" : "41",   "value" : "##LAST_CLORDID##" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        commands_file = open('./pyfixtest_multiple_stage2.json', 'w')
        commands_file.write(json_data_ok)
        commands_file.close()

        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "F", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "##UNIQUE##" }, \
                                    { "tag" : "41",   "value" : "##LAST_CLORDID##" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        commands_file = open('./pyfixtest_multiple_stage3.json', 'w')
        commands_file.write(json_data_ok)
        commands_file.close()

        self.client._session.last_message_sent = None

        # send the 'r\n' command, client will read the message_r.json file and will send the message
        # described in the file
        self.commands_socket_handler._connected_socket.send(b'multiple\n')
        timeout = 10.0
        NUMBER_OF_MESSAGES_TO_SEND = (self.client._commands_thread._number_of_messages_per_stage * 3) + 1
        while len(self.client._session.messages_sent) < NUMBER_OF_MESSAGES_TO_SEND and timeout > 0.0:
            time.sleep(0.1)
            timeout -= 0.1

        self.assertEqual(NUMBER_OF_MESSAGES_TO_SEND, len(self.client._session.messages_sent))

        # first message sent should be a LOGON message
        self.assertEqual(simplefix.MSGTYPE_LOGON, self.client._session.messages_sent[0].get(simplefix.TAG_MSGTYPE))
        # the rest should be D
        i = 1
        while i < NUMBER_OF_MESSAGES_TO_SEND:
            self.assertEqual(simplefix.MSGTYPE_NEW_ORDER_SINGLE, self.client._session.messages_sent[i].get(simplefix.TAG_MSGTYPE))
            clordid_order = self.client._session.messages_sent[i].get(simplefix.TAG_CLORDID)
            i += 1
            self.assertEqual(simplefix.MSGTYPE_ORDER_CANCEL_REPLACE_REQUEST, self.client._session.messages_sent[i].get(simplefix.TAG_MSGTYPE))
            clordid_replace = self.client._session.messages_sent[i].get(simplefix.TAG_CLORDID)
            origclordid_replace = self.client._session.messages_sent[i].get(simplefix.TAG_ORIGCLORDID)
            i += 1
            self.assertEqual(simplefix.MSGTYPE_ORDER_CANCEL_REQUEST, self.client._session.messages_sent[i].get(simplefix.TAG_MSGTYPE))
            clordid_cancel = self.client._session.messages_sent[i].get(simplefix.TAG_CLORDID)
            origclordid_cancel = self.client._session.messages_sent[i].get(simplefix.TAG_ORIGCLORDID)
            i += 1
            self.assertEqual(clordid_order, origclordid_replace)
            self.assertEqual(clordid_replace, origclordid_cancel)

        # check that they were sent in the specified interval
        if sys.version_info[0] == 2:
            time_before = datetime.strptime(self.client._session.messages_sent[1].get(simplefix.TAG_SENDING_TIME), '%Y%m%d-%H:%M:%S.%f')
        else:
            time_before = datetime.strptime(self.client._session.messages_sent[1].get(simplefix.TAG_SENDING_TIME).decode(), '%Y%m%d-%H:%M:%S.%f')
        for i in range(2, NUMBER_OF_MESSAGES_TO_SEND):
            if sys.version_info[0] == 2:
                time_now = datetime.strptime(self.client._session.messages_sent[i].get(simplefix.TAG_SENDING_TIME), '%Y%m%d-%H:%M:%S.%f')
            else:
                time_now = datetime.strptime(self.client._session.messages_sent[i].get(simplefix.TAG_SENDING_TIME).decode(), '%Y%m%d-%H:%M:%S.%f')
            #print ('time_before %s\n' % time_before)
            #print ('time_now %s\n' % time_now)
            #print ('diff: %f\n' % ((time_now-time_before).total_seconds() * 1000))
            # default is to send a message every 0.1 seconds... which is 100 millis
            # allow a difference of plus/minus 10 millis to consider it correct
            self.assertTrue(((time_now-time_before).total_seconds() * 1000) < 110)
            self.assertTrue(((time_now-time_before).total_seconds() * 1000) > 90)

            time_before = time_now

if __name__ == "__main__":
    unittest.main()
