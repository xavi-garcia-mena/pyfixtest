#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import ScriptUtils
import unittest
from contextlib import contextmanager
import sys, os
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class ScriptUtilsTests(unittest.TestCase):
    TEST_FILE_OK_PATH = "./testfile_ok.json"
    TEST_FILE_MESSAGE_TAG_BAD_PATH = "./testfile_message_tag_bad.json"
    def setUp(self):
        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(ScriptUtilsTests.TEST_FILE_OK_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_message_tag_bad = '{ "messagexxxxxx": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(ScriptUtilsTests.TEST_FILE_MESSAGE_TAG_BAD_PATH, 'w')
        file.write(json_data_message_tag_bad)
        file.close()

    def tearDown(self):
        os.remove(ScriptUtilsTests.TEST_FILE_OK_PATH)
        os.remove(ScriptUtilsTests.TEST_FILE_MESSAGE_TAG_BAD_PATH)

    @contextmanager
    def assertNotRaises(self, exc_type):
        try:
            yield None
        except exc_type:
            raise self.failureException('{} raised'.format(exc_type.__name__))

    def _assert_function_result(self, function, args, expected):
        with captured_output() as (out, _):
            try:
                if args is None:
                    function()
                else:
                    function(*args)
            except SystemExit:
                print ('SYSTEM EXIT')
            output = out.getvalue().strip()
        if output.find(expected) == -1:
            print ('expected: %s' % expected.replace('\n', '\\n'))
            print ('actual: %s' % output.replace('\n', '\\n'))
        self.assertNotEqual(output.find(expected), -1)
        print (output)

    def test_usage(self):
        script_utils = ScriptUtils('test_bin')
        self._assert_function_result(script_utils.usage, None, \
            'test_bin --ip=<IP ADDRESS> --port=<PORT> --fix=<FIX VERSION STRING> --sender_comp_id=<VALUE> --target_comp_id=<VALUE> --outgoing_seq_num=<NUMBER>\n    example: test_bin --ip=127.0.0.1 --port 6575 --fix=FIX.4.2 --sender_comp_id=SENDER --target_comp_id=TARGET --outgoing_seq_num=14\n    OPTIONAL FLAGS:\n      --tag_value_for_fills=[TAG,VALUE]\n      --highlight=[TAG1,TAG2,...,TAGN]\n      --quote_fill_this_side=1 or 2\n    TAGS FOR REPLIES DEFINITION:\n      --on-ack-reply-tags=[FILE_PATH]\n      --on-fill-reply-tags=[FILE_PATH]\n      --on-cancel-reply-tags=[FILE_PATH]\n      --on-replace-reply-tags=[FILE_PATH]\n      --on-quote-ack-reply-tags=[FILE_PATH]\n      --on-quote-fill-reply-tags=[FILE_PATH]')
        
    def test_verify_ip(self):
        script_utils = ScriptUtils('test_bin')
        # invalid ip
        with self.assertRaises(SystemExit) as cm:
            script_utils.verify_ip('blah')
            self.assertEqual(cm.exception.code, 2)
        # valid ip
        with self.assertNotRaises(SystemExit):
            script_utils.verify_ip('127.0.0.1')

    def test_check_ip_provided(self):
        script_utils = ScriptUtils('test_bin')
        # invalid ip
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_ip_provided(None)
            self.assertEqual(cm.exception.code, 2)
        # valid ip
        with self.assertNotRaises(SystemExit):
            script_utils.check_ip_provided('127.0.0.1')

    def test_check_provided_fix_string(self):
        script_utils = ScriptUtils('test_bin')
        # fix is none
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_provided_fix_string(None)
            self.assertEqual(cm.exception.code, 2)
        # invalid fix
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_provided_fix_string('BLAH')
            self.assertEqual(cm.exception.code, 2)
        # valid fix strings
        with self.assertNotRaises(SystemExit):
            script_utils.check_provided_fix_string('FIX.4.2')
        with self.assertNotRaises(SystemExit):
            script_utils.check_provided_fix_string('FIX.4.3')
        with self.assertNotRaises(SystemExit):
            script_utils.check_provided_fix_string('FIX.4.4')
        with self.assertNotRaises(SystemExit):
            script_utils.check_provided_fix_string('FIXT.1.1')

    def test_check_port_provided(self):
        script_utils = ScriptUtils('test_bin')
        # none port
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_port_provided(None)
            self.assertEqual(cm.exception.code, 2)
        # invalid port
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_port_provided('BLAH')
            self.assertEqual(cm.exception.code, 2)
        # valid port
        with self.assertNotRaises(SystemExit):
            script_utils.check_port_provided('1234')
        with self.assertNotRaises(SystemExit):
            script_utils.check_port_provided(1234)

    def test_check_sender_comp_id(self):
        script_utils = ScriptUtils('test_bin')
        # invalid 
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_sender_comp_id(None)
            self.assertEqual(cm.exception.code, 2)
        # valid 
        with self.assertNotRaises(SystemExit):
            script_utils.check_sender_comp_id('TEST')

    def test_check_target_comp_id(self):
        script_utils = ScriptUtils('test_bin')
        # invalid 
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_target_comp_id(None)
            self.assertEqual(cm.exception.code, 2)
        # valid 
        with self.assertNotRaises(SystemExit):
            script_utils.check_target_comp_id('TEST')

    def test_check_outgoing_seq_num(self):
        script_utils = ScriptUtils('test_bin')
        # invalid 
        with self.assertRaises(SystemExit) as cm:
            script_utils.check_outgoing_seq_num('blah')
            self.assertEqual(cm.exception.code, 2)
        # valid 
        with self.assertNotRaises(SystemExit):
            script_utils.check_outgoing_seq_num('12')
        with self.assertNotRaises(SystemExit):
            script_utils.check_outgoing_seq_num(1235)
        with self.assertNotRaises(SystemExit):
            script_utils.check_outgoing_seq_num(None)

    def test_get_expected_tag_value_for_fills(self):
        script_utils = ScriptUtils('test_bin')
        value = script_utils.get_expected_tag_value_for_fills(None)
        self.assertEqual(value, None)
        tag, value = script_utils.get_expected_tag_value_for_fills("[123,FILL_VALUE]")
        self.assertEqual(tag, '123')
        self.assertEqual(value, 'FILL_VALUE')
        self._assert_function_result(script_utils.get_expected_tag_value_for_fills, ["[123,FILL_VALUE]"], '')
        with self.assertRaises(SystemExit) as cm:
            script_utils.get_expected_tag_value_for_fills('blah')
            self.assertEqual(cm.exception.code, 2)
        self._assert_function_result(script_utils.get_expected_tag_value_for_fills, ["123,FILL_VALUE"], 'ERROR: expected tag for fills should match the format: [NUMBER,VALUE] where VALUE is an integer')
        self._assert_function_result(script_utils.get_expected_tag_value_for_fills, ["[123,FILL_VALUE"], 'ERROR: expected tag for fills should match the format: [NUMBER,VALUE] where VALUE is an integer')
        self._assert_function_result(script_utils.get_expected_tag_value_for_fills, ["[123,]"], 'ERROR: expected tag for fills should match the format: [NUMBER,VALUE] where VALUE is an integer')

    def test_get_highlighted_tags(self):
        script_utils = ScriptUtils('test_bin')
        values = script_utils.get_highlighted_tags("[1,2,3,4,5]")
        self.assertEqual(values, [1,2,3,4,5])
        values = script_utils.get_highlighted_tags(None)
        self.assertEqual(values, None)
        values = script_utils.get_highlighted_tags("[1]")
        self.assertEqual(values, [1])
        self._assert_function_result(script_utils.get_highlighted_tags, ["[123,]"], 'ERROR: expected highlighted tags should match the format: [NUMBER,NUMBER,NUMBER]')
        self._assert_function_result(script_utils.get_highlighted_tags, ["[123,as]"], 'ERROR: expected highlighted tags should match the format: [NUMBER,NUMBER,NUMBER]')
        self._assert_function_result(script_utils.get_highlighted_tags, ["[123,12"], 'ERROR: expected highlighted tags should match the format: [NUMBER,NUMBER,NUMBER]')
        self._assert_function_result(script_utils.get_highlighted_tags, ["[bla]"], 'ERROR: expected highlighted tags should match the format: [NUMBER,NUMBER,NUMBER]')

    def test_get_tags(self):
        script_utils = ScriptUtils('test_bin')
        tags = script_utils.get_msg_tags(ScriptUtilsTests.TEST_FILE_OK_PATH)
        self.assertNotEqual(tags, None)
        with self.assertRaises(SystemExit) as cm:
            script_utils.get_msg_tags(ScriptUtilsTests.TEST_FILE_MESSAGE_TAG_BAD_PATH)
            self.assertEqual(cm.exception.code, 2)

    def test_parse_options(self):
        script_utils = ScriptUtils('test_bin')
        sys.argv = ["test_bin", "--help"]
        self._assert_function_result(script_utils.parse_options, None, 'test_bin --ip=<IP ADDRESS> --port=<PORT> --fix=<FIX VERSION STRING> --sender_comp_id=<VALUE> --target_comp_id=<VALUE> --outgoing_seq_num=<NUMBER>\n    example: test_bin --ip=127.0.0.1 --port 6575 --fix=FIX.4.2 --sender_comp_id=SENDER --target_comp_id=TARGET --outgoing_seq_num=14')
        
        sys.argv = ["test_bin", "--blah"]
        self._assert_function_result(script_utils.parse_options, None, 'test_bin --ip=<IP ADDRESS> --port=<PORT> --fix=<FIX VERSION STRING> --sender_comp_id=<VALUE> --target_comp_id=<VALUE> --outgoing_seq_num=<NUMBER>\n    example: test_bin --ip=127.0.0.1 --port 6575 --fix=FIX.4.2 --sender_comp_id=SENDER --target_comp_id=TARGET --outgoing_seq_num=14')
        
        sys.argv = ["test_bin", "--ip=127.0.0.1"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: Port is required')

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: Fix version string is required')

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: sender_comp_id is required')

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: target_comp_id is required')

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--quote_fill_this_side=3"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: quote side [3] is invalid. Valid values are 1 and 2')

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--quote_fill_this_side=1"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--outgoing_seq_num=12"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--highlight=[1,2,3]"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()
            self.assertEqual(script_utils.highlight, [1,2,3])

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--tag_value_for_fills=[1,VALUE_FILL]"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()
            self.assertEqual(script_utils.tag_value_for_fills, ('1', 'VALUE_FILL'))

        ip, port, fix, sender, target, outgoing_seq_num = script_utils.parse_options()
        self.assertEqual(ip, '127.0.0.1')
        self.assertEqual(port, 1234)
        self.assertEqual(fix, 'FIX.4.2')
        self.assertEqual(sender, 'TEST_SENDER')
        self.assertEqual(target, 'TEST_TARGET')
        self.assertEqual(outgoing_seq_num, 1)
        
        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--outgoing_seq_num=45"]
        self._assert_function_result(script_utils.parse_options, None, '')
        ip, port, fix, sender, target, outgoing_seq_num = script_utils.parse_options()
        self.assertEqual(ip, '127.0.0.1')
        self.assertEqual(port, 1234)
        self.assertEqual(fix, 'FIX.4.2')
        self.assertEqual(sender, 'TEST_SENDER')
        self.assertEqual(target, 'TEST_TARGET')
        self.assertEqual(outgoing_seq_num, 45)

        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--on-ack-reply-tags=%s" % ScriptUtilsTests.TEST_FILE_OK_PATH, \
                    "--on-fill-reply-tags=%s" % ScriptUtilsTests.TEST_FILE_OK_PATH, \
                    "--on-cancel-reply-tags=%s" % ScriptUtilsTests.TEST_FILE_OK_PATH, \
                    "--on-replace-reply-tags=%s" % ScriptUtilsTests.TEST_FILE_OK_PATH, \
                    "--on-quote-ack-reply-tags=%s" % ScriptUtilsTests.TEST_FILE_OK_PATH, \
                    "--on-quote-fill-reply-tags=%s" % ScriptUtilsTests.TEST_FILE_OK_PATH, \
        ]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()
        self.assertNotEqual(script_utils.on_ack_reply_tags, None)
        self.assertNotEqual(script_utils.on_fill_reply_tags, None)
        self.assertNotEqual(script_utils.on_cancel_reply_tags, None)
        self.assertNotEqual(script_utils.on_replace_reply_tags, None)
        self.assertNotEqual(script_utils.on_quote_ack_reply_tags, None)
        self.assertNotEqual(script_utils.on_quote_fill_reply_tags, None)

    def test_multiple_orders_params(self):
        script_utils = ScriptUtils('test_bin')
        # OK
        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--multiple_nb_orders=1000", "--multiple_interval=0.5"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()
            self.assertEqual(1000, script_utils.get_multiple_nb_orders())
            self.assertEqual(0.5, script_utils.get_multiple_interval())

        # default values
        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET"]
        with self.assertNotRaises(SystemExit):
            script_utils.parse_options()
            self.assertEqual(10, script_utils.get_multiple_nb_orders())
            self.assertEqual(0.1, script_utils.get_multiple_interval())

        # bad number of orders (not integer)
        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--multiple_nb_orders=1000abc", "--multiple_interval=0.5"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: multiple_nb_orders should be an integer.')

        # bad interval (not float)
        sys.argv = ["test_bin", "--ip=127.0.0.1", "--port=1234", "--fix=FIX.4.2", "--sender_comp_id=TEST_SENDER", \
                    "--target_comp_id=TEST_TARGET", "--multiple_nb_orders=1000", "--multiple_interval=0.1dfs"]
        self._assert_function_result(script_utils.parse_options, None, 'ERROR: multiple_interval should be a float number.')


if __name__ == "__main__":
    unittest.main()
