#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2021, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import unittest
import simplefix
from pyfixtest import BinarySession, FixServer, StdOutLogger
import struct
import helper_serverrunner
from contextlib import contextmanager
import sys, os
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO
import time
from datetime import datetime

FIX_VERSION_STR = 'FIX.4.2'
SENDER_COMP_ID = 'SENDER'
TARGET_COMP_ID = 'TARGET'

class ATPSessionTestIsolated(BinarySession):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        BinarySession.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)

class ATPSession(BinarySession):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        BinarySession.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)

    # utility function for timestamps
    def get_nanos_from_epoch(self):
        now = datetime.now() 
        secs_since_epoch = int(time.time())
        #return ((secs_since_epoch * 1000000) +  now.microsecond) * 100
        return 163828775463199000 # always sending the same timestamp so tests pass

    # reimplementations to enrich the message so the binary protocol understands them
    def get_logon_message(self):
        logon = BinarySession.get_logon_message(self)
        # for LogonResponse we need to add:
        # { "field" : "length", "type" : "uint16", "tag" : "10009" },
        # { "field" : "msgtype", "type" : "uint8", "tag" : "10035" },
        # { "field" : "seqNo", "type" : "uint32", "tag" : "34" } 
        # { "field" : "resultCode", "type" : "uint8", "tag" : "100150" },
        # { "field" : "clientSeqNum", "type" : "uint32", "tag" : "34" } 
        logon.append_pair('10009', '12') # length of LoginResponse from ATP protocol
        logon.append_pair('10035', '2') 
        logon.append_pair('100150', '0')
        logon.append_pair('34', self._outgoing_seq_num) # set the actual seq num...

        return logon

    def get_ack_message(self, order):
        # firt get the default message
        ack = BinarySession.get_ack_message(self, order) 

        # enrich with required tags
        # { "field" : "length", "type" : "uint16", "tag" : "10009" },
        # { "field" : "msgtype", "type" : "uint8", "tag" : "10035" },
        # { "field" : "seqNo", "type" : "uint32", "tag" : "34" } 
        # { "field" : "orderRef", "type" : "uint32", "tag" : "41" },
        # { "field" : "marketDataID", "type" : "uint32", "tag" : "100123" },
        # { "field" : "status", "type" : "uint8", "tag" : "100150" },
        # { "field" : "tradedQuantity", "type" : "uint8", "tag" : "14" },
        # { "field" : "timestamp", "type" : "uint64", "tag" : "10060" },
        # { "field" : "userTag", "type" : "uint64", "tag" : "10011" }
        ack.append_pair('10009', '36') # length of LoginResponse from ATP protocol
        ack.append_pair('10035', '6')
        ack.append_pair('34', self._outgoing_seq_num) # set the actual seq num...
        ack.append_pair('100123', '6') 
        ack.append_pair('100150', '%s' % 0x40) # Status 0x40 in ATP means ack
        ack.append_pair('10060', '%s' % self.get_nanos_from_epoch())

        # IMPORTANT: ATP sends orderID in userTag field.
        # just echo here what the used sent in the order
        ack.append_pair('10011', order.get('10011')) 

        # orderRef is picked from order's 34 (ids in ATP are the seqNums)
        ack.append_pair('41', order.get('34'))
        # IMPORTANT: increase the session seq Num, as this is a business message
        self._outgoing_seq_num += 1

        return ack

class BinarySessionTests(unittest.TestCase):
    PROTOCOL_FILE = "./test/binary_protocols/atp.json"
    PROTOCOL_FILE_BAD_SYNTAX = "./test/binary_protocols/atp_bad_syntax.json"

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_bad_syntax_in_protocol(self):
        atp = ATPSessionTestIsolated(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, None)
        self.assertIsNotNone(atp)
        with self.assertRaises(ValueError) as catched_exception:
            atp.set_protocol_file(BinarySessionTests.PROTOCOL_FILE_BAD_SYNTAX)
            atp.load_protocol()
        self.assertEqual(str(catched_exception.exception) , 'No JSON object could be decoded')

    def test_convert_incoming_login(self):
        atp = ATPSessionTestIsolated(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, None)
        self.assertIsNotNone(atp)
        atp.set_protocol_file(BinarySessionTests.PROTOCOL_FILE)
        atp.load_protocol()

        # construct a binary Login message
        raw = b''
        # length
        raw += struct.pack('<H', 47)
        # msgtype (1)
        raw += struct.pack('B', 1)
        # seqnum
        raw += struct.pack('<L', 1999)
        # protocol version
        raw += struct.pack('<H', 23)
        # senderID
        raw += 'Sender'.ljust(16, '\x00').encode()
        # password
        raw += 'Password'.ljust(16, '\x00').encode()
        # inactivityTimeout
        raw += struct.pack('<H', 30)
        # aptSeqNo
        raw += struct.pack('<L', 3121)
        fix = atp.convert_incoming(raw)
        self.assertIsNotNone(fix)

        self.assertEqual('47', fix.get('10009')) # in the protocol description lenght --> 10009
        self.assertEqual('1', fix.get('10035'))  # in the protocol description msgtype --> 10035
        self.assertEqual('1999', fix.get(simplefix.TAG_MSGSEQNUM)) 
        self.assertEqual('23', fix.get('10008'))
        self.assertEqual('TARGET', fix.get('56'))
        self.assertEqual('Password', fix.get('554'))
        self.assertEqual('30', fix.get('108'))
        self.assertEqual('3121', fix.get('10034'))

    def test_convert_outgoing_login_response(self):
        atp = ATPSessionTestIsolated(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, None)
        self.assertIsNotNone(atp)
        atp.set_protocol_file(BinarySessionTests.PROTOCOL_FILE)
        atp.load_protocol()
        fix = atp.get_logon_message()

        # enrich the fix message with the tags required to converto to binary
        fix.append_pair('10035', '2')  # msgtype uses tag 10035 to indentify what kind of message is
        fix.append_pair('10009', '12') # lengh tag is 10009
        fix.append_pair(simplefix.TAG_MSGSEQNUM, 1999) # seqNum is tag 34 (this one should already be set in a normal session)
        fix.append_pair('100150', '9') # resultCode is tag 100150

        # construct the expected binary message
        raw_expected = b''
        raw_expected += struct.pack('<H', 12)
        raw_expected += struct.pack('B', 2)
        raw_expected += struct.pack('<L', 1999)
        raw_expected += struct.pack('B', 9)
        raw_expected += struct.pack('<L', 1999)

        raw = atp.convert_outgoing(fix)

        # finally check that the expected one is equal to the generated
        self.assertEqual(raw_expected, raw)

    def test_convert_outgoing_logout_response(self):
        atp = ATPSession(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, None)
        self.assertIsNotNone(atp)
        atp.set_protocol_file(BinarySessionTests.PROTOCOL_FILE)
        atp.load_protocol()
        fix = atp.get_logout_message('TEST_LOGOUT')

        # enrich the fix message with the tags required to converto to binary
        fix.append_pair('10035', '4')  # msgtype uses tag 10035 to indentify what kind of message is
        fix.append_pair('10009', '40') # lengh tag is 10009
        fix.append_pair('10058', 9)    # reasonCode is tag 10058 
        fix.append_pair(simplefix.TAG_MSGSEQNUM, 1999) # seqNum is tag 34 (this one should already be set in a normal session)

        # construct the expected binary message
        raw_expected = b''
        raw_expected += struct.pack('<H', 40)
        raw_expected += struct.pack('B', 4)
        raw_expected += struct.pack('<L', 1999)
        raw_expected += struct.pack('B', 9)
        raw_expected += 'TEST_LOGOUT'.ljust(32, '\x00')

        raw = atp.convert_outgoing(fix)

        # finally check that the expected one is equal to the generated
        self.assertEqual(raw_expected, raw)

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class BinaryServerTests(helper_serverrunner.HelperServerRunner):
    PROTOCOL_FILE = "./test/binary_protocols/atp.json"

    def setUp(self):
        self.server = None
        self.client = None

    def tearDown(self):
        if self.client != None:
            self.client.close()
        if self.server != None:
            self.server.stop()

    def _get_server(self, port, expected_tag_fills=None, highlighted_tags=None):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        server = FixServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)
        if expected_tag_fills != None:
            server.set_expected_tag_in_new_orders_for_fills(expected_tag_fills[0], expected_tag_fills[1])
        if highlighted_tags != None:
            server.set_highlighted_tags(highlighted_tags)
        server.set_fix_session_class(ATPSession)
        server.set_protocol_file(BinaryServerTests.PROTOCOL_FILE)
        return server

    def test_logon(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        self._wait_client_connected(self.server)

        # construct a binary Login message
        raw = b''
        # length
        raw += struct.pack('<H', 47)
        # msgtype (1)
        raw += struct.pack('B', 1)
        # seqnum
        raw += struct.pack('<L', 1999)
        # protocol version
        raw += struct.pack('<H', 23)
        # senderID
        raw += 'Sender'.ljust(16, '\x00').encode()
        # password
        raw += 'Password'.ljust(16, '\x00').encode()
        # inactivityTimeout
        raw += struct.pack('<H', 30)
        # aptSeqNo
        raw += struct.pack('<L', 3121)

        with captured_output() as (out, _):
            self.client.send(raw)
            time.sleep(0.5)
            output = out.getvalue().strip()
        print ('----')
        print (output)
        print ('----')

        outputlines = output.split('\n')
        self.assertLessEqual(4, len(outputlines))
        self.assertEqual('RAW IN:    2f:00:01:cf:07:00:00:17:00:53:65:6e:64:65:72:00:00:00:00:00:00:00:00:00:00:50:61:73:73:77:6f:72:64:00:00:00:00:00:00:00:00:1e:00:31:0c:00:00', outputlines[0])
        self.assertEqual('RECV: A    8=FIX.4.2|9=100|35=A|49=SENDER|56=TARGET|10009=47|10035=1|34=1999|10008=23|56=Sender|554=Password|108=30|10034=3121|10=130|', outputlines[1])
        self.assertEqual('SENT: A    8=FIX.4.2|9=61|35=A|49=SENDER|56=TARGET|98=0|10009=12|10035=2|100150=0|34=1|10=014|', outputlines[2])
        self.assertEqual('RAW OUT:   0c:00:02:01:00:00:00:00:01:00:00:00', outputlines[3])


    def test_new_order(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        self._wait_client_connected(self.server)

        # construct a binary OrderAdd message
        neworder = b''
        # length
        neworder += struct.pack('<H', 35)
        # msgtype
        neworder += struct.pack('B', 5)
        # seqNum
        neworder += struct.pack('<L', 1999)
        # securityID
        neworder += struct.pack('<H', 223)
        # orderType
        neworder += struct.pack('B', 2)
        # timeinForce
        neworder += struct.pack('B', 3)
        # side
        neworder += struct.pack('B', 2)
        # quantity
        neworder += struct.pack('<L', 100)
        # price
        neworder += struct.pack('<Q', 12345)
        # ordercapacity
        neworder += struct.pack('B', 1)
        # account
        neworder += struct.pack('B', 2)
        # userTag
        neworder += struct.pack('<Q', 1234566)
        # flags
        neworder += struct.pack('B', 1)
        # tableSelect1
        neworder += struct.pack('B', 1)
        # shortCode1
        neworder += struct.pack('<L', 11)
        # tableSelect2
        neworder += struct.pack('B', 2)
        # shortCode3
        neworder += struct.pack('<L', 22)
        # tableSelect3
        neworder += struct.pack('B', 3)
        # shortCode3
        neworder += struct.pack('<L', 33)

        with captured_output() as (out, _):
            self.client.send(neworder)
            time.sleep(0.5)
            output = out.getvalue().strip()
        print ('----')
        print (output)
        print ('----')

        outputlines = output.split('\n')
        self.assertLessEqual(4, len(outputlines))
        self.assertEqual('RAW IN:    23:00:05:cf:07:00:00:df:00:02:03:02:64:00:00:00:39:30:00:00:00:00:00:00:01:02:86:d6:12:00:00:00:00:00:01:01:0b:00:00:00:02:16:00:00:00:03:21:00:00:00', outputlines[0])
        self.assertEqual('RECV: D    8=FIX.4.2|9=177|35=D|49=SENDER|56=TARGET|10009=35|10035=5|34=1999|55=223|40=2|59=3|54=2|38=100|44=12345|47=1|1=2|10011=1234566|100125=1|100126=1|100127=11|100128=2|100129=22|100130=3|100131=33|10=016|', outputlines[1])
        self.assertEqual('SENT: 8    8=FIX.4.2|9=180|35=8|49=SENDER|56=TARGET|37=tag37-1|17=tag17-1|20=0|55=223|54=2|150=0|39=0|151=100|14=0|6=0|10009=36|10035=6|34=1|100123=6|100150=64|10060=163828775463199000|10011=1234566|41=1999|10=045|', outputlines[2])
        self.assertEqual('RAW OUT:   24:00:06:01:00:00:00:cf:07:00:00:06:00:00:00:40:00:18:a1:78:3d:67:09:46:02:86:d6:12:00:00:00:00:00', outputlines[3])

    def test_new_order_debug_session(self):
        self.server = None
        while not self.server:
            port = self._get_port()
            self.server = self._start_server(self._get_server(port))
        self.client = self._connect_client(port)
        self._wait_client_connected(self.server)
        self.server._session.set_debug(True)

        # construct a binary OrderAdd message
        neworder = b''
        # length
        neworder += struct.pack('<H', 35)
        # msgtype
        neworder += struct.pack('B', 5)
        # seqNum
        neworder += struct.pack('<L', 1999)
        # securityID
        neworder += struct.pack('<H', 223)
        # orderType
        neworder += struct.pack('B', 2)
        # timeinForce
        neworder += struct.pack('B', 3)
        # side
        neworder += struct.pack('B', 2)
        # quantity
        neworder += struct.pack('<L', 100)
        # price
        neworder += struct.pack('<Q', 12345)
        # ordercapacity
        neworder += struct.pack('B', 1)
        # account
        neworder += struct.pack('B', 2)
        # userTag
        neworder += struct.pack('<Q', 1234566)
        # flags
        neworder += struct.pack('B', 1)
        # tableSelect1
        neworder += struct.pack('B', 1)
        # shortCode1
        neworder += struct.pack('<L', 11)
        # tableSelect2
        neworder += struct.pack('B', 2)
        # shortCode3
        neworder += struct.pack('<L', 22)
        # tableSelect3
        neworder += struct.pack('B', 3)
        # shortCode3
        neworder += struct.pack('<L', 33)

        with captured_output() as (out, _):
            self.client.send(neworder)
            time.sleep(0.5)
            output = out.getvalue().strip()
        print ('----')
        print (output)
        print ('----')

        outputlines = output.split('\n')
        self.assertLessEqual(6, len(outputlines))
        self.assertEqual('RAW IN:    23:00:05:cf:07:00:00:df:00:02:03:02:64:00:00:00:39:30:00:00:00:00:00:00:01:02:86:d6:12:00:00:00:00:00:01:01:0b:00:00:00:02:16:00:00:00:03:21:00:00:00', outputlines[0])
        self.assertEqual('DECODED:   length=35|msgtype=5|seqNo=1999|securityID=223|orderType=2|timeInForce=3|side=2|quantity=100|price=12345|orderCapacity=1|account=2|userTag=1234566|flags=1|tableSelect1=1|shortCode1=11|tableSelect2=2|shortCode2=22|tableSelect3=3|shortCode3=33|', outputlines[1])
        self.assertEqual('RECV: D    8=FIX.4.2|9=177|35=D|49=SENDER|56=TARGET|10009=35|10035=5|34=1999|55=223|40=2|59=3|54=2|38=100|44=12345|47=1|1=2|10011=1234566|100125=1|100126=1|100127=11|100128=2|100129=22|100130=3|100131=33|10=016|', outputlines[2])
        self.assertEqual('SENT: 8    8=FIX.4.2|9=180|35=8|49=SENDER|56=TARGET|37=tag37-1|17=tag17-1|20=0|55=223|54=2|150=0|39=0|151=100|14=0|6=0|10009=36|10035=6|34=1|100123=6|100150=64|10060=163828775463199000|10011=1234566|41=1999|10=045|', outputlines[3])
        self.assertEqual('ENCODED:   length=36|msgtype=6|seqNo=1|orderRef=1999|marketDataID=6|status=64|tradedQuantity=0|timestamp=163828775463199000|userTag=1234566|', outputlines[4])
        self.assertEqual('RAW OUT:   24:00:06:01:00:00:00:cf:07:00:00:06:00:00:00:40:00:18:a1:78:3d:67:09:46:02:86:d6:12:00:00:00:00:00', outputlines[5])

if __name__ == "__main__":
    unittest.main()