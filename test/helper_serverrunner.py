#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import serverexample
import unittest
import simplefix
import threading
import time
import socket
import select
from random import randint

PORTS_BEGIN = 49152
PORTS_END = 65535

class HelperServerRunner(unittest.TestCase):
    def setUp(self):
        self._server = None 

    def tearDown(self):
        pass

    def _server_thread(self, server):
        server.start()
        while server.is_running():
            time.sleep(0.1)
            self.assertEqual(server.is_running(), True)
        self.assertEqual(server.is_running(), False)

    def _client_thread(self, client):
        client.start()

    def _start_client(self, client, ctrl_c_after_ms=0):
        client_thread_handler = threading.Thread(
            target=self._client_thread,
            args=(client,)
        )
        client_thread_handler.daemon = True
        client_thread_handler.start()
        timeout = 4
        while not client.is_running() and timeout > 0:
            time.sleep(0.2)
            timeout -= 0.2
            pass
        if timeout < 0:
            return None
        
        return client

    def _start_server(self, server):

        server_thread_handler = threading.Thread(
            target=self._server_thread,
            args=(server,)
        )
        server_thread_handler.daemon = True
        server_thread_handler.start()
        timeout = 4
        while not server.is_running() and timeout > 0:
            time.sleep(0.2)
            timeout -= 0.2
            pass
        if timeout < 0:
            return None
        return server 

    def _connect_client(self, port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('127.0.0.1', port))
        return sock

    def _get_port(self):
        global PORTS_BEGIN
        global PORTS_END

        return randint(PORTS_BEGIN, PORTS_END)

    def _wait_server_not_running(self, server, timeout=3):
        while timeout > 0 and server.is_running():
            timeout -= 0.1
            time.sleep(0.1)

    def _wait_client_connected(self, server, timeout=3):
        while timeout > 0 and server._session is None:
            timeout -= 0.1
            time.sleep(0.1)

if __name__ == "__main__":
    unittest.main()
