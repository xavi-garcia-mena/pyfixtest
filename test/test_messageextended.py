#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import unittest
import simplefix
from pyfixtest import RepeatingGroup, FixMessageExtended

class RepeatingGroupTests(unittest.TestCase):
    def test_parse_basic_repeating_group(self):
        repeating_group = RepeatingGroup(123, [222, 333, 444])
        repeating_group_no_ok = RepeatingGroup(123, [222])

        test_msg = simplefix.FixMessage()
        test_msg.append_pair('1', '2')
        test_msg.append_pair('2', '2')
        test_msg.append_pair('123', '2') # 123 is the repeating group
        test_msg.append_pair('222', '222_1')
        test_msg.append_pair('333', '333_1')
        test_msg.append_pair('444', '444_1')
        test_msg.append_pair('222', '222_2')
        test_msg.append_pair('333', '333_2')
        test_msg.append_pair('444', '444_2')

        self.assertTrue(repeating_group.fill_from_message(test_msg))
        self.assertFalse(repeating_group_no_ok.fill_from_message(test_msg))


    def test_parse_complex_repeating_group(self):
        repeating_group = RepeatingGroup(123, [222, RepeatingGroup(1234, [555, 666]), 444])
        repeating_group_2 = RepeatingGroup(123, [222, RepeatingGroup(1234, [555, 666]), 444])

        test_msg = simplefix.FixMessage()
        test_msg.append_pair(simplefix.TAG_BEGINSTRING, 'BLAH')
        test_msg.append_pair('35', 'D')
        test_msg.append_pair('1', '2')
        test_msg.append_pair('2', '2')
        test_msg.append_pair('123', '2') # 123 is the repeating group

        test_msg.append_pair('222', '222_1')

        test_msg.append_pair('1234', '3')
        test_msg.append_pair('555', '555_1_a')
        test_msg.append_pair('666', '666_1_a')
        test_msg.append_pair('555', '555_2_a')
        test_msg.append_pair('666', '666_2_a')
        test_msg.append_pair('555', '555_3_a')
        test_msg.append_pair('666', '666_3_a')

        test_msg.append_pair('444', '444_1')

        test_msg.append_pair('222', '222_2')
        test_msg.append_pair('1234', '2')
        test_msg.append_pair('555', '555_1_b')
        test_msg.append_pair('666', '666_1_b')
        test_msg.append_pair('555', '555_2_b')
        test_msg.append_pair('666', '666_2_b')
        test_msg.append_pair('444', '444_2')

        self.assertTrue(repeating_group.fill_from_message(test_msg))
        self.assertEqual(str(repeating_group), \
                '123=2|222=222_1|1234=3|555=555_1_a|666=666_1_a|555=555_2_a|666=666_2_a|555=555_3_a|666=666_3_a|444=444_1|222=222_2|1234=2|555=555_1_b|666=666_1_b|555=555_2_b|666=666_2_b|444=444_2|')

        # check equal operator
        self.assertTrue(repeating_group_2.fill_from_message(test_msg))
        self.assertTrue(repeating_group == repeating_group_2)
        

    def test_parse_more_complex_repeating_group(self):
        repeating_group = RepeatingGroup(123, [222, RepeatingGroup(1234, [555, RepeatingGroup(12345, [666, 888])]),  444])
        repeating_group_equal = RepeatingGroup(123, [222, RepeatingGroup(1234, [555, RepeatingGroup(12345, [666, 888])]),  444])

        test_msg = simplefix.FixMessage()
        test_msg.append_pair(simplefix.TAG_BEGINSTRING, 'BLAH')
        test_msg.append_pair('35', 'D')
        test_msg.append_pair('1', '2')
        test_msg.append_pair('2', '2')
        test_msg.append_pair('123', '2') # 123 is the repeating group

        test_msg.append_pair('222', '222_1')

        test_msg.append_pair('1234', '3')

        test_msg.append_pair('555', '555_1_a')
        test_msg.append_pair('12345', '2')
        test_msg.append_pair('666', '666_1')
        test_msg.append_pair('888', '888_1')
        test_msg.append_pair('666', '666_2')
        test_msg.append_pair('888', '888_2')

        test_msg.append_pair('555', '555_1_b')
        test_msg.append_pair('12345', '0')

        test_msg.append_pair('555', '555_1_c')
        test_msg.append_pair('12345', '1')
        test_msg.append_pair('666', '666_1')
        test_msg.append_pair('888', '888_1')

        test_msg.append_pair('444', '444_1')

        test_msg.append_pair('222', '222_2')
        test_msg.append_pair('1234', '0')
        test_msg.append_pair('444', '444_1')

        self.assertTrue(repeating_group.fill_from_message(test_msg))
        self.assertEqual(str(repeating_group), \
                '123=2|222=222_1|1234=3|555=555_1_a|12345=2|666=666_1|888=888_1|666=666_2|888=888_2|555=555_1_b|12345=0|555=555_1_c|12345=1|666=666_1|888=888_1|444=444_1|222=222_2|1234=0|444=444_1|')

        # parse a different repeating group, it should fail
        repeating_group_2 = RepeatingGroup(123, [222, 444])
        self.assertFalse(repeating_group_2.fill_from_message(test_msg))

        # check equal operator
        self.assertTrue(repeating_group_equal.fill_from_message(test_msg))
        self.assertEqual(repeating_group, repeating_group_equal)

        # define a different repeating group
        repeating_group_different = RepeatingGroup(1111, [333, 555])
        test_msg_2 = simplefix.FixMessage()
        test_msg_2.append_pair('1', '11')
        test_msg_2.append_pair('54', '54')
        test_msg_2.append_pair('1111', '2')
        test_msg_2.append_pair('333', '333_a')
        test_msg_2.append_pair('555', '555_a')
        test_msg_2.append_pair('333', '333_b')
        test_msg_2.append_pair('555', '555_b')

        self.assertTrue(repeating_group_different.fill_from_message(test_msg_2))
        self.assertNotEqual(repeating_group, repeating_group_different)

    def test_is_sub_tag(self):
        repeating_group = RepeatingGroup(123, [ 11, 22, RepeatingGroup( 1234, [ 12, 13 ] ) ] )
        sub_tags = [ 11, 22, 12, 13 ]
        for tag in sub_tags:
            self.assertTrue(repeating_group.is_sub_tag(tag))

        not_sub_tags = [ 54, '34', '10' ]
        for tag in not_sub_tags:
            self.assertFalse(repeating_group.is_sub_tag(tag))

    def test_bad_fill_from_message(self):
        msg = simplefix.FixMessage()
        repeating_group = RepeatingGroup(1234, [11, 22])
        self.assertFalse(repeating_group.fill_from_message(msg, error_if_not_found=True))

    def test_repeating_group_incomplete(self):
        msg = simplefix.FixMessage()
        msg.append_pair('1234', '2')
        msg.append_pair('11', '11')
        msg.append_pair('22', '22')
        repeating_group = RepeatingGroup(1234, [11, 22])
        self.assertFalse(repeating_group.fill_from_message(msg, error_if_not_found=True))

    def test_repeating_group_no_items_is_not_integer(self):
        msg = simplefix.FixMessage()
        msg.append_pair('1234', 'a')
        msg.append_pair('11', '11')
        msg.append_pair('22', '22')
        repeating_group = RepeatingGroup(1234, [11, 22])
        self.assertFalse(repeating_group.fill_from_message(msg, error_if_not_found=True))

    def test_fail_parsin_sub_repeating_group(self):
        msg = simplefix.FixMessage()
        msg.append_pair('1234', '1')
        msg.append_pair('11', '11')
        msg.append_pair('12345', 'a')
        msg.append_pair('22', '22')
        repeating_group = RepeatingGroup(1234, [11, RepeatingGroup(12345, [22])])
        self.assertFalse(repeating_group.fill_from_message(msg, error_if_not_found=True))

    def test_repeating_group_not_equal(self):
        msg = simplefix.FixMessage()
        msg.append_pair('1234', '1')
        msg.append_pair('11', '11')
        msg.append_pair('22', '22')
        repeating_group = RepeatingGroup(1234, [11, 22])
        self.assertTrue(repeating_group.fill_from_message(msg))

        repeating_group_2 = RepeatingGroup(1234, [11, 22])
        self.assertNotEqual(repeating_group, repeating_group_2)
        
        msg2 = simplefix.FixMessage()
        msg2.append_pair('1234', '1')
        msg2.append_pair('11', '11')
        msg2.append_pair('22', '2222')

        self.assertTrue(repeating_group_2.fill_from_message(msg2))
        self.assertTrue(repeating_group != repeating_group_2)

        repeating_group_2.fill_from_message(msg)
        self.assertEqual(repeating_group, repeating_group_2)

    def test_set_values(self):
        group = RepeatingGroup(123, [222, RepeatingGroup(1234, [555, RepeatingGroup(12345, [666, 888])]),  444])
        values = []
        values.append(('123', '2')) # 123 is the repeating group

        values.append(('222', '222_1'))

        values.append(('1234', '3'))

        values.append(('555', '555_1_a'))
        values.append(('12345', '2'))
        values.append(('666', '666_1'))
        values.append(('888', '888_1'))
        values.append(('666', '666_2'))
        values.append(('888', '888_2'))

        values.append(('555', '555_1_b'))
        values.append(('12345', '0'))

        values.append(('555', '555_1_c'))
        values.append(('12345', '1'))
        values.append(('666', '666_1'))
        values.append(('888', '888_1'))

        values.append(('444', '444_1'))

        values.append(('222', '222_2'))
        values.append(('1234', '0'))
        values.append(('444', '444_1'))
        self.assertTrue(group.set_values(values))


class FixMessageExtendedTests(unittest.TestCase):
    def test_message_extended_basic(self):
        # very basic test to verify that FixMessageExtended works like simplefix.FixMessage
        msg = FixMessageExtended()
        msg.append_pair('1', '1')
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2')
        msg.append_pair(simplefix.TAG_MSGTYPE, 'D')
        msg.append_pair('54', '54')

        self.assertEqual(msg.get('5555'), None)
        self.assertEqual(msg.get('1'), b'1')
        self.assertEqual(msg.get('54'), b'54')

        self.assertEqual(msg.encode().replace(b'\x01', b'|').decode(), '8=FIX.4.2|9=15|35=D|1=1|54=54|10=150|')

    def test_basic_repeating_group(self):
        repeating_group = RepeatingGroup(123, [222, 333, 444])

        test_msg = FixMessageExtended()
        test_msg.append_pair('1', '2')
        test_msg.append_pair('2', '2')
        test_msg.append_pair('123', '2') # 123 is the repeating group
        test_msg.append_pair('222', '222_1')
        test_msg.append_pair('333', '333_1')
        test_msg.append_pair('444', '444_1')
        test_msg.append_pair('222', '222_2')
        test_msg.append_pair('333', '333_2')
        test_msg.append_pair('444', '444_2')

        repeating_group_in_msg = test_msg.get_repeating_group(123)
        self.assertEqual(repeating_group_in_msg, None) # repeating group not defined in msg

        test_msg.add_repeating_group_definition(repeating_group)
        repeating_group_in_msg = test_msg.get_repeating_group(123)
        self.assertNotEqual(repeating_group_in_msg, None) # repeating group is now defined in msg

        # create and fill the expected values for repeating group
        repeating_group_compare = []
        repeating_group_compare.append((b'123', b'2'))
        repeating_group_compare.append((b'222', b'222_1'))
        repeating_group_compare.append((b'333', b'333_1'))
        repeating_group_compare.append((b'444', b'444_1'))
        repeating_group_compare.append((b'222', b'222_2'))
        repeating_group_compare.append((b'333', b'333_2'))
        repeating_group_compare.append((b'444', b'444_2'))

        #ensure it's equal to the one obtained from the message
        self.assertEqual(repeating_group_compare, repeating_group_in_msg)
       
        # read values of repeating group for a simplefix.FixMessage
        # message content is different, but the repeating group value is the same
        repeating_group_simplefix = RepeatingGroup(123, [222, 333, 444])
        test_msg_simple = FixMessageExtended()
        test_msg_simple.append_pair('133', '133')
        test_msg_simple.append_pair('22', '22')
        test_msg_simple.append_pair('123', '2') # 123 is the repeating group
        test_msg_simple.append_pair('222', '222_1')
        test_msg_simple.append_pair('333', '333_1')
        test_msg_simple.append_pair('444', '444_1')
        test_msg_simple.append_pair('222', '222_2')
        test_msg_simple.append_pair('333', '333_2')
        test_msg_simple.append_pair('444', '444_2')
        test_msg_simple.append_pair('56', '56')

        repeating_group_simplefix.fill_from_message(test_msg_simple)
        # check that it's equal to the one in the message
        self.assertEqual(repeating_group_simplefix.get_values(), repeating_group_in_msg)

    def test_add_repeating_group_definition_to_message(self):
        msg = FixMessageExtended()
        msg.append_pair('1', '1')

        repeating_group = RepeatingGroup(123, [222, 333, 444])
        repeating_group_values = []
        repeating_group_values.append((b'123', b'2'))
        repeating_group_values.append((b'222', b'222_1'))
        repeating_group_values.append((b'333', b'333_1'))
        repeating_group_values.append((b'444', b'444_1'))
        repeating_group_values.append((b'222', b'222_2'))
        repeating_group_values.append((b'333', b'333_2'))
        repeating_group_values.append((b'444', b'444_2'))

        # try to add the repeating group pairs
        # message should not be modified as the repeating group is not defined yet
        lenght_before_adding_tags = len(msg.pairs)
        msg.append_repeating_group_pairs(repeating_group_values)
        self.assertEqual(lenght_before_adding_tags, len(msg.pairs))

        # check that adding an empty list does nothing
        msg.append_repeating_group_pairs([])
        self.assertEqual(lenght_before_adding_tags, len(msg.pairs))

        # ensure it is NOT a repeating group now in the message
        self.assertFalse(msg.is_repeating_group('123'))

        # add the repeating group to the msg and add the pairs, they should be added now
        msg.add_repeating_group_definition(repeating_group)
        msg.append_repeating_group_pairs(repeating_group_values)
        self.assertTrue(lenght_before_adding_tags < len(msg.pairs)) 

        # get the values of the repeating group in the msg
        # ensure it is a repeating group now in the message
        self.assertTrue(msg.is_repeating_group('123'))
        values = msg.get_repeating_group(123)
        self.assertEqual(values, repeating_group_values)

    def test_get_repeating_group_from_not_filled_message(self):
        msg = FixMessageExtended()
        repeating_group = RepeatingGroup(1234, [111, 222])
        # add some values, but not the ones for the repeating group
        msg.append_pair('1234', '2')
        msg.append_pair('54', '54')
        # add the repeating group to the msg
        msg.add_repeating_group_definition(repeating_group)

        values = msg.get_repeating_group(1234)
        self.assertEqual(values, None)

    def test_tag_belongs_to_repeating_group(self):
        msg = FixMessageExtended()
        repeating_group = RepeatingGroup(1234, [11, RepeatingGroup(12345, [22, 33])])
       
        msg.add_repeating_group_definition(repeating_group)

        tags = ['1234', '11', '12345', '22', '33' ]
        for tag in tags:
            self.assertTrue(msg.tag_belongs_to_repeating_group(tag))
            self.assertTrue(msg.tag_belongs_to_repeating_group(int(tag)))

        tags_not_in_rep_group = [ '123', '1', '3' ]
        for tag in tags_not_in_rep_group:
            self.assertFalse(msg.tag_belongs_to_repeating_group(tag))
            self.assertFalse(msg.tag_belongs_to_repeating_group(int(tag)))


    def test_parse_no_items_equal_zero(self):
        msg = FixMessageExtended()
        repeating_group = RepeatingGroup(1234, [11, RepeatingGroup(12345, [22, 33])])
        msg.add_repeating_group_definition(repeating_group)
        msg.append_pair('1234', '0')
        self.assertTrue(repeating_group.fill_from_message(msg))
        self.assertEqual(len(repeating_group.get_values()), 1)
        self.assertEqual(repeating_group.get_values()[0][0], b'1234')
        self.assertEqual(repeating_group.get_values()[0][1], b'0')
        
        values = msg.get_repeating_group(1234)
        self.assertEqual(len(values), 1)
        self.assertEqual(values[0][0], b'1234')
        self.assertEqual(values[0][1], b'0')

    def test_optional_tags_in_repeating_group(self):
        msg = FixMessageExtended()
        rp = RepeatingGroup(1234, [11, 12, 13, 14, 15], True)
        msg.add_repeating_group_definition(rp)
        # add 2 groups, we won't set some tags
        msg.append_pair('1234', '2')
        msg.append_pair('11', 'value_11')
        # we skip tag 12
        msg.append_pair('13', 'value_13')
        # we skip tag 14
        msg.append_pair('15', 'value_15')
        # SECOND ITEM
        # skip tag 11
        msg.append_pair('12', 'value_12')
        # skip tag 13
        msg.append_pair('14', 'value_14')
        # skip tag 15

        values = msg.get_repeating_group(1234)
        self.assertNotEqual(values, None)
        self.assertEqual(len(values), 11)
        self.assertEqual(values[0][0], b'1234')
        self.assertEqual(values[0][1], b'2')
        self.assertEqual(values[1][0], b'11')
        self.assertEqual(values[1][1], b'value_11')
        self.assertEqual(values[2][0], b'12')
        self.assertEqual(values[2][1], None)
        self.assertEqual(values[3][0], b'13')
        self.assertEqual(values[3][1], b'value_13')
        self.assertEqual(values[4][0], b'14')
        self.assertEqual(values[4][1], None)
        self.assertEqual(values[5][0], b'15')
        self.assertEqual(values[5][1], b'value_15')
        self.assertEqual(values[6][0], b'11')
        self.assertEqual(values[6][1], None)
        self.assertEqual(values[7][0], b'12')
        self.assertEqual(values[7][1], b'value_12')
        self.assertEqual(values[8][0], b'13')
        self.assertEqual(values[8][1], None)
        self.assertEqual(values[9][0], b'14')
        self.assertEqual(values[9][1], b'value_14')
        self.assertEqual(values[10][0],b'15')
        self.assertEqual(values[10][1], None)

    def test_tag_equal(self):
        msg = FixMessageExtended()
        rep_group = RepeatingGroup(1234, [11, RepeatingGroup(2222, [22, 33])])
        msg.append_pair('1234', '2')
        msg.append_pair('11', '11_a')
        msg.append_pair('2222', '1')
        msg.append_pair('22', '22_a')
        msg.append_pair('33', '33_a')
        msg.append_pair('11', '11_b')
        msg.append_pair('2222', '1')
        msg.append_pair('22', '22_b')
        msg.append_pair('33', '33_b')
        msg.append_pair('54', '54')
        msg.add_repeating_group_definition(rep_group)

        values = msg.get_repeating_group(1234)
        self.assertFalse(len(values) == 0)

        msg2 = FixMessageExtended()
        self.assertFalse(msg.tag_equal(msg2, 1234))

        msg2.add_repeating_group_definition(rep_group)
        self.assertFalse(msg.tag_equal(msg2, 1234))

        msg2.append_pair('1234', '2')
        msg2.append_pair('11', '11_a')
        msg2.append_pair('2222', '1')
        msg2.append_pair('22', '22_a')
        msg2.append_pair('33', '33_a')
        msg2.append_pair('11', '11_b')
        msg2.append_pair('2222', '1')
        msg2.append_pair('22', '22_b')
        msg2.append_pair('33', '33_c') # this tag is different
        self.assertFalse(msg.tag_equal(msg2, 1234))

        msg3 = FixMessageExtended()
        msg3.append_pair('1234', '2')
        msg3.append_pair('11', '11_a')
        msg3.append_pair('2222', '1')
        msg3.append_pair('22', '22_a')
        msg3.append_pair('33', '33_a')
        msg3.append_pair('11', '11_b')
        msg3.append_pair('2222', '1')
        msg3.append_pair('22', '22_b')
        msg3.append_pair('33', '33_b')
        msg3.add_repeating_group_definition(rep_group)
        self.assertTrue(msg.tag_equal(msg3, 1234))

        not_extended_msg = simplefix.FixMessage()
        not_extended_msg.append_pair('1234', '2')
        not_extended_msg.append_pair('11', '11_a')
        not_extended_msg.append_pair('2222', '1')
        not_extended_msg.append_pair('22', '22_a')
        not_extended_msg.append_pair('33', '33_a')
        not_extended_msg.append_pair('11', '11_b')
        not_extended_msg.append_pair('2222', '1')
        not_extended_msg.append_pair('22', '22_b')
        not_extended_msg.append_pair('33', '33_b')
        not_extended_msg.append_pair('54', '54')

        self.assertTrue(msg.tag_equal(not_extended_msg, '54')) # it's not a repeating group so simple comparison
        self.assertFalse(msg.tag_equal(not_extended_msg, '1234'))

        extended_msg = FixMessageExtended()
        extended_msg.append_pair('123456', '2')
        extended_msg.append_pair('11', '11_a')
        extended_msg.append_pair('2222', '1')
        extended_msg.append_pair('22', '22_a')
        extended_msg.append_pair('33', '33_a')
        extended_msg.append_pair('11', '11_b')
        extended_msg.append_pair('2222', '1')
        extended_msg.append_pair('22', '22_b')
        extended_msg.append_pair('33', '33_b')
        extended_msg.append_pair('54', '54')
        rep_group_2 = RepeatingGroup(123456, [11, RepeatingGroup(2222, [22, 33])])
        extended_msg.add_repeating_group_definition(rep_group_2)

        self.assertTrue(msg.tag_equal(extended_msg, '54'))
        self.assertFalse(msg.tag_equal(extended_msg, '123456'))

if __name__ == "__main__":
    unittest.main()

