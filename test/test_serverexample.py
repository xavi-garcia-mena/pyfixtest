#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import serverexample
import unittest
import simplefix
import threading
import time
import socket
import select
from random import randint
import helper_serverrunner

SENDER_COMP_ID="Sender"
TARGET_COMP_ID="Target"
FIX_VERSION_STR="FIX.4.2"

class ExampleServerTests(helper_serverrunner.HelperServerRunner):

    def _get_server(self, port):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR

        return serverexample.ExampleServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)

    def _test_basic_message(self, msgtype):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        
        port = self._get_port()
        self._start_server(self._get_server(port))
        client = self._connect_client(port)
        logon = simplefix.FixMessage()
        logon.append_pair(simplefix.TAG_BEGINSTRING, FIX_VERSION_STR, header=True)
        logon.append_pair(simplefix.TAG_MSGTYPE, msgtype)
        logon.append_pair(simplefix.TAG_SENDER_COMPID, TARGET_COMP_ID, header=True)
        logon.append_pair(simplefix.TAG_TARGET_COMPID, SENDER_COMP_ID, header=True)
        logon.append_utc_timestamp(simplefix.TAG_SENDING_TIME, header=True)
        logon.append_pair(simplefix.TAG_MSGSEQNUM, '1', header=True)
        client.send(logon.encode())
        reply_raw = client.recv(1024)

        parser = simplefix.FixParser()
        parser.append_buffer(reply_raw)
        reply = parser.get_message()

        self.assertTrue(reply != None)
        self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), msgtype)
        self.assertEqual(reply.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(reply.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(reply.get(simplefix.TAG_MSGSEQNUM), '1')
        self.assertTrue(len(reply.get(simplefix.TAG_SENDING_TIME)) > 0)

    def test_logon(self):
        self._test_basic_message(simplefix.MSGTYPE_LOGON)

    def test_heartbeat(self):
        self._test_basic_message(simplefix.MSGTYPE_HEARTBEAT)

    def test_logout(self):
        self._test_basic_message(simplefix.MSGTYPE_LOGOUT)

    def _parse_fix_msg(self, raw_msg):
        parser = simplefix.FixParser()
        parser.append_buffer(raw_msg)
        msg = parser.get_message()
        return msg

    def _test_basic_execution_report_message(self, execution_report, order, isFill):
        seq_num = execution_report.get(simplefix.TAG_MSGSEQNUM)
        self.assertEqual(execution_report.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_EXECUTION_REPORT)
        self.assertEqual(execution_report.get(simplefix.TAG_ORDERID), 'tag37-%s' % seq_num)
        self.assertEqual(execution_report.get(simplefix.TAG_EXECID), 'tag17-%s' % seq_num)
        self.assertEqual(execution_report.get(simplefix.TAG_EXECTRANSTYPE), '0')
        self.assertEqual(execution_report.get(simplefix.TAG_SYMBOL), order.get(simplefix.TAG_SYMBOL))
        self.assertEqual(execution_report.get(simplefix.TAG_SIDE), order.get(simplefix.TAG_SIDE))
        if not isFill:
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), '0')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), '0')
            self.assertEqual(execution_report.get('151'), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), '0')
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), '0')
            # check the extra tags added in the example
            self.assertEqual(execution_report.get('8013'), '8013_test')
            self.assertEqual(execution_report.get('8014'), '8014_test')
        else:
            self.assertEqual(execution_report.get(simplefix.TAG_EXECTYPE), '2')
            self.assertEqual(execution_report.get(simplefix.TAG_ORDSTATUS), '2')
            self.assertEqual(execution_report.get('151'), '0')
            self.assertEqual(execution_report.get(simplefix.TAG_CUMQTY), order.get(simplefix.TAG_ORDERQTY))
            self.assertEqual(execution_report.get(simplefix.TAG_AVGPX), order.get(simplefix.TAG_PRICE))
            # check the extra tags added in the example
            self.assertEqual(execution_report.get('12345'), '12345_test')
            self.assertEqual(execution_report.get('1999'), '1999_test')

    def test_new_order_single_ack(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        order = simplefix.FixMessage()
        order = server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')

        client.send(order.encode())
        ready_to_read, _, _ = select.select([client], [], [], 1)
        self.assertTrue(len(ready_to_read) > 0)
        reply_raw = client.recv(1024)
        ack = self._parse_fix_msg(reply_raw)
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, False)

        ready_to_read, _, _ = select.select([client], [], [], 1)
        self.assertTrue(len(ready_to_read) == 0)

    def test_new_order_single_fill(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        order = simplefix.FixMessage()
        order = server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY)
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL')
        order.append_pair(simplefix.TAG_TEXT, 'GET_FILL')

        # set the tag value that we want to find out to send fills (it could be any tag)
        server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_TEXT, 'GET_FILL')

        client.send(order.encode())

        ready_to_read, _, _ = select.select([client], [], [], 1)
        self.assertTrue(len(ready_to_read) > 0)
        reply_raw = client.recv(1024)
        ack = self._parse_fix_msg(reply_raw)
        self.assertTrue(ack != None)
        self._test_basic_execution_report_message(ack, order, False)

        ready_to_read, _, _ = select.select([client], [], [], 1)
        self.assertTrue(len(ready_to_read) > 0)
        reply_raw = client.recv(1024)
        fill = self._parse_fix_msg(reply_raw)
        self.assertTrue(fill != None)
        self._test_basic_execution_report_message(fill, order, True)

    def test_quote(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        order = simplefix.FixMessage()
        order = server._session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_QUOTE)

        client.send(order.encode())

        ready_to_read, _, _ = select.select([client], [], [], 1)
        self.assertTrue(len(ready_to_read) > 0)
        reply_raw = client.recv(1024)
        reply = self._parse_fix_msg(reply_raw)
        self.assertTrue(reply != None)
        self.assertEqual(reply.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_QUOTE_RESPONSE)
        self.assertEqual(reply.get('693'), 'QUOTE_ID')
        self.assertEqual(reply.get('694'), '6')

        ready_to_read, _, _ = select.select([client], [], [], 1)
        self.assertTrue(len(ready_to_read) == 0)

if __name__ == "__main__":
    unittest.main()
