#! /usr/bin/env python
########################################################################
# dailytest
# Copyright (C) 2021 Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import HTMLReportField, HTMLReportFields, HTMLReportParser
import unittest
import os
import sys
from contextlib import contextmanager
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class HTMLReportParserTests(unittest.TestCase):
    TEST_FILE_OK_PATH = "./test_html_report_ok.json"
    TEST_FILE_BAD_TAG_REPORT_PATH = "./test_html_report_bad_tag_report.json"
    TEST_FILE_BAD_DESCRIPTION_FIELDS_PATH = "./test_html_report_bad_tag_description_fields.json"
    TEST_FILE_BAD_NAME_PATH = "./test_html_report_bad_tag_name.json"
    TEST_FILE_BAD_ID_PATH = "./test_html_report_bad_tag_id.json"
    TEST_FILE_BAD_EXPECTED_ACTUAL_FIELDS_PATH = "./test_html_report_bad_tag_expected_actual_fields.json"

    def setUp(self):
        json_data_ok = '{ "report": \
                            { \
                            "description_fields" : \
                            [ \
                                    { "name" : "OrderQty",          "id" : "38" }, \
                                    { "name" : "Side",              "id" : "54" }, \
                                    { "name" : "ClOrdID",           "id" : "11" } \
                            ], \
                            "expected_actual_fields" : \
                            [ \
                                    { "name" : "Status",            "id" : "150" }, \
                                    { "name" : "Text",              "id" : "58" } \
                            ] \
                            } \
                    }'

        file = open(HTMLReportParserTests.TEST_FILE_OK_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_ok = '{ "reportxxxx": \
                    { \
                    "description_fields" : \
                    [ \
                            { "name" : "OrderQty",          "id" : "38" }, \
                            { "name" : "Side",              "id" : "54" }, \
                            { "name" : "ClOrdID",           "id" : "11" } \
                    ], \
                    "expected_actual_fields" : \
                    [ \
                            { "name" : "Status",            "id" : "150" }, \
                            { "name" : "Text",              "id" : "58" } \
                    ] \
                    } \
            }'

        file = open(HTMLReportParserTests.TEST_FILE_BAD_TAG_REPORT_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_ok = '{ "report": \
                    { \
                    "description_fieldsxxx" : \
                    [ \
                            { "name" : "OrderQty",          "id" : "38" }, \
                            { "name" : "Side",              "id" : "54" }, \
                            { "name" : "ClOrdID",           "id" : "11" } \
                    ], \
                    "expected_actual_fields" : \
                    [ \
                            { "name" : "Status",            "id" : "150" }, \
                            { "name" : "Text",              "id" : "58" } \
                    ] \
                    } \
            }'

        file = open(HTMLReportParserTests.TEST_FILE_BAD_DESCRIPTION_FIELDS_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_ok = '{ "report": \
                    { \
                    "description_fields" : \
                    [ \
                            { "namexxx" : "OrderQty",          "id" : "38" }, \
                            { "name" : "Side",              "id" : "54" }, \
                            { "name" : "ClOrdID",           "id" : "11" } \
                    ], \
                    "expected_actual_fields" : \
                    [ \
                            { "name" : "Status",            "id" : "150" }, \
                            { "name" : "Text",              "id" : "58" } \
                    ] \
                    } \
            }'

        file = open(HTMLReportParserTests.TEST_FILE_BAD_NAME_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_ok = '{ "report": \
                    { \
                    "description_fields" : \
                    [ \
                            { "name" : "OrderQty",          "idxxx" : "38" }, \
                            { "name" : "Side",              "id" : "54" }, \
                            { "name" : "ClOrdID",           "id" : "11" } \
                    ], \
                    "expected_actual_fields" : \
                    [ \
                            { "name" : "Status",            "id" : "150" }, \
                            { "name" : "Text",              "id" : "58" } \
                    ] \
                    } \
            }'

        file = open(HTMLReportParserTests.TEST_FILE_BAD_ID_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_ok = '{ "report": \
                    { \
                    "description_fields" : \
                    [ \
                            { "name" : "OrderQty",          "id" : "38" }, \
                            { "name" : "Side",              "id" : "54" }, \
                            { "name" : "ClOrdID",           "id" : "11" } \
                    ], \
                    "expected_actual_fieldsxxx" : \
                    [ \
                            { "name" : "Status",            "id" : "150" }, \
                            { "name" : "Text",              "id" : "58" } \
                    ] \
                    } \
            }'

        file = open(HTMLReportParserTests.TEST_FILE_BAD_EXPECTED_ACTUAL_FIELDS_PATH, 'w')
        file.write(json_data_ok)
        file.close()

    def tearDown(self):
        os.remove(HTMLReportParserTests.TEST_FILE_OK_PATH)
        os.remove(HTMLReportParserTests.TEST_FILE_BAD_TAG_REPORT_PATH)
        os.remove(HTMLReportParserTests.TEST_FILE_BAD_DESCRIPTION_FIELDS_PATH)
        os.remove(HTMLReportParserTests.TEST_FILE_BAD_NAME_PATH)
        os.remove(HTMLReportParserTests.TEST_FILE_BAD_ID_PATH)
        os.remove(HTMLReportParserTests.TEST_FILE_BAD_EXPECTED_ACTUAL_FIELDS_PATH)

    def _assert_function_result(self, function, expected_text, param=None):
        with captured_output() as (out, _):
            result = False
            if param is None:
                result = function()
            else:
                result = function(param)
            output = out.getvalue().strip()
        if (len(expected_text) > 0):
            if output.find(expected_text) == -1:
                print('expected: %s' % expected_text.replace('\n', '\\n'))
                print('actual: %s' % output.replace('\n', '\\n'))
            self.assertNotEqual(output.find(expected_text), -1)
        else:
            self.assertEqual(output, expected_text)
        return result

    def test_ok(self):
        parser = HTMLReportParser(HTMLReportParserTests.TEST_FILE_OK_PATH)
        result = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(result, None)

        # verify columns
        self.assertEqual(7, len(result.getColumns()))
        self.assertEqual(3, len(result.getDescriptionColumns()))
        self.assertEqual(2, len(result.getExpectedColumns()))
        self.assertEqual(2, len(result.getActualColumns()))
        # verify items
        self.assertEqual(3, len(result.getDescriptionItems()))
        self.assertEqual(2, len(result.getExpectedItems()))
        self.assertEqual(2, len(result.getActualItems()))

        # fully verify description items
        des = result.getDescriptionItems()
        self.assertEqual('OrderQty', des[0].getName())
        self.assertEqual('38', des[0].getID())
        self.assertEqual('Side', des[1].getName())
        self.assertEqual('54', des[1].getID())
        self.assertEqual('ClOrdID', des[2].getName())
        self.assertEqual('11', des[2].getID())

        # fully verify expected items
        des = result.getExpectedItems()
        self.assertEqual('Status', des[0].getName())
        self.assertEqual('150', des[0].getID())
        self.assertEqual('Text', des[1].getName())
        self.assertEqual('58', des[1].getID())

        # fully verify actual items
        des = result.getActualItems()
        self.assertEqual('Status', des[0].getName())
        self.assertEqual('150', des[0].getID())
        self.assertEqual('Text', des[1].getName())
        self.assertEqual('58', des[1].getID())

    def test_bad_tag_report(self):
        parser = HTMLReportParser(HTMLReportParserTests.TEST_FILE_BAD_TAG_REPORT_PATH)
        result = self._assert_function_result(parser.parse, "Required tag report not found")
        self.assertEqual(result, None)

    def test_bad_tag_description_fields(self):
        parser = HTMLReportParser(HTMLReportParserTests.TEST_FILE_BAD_DESCRIPTION_FIELDS_PATH)
        result = self._assert_function_result(parser.parse, "Required tag description_fields not found")
        self.assertEqual(result, None)

    def test_bad_tag_name(self):
        parser = HTMLReportParser(HTMLReportParserTests.TEST_FILE_BAD_NAME_PATH)
        result = self._assert_function_result(parser.parse, "Required tag name not found")
        self.assertEqual(result, None)

    def test_bad_tag_id(self):
        parser = HTMLReportParser(HTMLReportParserTests.TEST_FILE_BAD_ID_PATH)
        result = self._assert_function_result(parser.parse, "Required tag id not found")
        self.assertEqual(result, None)

    def test_bad_tag_expected_actual_fields(self):
        parser = HTMLReportParser(HTMLReportParserTests.TEST_FILE_BAD_EXPECTED_ACTUAL_FIELDS_PATH)
        result = self._assert_function_result(parser.parse, "Required tag expected_actual_fields not found")
        self.assertEqual(result, None)

    def test_file_does_not_exist(self):
        parser = HTMLReportParser('./this_file_does_not_exist.txt')
        result = self._assert_function_result(parser.parse, "ERROR parsing report definition. File [./this_file_does_not_exist.txt] does not exist.")
        self.assertEqual(result, None)

if __name__ == "__main__":
    unittest.main()