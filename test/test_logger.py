#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import Logger, LoggerEntry, LoggerEntryBold, LoggerEntryType, StdOutLogger, FileLogger, FileStdOutLogger, HtmlLogger
import unittest
from contextlib import contextmanager
import sys, os
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class LoggerTests(unittest.TestCase):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    FILE_PATH_BASIC_TEST = './test_file_ok.log'
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_basic_entry(self):
        logger = Logger()
        logger.add("THIS IS A TEST")
        self.assertEqual(len(logger.getEntries()), 1)
        entry = logger.getEntries()[0]
        self.assertEqual(entry._type, LoggerEntryType.BASIC)
        self.assertEqual(len(entry.getContent()), 1)
        self.assertEqual(entry.getContent()[0], "THIS IS A TEST")

    def test_new_lines(self):
        logger = Logger()
        logger.add("LINE1\nLINE2\nLINE3")
        self.assertEqual(len(logger.getEntries()), 1)
        entry = logger.getEntries()[0]
        self.assertEqual(entry._type, LoggerEntryType.BASIC)
        self.assertEqual(len(entry.getContent()), 3)
        self.assertEqual(entry.getContent()[0], "LINE1")
        self.assertEqual(entry.getContent()[1], "LINE2")
        self.assertEqual(entry.getContent()[2], "LINE3")

    def test_special_chars(self):
        logger = Logger()
        logger.add("%sTHIS %sIS %sA %sTEST%s FOR %sSPECIAL %sCHARS%s\nLINE2" % \
                                                (LoggerTests.HEADER, \
                                                LoggerTests.OKBLUE, \
                                                LoggerTests.OKGREEN, \
                                                LoggerTests.WARNING,\
                                                LoggerTests.FAIL,\
                                                LoggerTests.ENDC, \
                                                LoggerTests.BOLD, \
                                                LoggerTests.UNDERLINE))
        self.assertEqual(len(logger.getEntries()), 1)
        entry = logger.getEntries()[0]
        self.assertEqual(entry._type, LoggerEntryType.BASIC)
        self.assertEqual(len(entry.getContent()), 2)
        self.assertEqual(entry.getContent()[0], "THIS IS A TEST FOR SPECIAL CHARS")
        self.assertEqual(entry.getContent()[1], "LINE2")

    def test_basic_entry_bold(self):
        logger = Logger()
        logger.add("THIS IS A TEST", style=LoggerEntryType.BOLD)
        self.assertEqual(len(logger.getEntries()), 1)
        entry = logger.getEntries()[0]
        self.assertEqual(entry._type, LoggerEntryType.BOLD)
        self.assertEqual(len(entry.getContent()), 1)
        self.assertEqual(entry.getContent()[0], "THIS IS A TEST")

    def test_basic_stdout_logger(self):
        with captured_output() as (out, _):
            logger = StdOutLogger()
            logger.add("THIS IS A TEST\nLINE2")
            output = out.getvalue().strip()
        self.assertEqual(output, 'THIS IS A TEST\nLINE2')

    def test_special_chars_stdout_logger(self):
        with captured_output() as (out, _):
            logger = StdOutLogger()
            logger.add("%sTHIS IS A BLUE LINE%s" % (LoggerTests.OKBLUE, LoggerTests.ENDC))
            output = out.getvalue().strip()
        self.assertEqual(output, '\x1b[94mTHIS IS A BLUE LINE\x1b[0m')

    def test_basic_file_logger(self):
        logger = FileLogger(LoggerTests.FILE_PATH_BASIC_TEST)
        logger.add("%sTHIS IS A TEST%s\nWITH\nMORE THAN 1 LINE" % (LoggerTests.OKBLUE, LoggerTests.ENDC))
        
        file_test = open(LoggerTests.FILE_PATH_BASIC_TEST, "r")
        lines = file_test.readlines()
        self.assertEqual(len(lines), 3)
        self.assertEqual(lines[0], "THIS IS A TEST\n")
        self.assertEqual(lines[1], "WITH\n")
        self.assertEqual(lines[2], "MORE THAN 1 LINE\n")

        os.remove(LoggerTests.FILE_PATH_BASIC_TEST)

    def test_basic_file_stdout_logger(self):
        with captured_output() as (out, _):
            logger = FileStdOutLogger(LoggerTests.FILE_PATH_BASIC_TEST)
            logger.add("%sTHIS IS A TEST%s\nWITH\nMORE THAN 1 LINE" % (LoggerTests.OKBLUE, LoggerTests.ENDC))
            output = out.getvalue().strip()
        self.assertEqual(output, "%sTHIS IS A TEST%s\nWITH\nMORE THAN 1 LINE" % (LoggerTests.OKBLUE, LoggerTests.ENDC))
        
        file_test = open(LoggerTests.FILE_PATH_BASIC_TEST, "r")
        lines = file_test.readlines()
        self.assertEqual(len(lines), 3)
        self.assertEqual(lines[0], "THIS IS A TEST\n")
        self.assertEqual(lines[1], "WITH\n")
        self.assertEqual(lines[2], "MORE THAN 1 LINE\n")

        os.remove(LoggerTests.FILE_PATH_BASIC_TEST)

class HtmlLoggerTests (unittest.TestCase):
    FILE_PATH_BASIC_TEST = './test_file.log'
    FILE_PATH_BASIC_TEST_HTML = './test_file_html.log'

    def setUp(self):
        pass

    def tearDown(self):
        if (os.path.exists(HtmlLoggerTests.FILE_PATH_BASIC_TEST_HTML)):
            os.remove(HtmlLoggerTests.FILE_PATH_BASIC_TEST_HTML)
        if (os.path.exists(HtmlLoggerTests.FILE_PATH_BASIC_TEST)):
            os.remove(HtmlLoggerTests.FILE_PATH_BASIC_TEST)

    def test_bad_definition(self):
        with self.assertRaises(SystemExit) as catched_exception:
            _ = HtmlLogger(HtmlLoggerTests.FILE_PATH_BASIC_TEST, HtmlLoggerTests.FILE_PATH_BASIC_TEST_HTML, "./this_does_not_exist.txt")
        self.assertEqual(str(catched_exception.exception) , '2')
if __name__ == "__main__":
    unittest.main()

