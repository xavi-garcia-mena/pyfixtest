#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import unittest
from test_fixsessionbase import FixSessionBaseTests
from test_fixsessionserver import FixSessionServerTests
from test_fixsessionserver import FixServerTests
from test_fixsessionclient import FixSessionClientTests, FixClientTests, FixClientCommandsTests
from test_testcase import TestCaseTests, TestCaseUtilsTests
from test_msgutils import MsgUtilsTests, BufferFixParserTests
from test_messageextended import FixMessageExtendedTests, RepeatingGroupTests
from test_stopablethread import StopableThreadTests
from test_jsonmsgparser import JsonMsgParserTests
from test_scriptutils import ScriptUtilsTests
from test_logger import LoggerTests
from test_html_report_parser import HTMLReportParserTests
from test_binary_session import BinaryServerTests, BinaryServerTests

if __name__ == "__main__":
    unittest.main(verbosity=2)
