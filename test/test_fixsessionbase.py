#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################

from pyfixtest import FixSessionBase, FixSessionHandler
import unittest
import simplefix
from contextlib import contextmanager
import sys
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO

SENDER_COMP_ID=b"Sender"
TARGET_COMP_ID=b"Target"
FIX_VERSION_STR=b"FIX.4.2"

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class TestSocket(object):
    def __init__(self):
        self._buffer = None
        self._status = "OPEN"

    def send(self, buff):
        self._buffer = buff

    def get_buffer(self):
        return self._buffer

    def close(self):
        self._status = "CLOSED"

class FixSessionTest(FixSessionBase):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num):
        FixSessionBase.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num)

class NoFixSessionBase(object):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket):
        pass

class FixSessionBaseTests(unittest.TestCase):
    def setUp(self):
        global SENDER_COMP_ID
        global TARGET_COMP_ID
        global FIX_VERSION_STR
        self._socket = TestSocket()
        self._session = FixSessionBase(FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID, self._socket)

    def tearDown(self):
        pass

    def test_get_msg_header_message(self):
        msg = self._session.get_msg_header_message()
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        msg_str = self._session.get_msg_str(msg)
        self.assertEqual(msg_str.decode(), '8=FIX.4.2|9=25|35=A|49=Sender|56=Target|10=000|')

    def test_get_msg_header_message_reverse(self):
        msg = self._session.get_msg_header_reverse_credentials_message()
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        msg_str = self._session.get_msg_str(msg)
        self.assertEqual(msg_str.decode(), '8=FIX.4.2|9=25|35=A|49=Target|56=Sender|10=000|')

    def test_get_msg_header_message_highlight_tag(self):
        msg = self._session.get_msg_header_message()
        self._session.set_highlighted_tags([49])
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        msg_str = self._session.get_message_highlighted_string(msg)
        self.assertEqual(msg_str.decode(), '8=FIX.4.2|9=25|35=A|\x1b[92m49=Sender\x1b[0m|56=Target|10=000|')

    def _get_msg_from_socket(self):
        parser = simplefix.FixParser()
        parser.append_buffer(self._socket.get_buffer())
        msg_sent = parser.get_message()
        return msg_sent

    def test_send_message(self):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        msg = self._session.get_msg_header_message()
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        self._socket._buffer = None
        self._session.send_message(msg)

        # get the msg from what the socket sent
        msg_sent = self._get_msg_from_socket()
        self.assertEqual(msg.encode(), msg_sent.encode())

        self.assertEqual(msg_sent.get(simplefix.TAG_MSGSEQNUM), b'1')
        self.assertEqual(msg_sent.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_LOGON)
        self.assertTrue(len(msg_sent.get(simplefix.TAG_SENDING_TIME)) > 0)

        msg = self._session.get_msg_header_message()
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        self._session.send_message(msg)
        msg_sent = self._get_msg_from_socket()
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGSEQNUM), b'2')

    def test_close(self):
        self._socket._buffer = "test"
        self._session.close()
        self.assertEqual(self._socket._status, "CLOSED")

    def test_get_logon_message(self):
        msg = self._session.get_logon_message()
        msg_str = self._session.get_msg_str(msg)
        self.assertEqual(msg_str, b'8=FIX.4.2|9=30|35=A|49=Sender|56=Target|98=0|10=219|')

    def test_get_heartbeat_message(self):
        msg = self._session.get_heartbeat_message()
        msg_str = self._session.get_msg_str(msg)
        self.assertEqual(msg_str, b'8=FIX.4.2|9=25|35=0|49=Sender|56=Target|10=239|')

    def test_get_logout_message(self):
        msg = self._session.get_logout_message("LOGOUT TEST")
        msg_str = self._session.get_msg_str(msg)
        self.assertEqual(msg_str, b'8=FIX.4.2|9=40|35=5|49=Sender|56=Target|58=LOGOUT TEST|10=214|')

    def test_on_heartbeat(self):
        self._session._outgoing_seq_num = 1999
        self._socket._buffer = None
        self._session.on_heartbeat()
        msg_sent = self._get_msg_from_socket()
        self.assertNotEqual(msg_sent, None)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGSEQNUM), b'1999')
        self.assertEqual(msg_sent.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_HEARTBEAT)
        self.assertTrue(len(msg_sent.get(simplefix.TAG_SENDING_TIME)) > 0)

    def test_on_test_request(self):
        self._session._outgoing_seq_num = 1999
        self._socket._buffer = None
        msg = simplefix.FixMessage()
        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_TEST_REQUEST)
        msg.append_pair(simplefix.TAG_TESTREQID, b'ABCD')
        self._session.on_test_request(msg)
        msg_sent = self._get_msg_from_socket()
        self.assertNotEqual(msg_sent, None)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGSEQNUM), b'1999')
        self.assertEqual(msg_sent.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_HEARTBEAT)
        self.assertEqual(msg_sent.get(simplefix.TAG_TESTREQID), b'ABCD')
        self.assertTrue(len(msg_sent.get(simplefix.TAG_SENDING_TIME)) > 0)

    def test_on_logout(self):
        self._session._outgoing_seq_num = 1999
        self._socket._buffer = None
        self.assertEqual(self._socket._status, "OPEN")
        self._session.on_logout()
        msg_sent = self._get_msg_from_socket()
        self.assertNotEqual(msg_sent, None)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGSEQNUM), b'1999')
        self.assertEqual(msg_sent.get(simplefix.TAG_SENDER_COMPID), SENDER_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_TARGET_COMPID), TARGET_COMP_ID)
        self.assertEqual(msg_sent.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_LOGOUT)
        self.assertEqual(msg_sent.get(simplefix.TAG_TEXT), b'Logout')
        self.assertTrue(len(msg_sent.get(simplefix.TAG_SENDING_TIME)) > 0)
        self.assertEqual(self._socket._status, "CLOSED")

    def test_sequence_reset_message(self):
        resend_request = self._session.get_msg_header_message()
        resend_request.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_RESEND_REQUEST)
        resend_request.append_pair(simplefix.TAG_BEGINSEQNO, b'1')
        resend_request.append_pair(simplefix.TAG_ENDSEQNO, b'0')
        msg = self._session.get_sequence_reset_message(resend_request)
        self.assertEqual(msg.get(simplefix.TAG_MSGTYPE), simplefix.MSGTYPE_SEQUENCE_RESET)
        self.assertEqual(msg.get(simplefix.TAG_GAPFILLFLAG), b'Y')
        self.assertEqual(msg.get(simplefix.TAG_NEWSEQNO), b'1')

        self._session._outgoing_seq_num = 22
        msg = self._session.get_sequence_reset_message(resend_request)
        self.assertEqual(msg.get(simplefix.TAG_NEWSEQNO), b'22')

    def test_start_stop(self):
        fixhandler = FixSessionHandler('127.0.0.1', 1234, None, None, None, None)
        with captured_output() as (out, _):
            fixhandler.start()
            output = out.getvalue().strip()
            self.assertEqual(output, 'Plese reimplement the FixSessionHandler::start() method!')
        with captured_output() as (out, _):
            fixhandler.stop()
            output = out.getvalue().strip()
            self.assertEqual(output, 'Please reimplement the FixSessionHandler::stop() method')

    def test_session_type(self):
        fixhandler = FixSessionHandler('127.0.0.1', 1234, None, None, None, None)
        session = fixhandler._create_new_fix_session(None)
        self.assertTrue(isinstance(session, FixSessionBase))

        fixhandler.set_fix_session_class(FixSessionTest)
        session = fixhandler._create_new_fix_session(None)
        self.assertTrue(isinstance(session, FixSessionTest))
        self.assertTrue(isinstance(session, FixSessionBase))
        

if __name__ == "__main__":
    unittest.main()
