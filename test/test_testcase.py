#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
import unittest
import simplefix
from random import randint
from pyfixtest import TestCase, TestSuite, TestCaseServerEcho, TestCaseUtils, HtmlLogger
import sys
from contextlib import contextmanager
from pyfixtest import FixServer, FixSessionServer, FixSessionBase
import threading
import socket
import time
import select
import helper_serverrunner
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO
import os
import re

SENDER_COMP_ID="Sender"
TARGET_COMP_ID="Target"
FIX_VERSION_STR="FIX.4.2"
PORTS_BEGIN = 49152
PORTS_END = 65535

class FixTestServer(FixServer):
    def __init__(self, ip, port, fix_version_string, sender_comp_id, target_comp_id):
        super(FixTestServer, self).__init__(ip, port, fix_version_string, sender_comp_id, target_comp_id)
        self.set_fix_session_class(FixSessionServerBadEncoding)

class FixSessionServerBadEncoding(FixSessionServer):
    def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1):
        super(FixSessionServerBadEncoding, self).__init__(fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num)
        self._test_bad_echo = False

    def send_message(self, msg):
        self._socket.send(b"BAD ENCODING")

    def set_send_bad_echo(self, send_bad_echo):
        self._test_bad_echo = send_bad_echo

    def on_message(self, raw_message):
        if len(raw_message) > 0:
            print('on_message bad server')
            if not self._test_bad_echo:
                super(FixSessionServerBadEncoding, self).on_message(raw_message)
            else:
                print('TEST SOCKET SENT: BAD_ECHO')
                self._test_socket.sendto(b'BAD_ECHO', ('127.0.0.1', self._test_port))
        return True


@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class TestCaseUtilsTests(unittest.TestCase):
    def setUp(self):
        self._utils = TestCaseUtils()

    def tearDown(self):
        pass

    def test_get_expected_tag_non_existing_value(self):
        self.assertEqual(self._utils.get_expected_tag_non_existing_value(), 'pyfixtest$$NONE')

    def test_get_expected_tag_non_empty(self):
        self.assertEqual(self._utils.get_expected_tag_non_empty(), 'pyfixtest$$NOT_EMPTY')

    def test_parse_fix_message_from_raw(self):
        raw_msg = b'8=FIX.4.2|9=55|35=A|49=Target|56=Sender|34=2|52=20181008-14:20:51.740|10=170|'
        msg = self._utils.parse_fix_message_from_raw(raw_msg, True)
        self.assertTrue(msg != None)
        self.assertEqual(msg.encode(), raw_msg.replace(b'|', b'\x01')) 

    def test_parse_fix_message_from_raw_remove_tags_34_and_52(self):
        raw_msg = b'8=FIX.4.2|9=55|35=A|49=Target|56=Sender|34=2|52=20181008-14:20:51.740|10=170|'
        msg = self._utils.parse_fix_message_from_raw(raw_msg, True, True)
        self.assertTrue(msg != None)
        self.assertEqual(msg.encode(), b'8=FIX.4.2|9=25|35=A|49=Target|56=Sender|10=000|'.replace(b'|', simplefix.SOH_STR)) 

    def test_parse_fix_message_from_raw_bad_message(self):
        raw_msg = b'This is not a fix message'
        msg = self._utils.parse_fix_message_from_raw(raw_msg, True, True)
        self.assertTrue(msg is None)

class TestCaseTests(helper_serverrunner.HelperServerRunner):
    HTML_LOGGER_HTML_FILE = './html_logger.html'
    HTML_LOGGER_LOG_FILE = './html_logger.log'
    HTML_LOGGER_DEFINITION_FILE = './html_logger_definition.json'
    def setUp(self):
        with captured_output() as (out, _):
            out.getvalue().strip()
        json_data_ok = '{ "report": \
                            { \
                            "description_fields" : \
                            [ \
                            ], \
                            "expected_actual_fields" : \
                            [ \
                            ] \
                            } \
                        }'

        file = open(TestCaseTests.HTML_LOGGER_DEFINITION_FILE, 'w')
        file.write(json_data_ok)
        file.close()

    def tearDown(self):
        #if os.path.exists(TestCaseTests.HTML_LOGGER_HTML_FILE):
        #    os.remove(TestCaseTests.HTML_LOGGER_HTML_FILE)
        #if os.path.exists(TestCaseTests.HTML_LOGGER_LOG_FILE):
        #    os.remove(TestCaseTests.HTML_LOGGER_LOG_FILE)
        #if os.path.exists(TestCaseTests.HTML_LOGGER_DEFINITION_FILE):
        #    os.remove(TestCaseTests.HTML_LOGGER_DEFINITION_FILE)
        pass

    def _compare_files_excluding_time_stamps_msg_len_and_checksum(self, file1, file2):
        self.assertTrue(os.path.exists(file1))
        self.assertTrue(os.path.exists(file2))
        lines1 = open(file1, 'r').readlines()
        lines2 = open(file2, 'r').readlines()
        self.assertEqual(len(lines1), len(lines2))
        for i in range(len(lines1)):
            # remove timestamp
            line1_no_time_stamp = re.sub("\d+-\d+:\d+:\d+.\d+", "***TIMESTAMP***", lines1[i])
            line1_no_time_stamp_and_length = re.sub("9=\d+", "***MSGLEN***", line1_no_time_stamp)
            line1_no_time_stamp_and_length_not_checksum = re.sub("10=\d+", "***CKSUM***", line1_no_time_stamp_and_length)
            line2_no_time_stamp = re.sub("\d+-\d+:\d+:\d+.\d+", "***TIMESTAMP***", lines2[i])
            line2_no_time_stamp_and_length = re.sub("9=\d+", "***MSGLEN***", line2_no_time_stamp)
            line2_no_time_stamp_and_length_not_checksum = re.sub("10=\d+", "***CKSUM***", line2_no_time_stamp_and_length)
            self.assertEqual(line1_no_time_stamp_and_length_not_checksum, line2_no_time_stamp_and_length_not_checksum)

    def _get_client_session(self, socket):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        return FixSessionBase(FIX_VERSION_STR, TARGET_COMP_ID, SENDER_COMP_ID, socket)

    def _get_server(self, port, create_test_connection=False):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        server =  FixServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)
        if create_test_connection:
            server.set_create_test_connection(True)
        return server


    def _get_server_bad_encoding(self, port, create_test_connection=False):
        global FIX_VERSION_STR
        global SENDER_COMP_ID
        global TARGET_COMP_ID

        server = FixTestServer('127.0.0.1', port, FIX_VERSION_STR, SENDER_COMP_ID, TARGET_COMP_ID)
        if create_test_connection:
            server.set_create_test_connection(True)
        return server

    def _expectEqual(self, expected, actual):
        if expected != actual:
            print( 'Value mismatches: \'%s\' != \'%s\'' % (actual.replace('\n', '\\n'), expected.replace('\n', '\\n')))

    def _assert_function_result(self, function, test_case, expected, actual, expected_result, expected_text):
        with captured_output() as (out, _):
            if expected is None and actual is None:
                result = function()
            else:
                result = function(expected, actual)
            output = out.getvalue().strip()
        self.assertEqual(result, expected_result)
        if (len(expected_text) > 0):
            if output.find(expected_text) == -1:
                print ('expected: %s' % expected_text.replace('\n', '\\n'))
                print ('actual: %s' % output.replace('\n', '\\n'))
            self.assertNotEqual(output.find(expected_text), -1)
        else:
            self.assertEqual(output, expected_text)

    def _assert_suite_result(self, test_suite, excluded, expected_ok, expected_failed, expected_excluded, expected_result):
        with captured_output() as (out, _):
            result = test_suite.run(excluded)
            output = out.getvalue().strip()
        print (output)
        self.assertEqual(result, expected_result)
        expected_text = test_suite.get_footer(expected_ok, expected_failed, expected_excluded, len(test_suite._test_cases))
        if not output.endswith(expected_text):
            print ('expected: %s' % expected_text.replace('\n', '\\n'))
            print ('actual: %s' % output.replace('\n', '\\n'))
        self.assertTrue(output.endswith(expected_text))

    def test_check_tag_equal(self):
        test_case = TestCase('test-equal', None, None, False, None)
        expected = ('22', 'this is the expected value')

        actual_bad = ('22', 'this is the non expected value')
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_bad, False,
                'TEST --test-equal-- *** ERROR *** value mismatch for tag 22: expected value [this is the expected value], actual is [this is the non expected value]')

        actual_none = ('22', None)
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_none, False,
                'TEST --test-equal-- *** ERROR *** value mismatch for tag 22: expected value [this is the expected value], actual is [None]')

        actual_int = ('22', 1234)
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_int, False,
                'TEST --test-equal-- *** ERROR *** value mismatch for tag 22: expected value [this is the expected value], actual is [1234]')

        actual_ok = ('22', 'this is the expected value')
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_ok, True,
                '')

    def test_check_tag_none(self):
        test_case = TestCase('test-none', None, None, False, None)
        utils = TestCaseUtils()
        expected = ('22', utils.get_expected_tag_non_existing_value())

        actual_bad = ('22', 'this is the non expected value')
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_bad, False,
                'TEST --test-none-- *** ERROR *** value mismatch for tag 22: expected non existing tag has value [this is the non expected value]')

        actual_ok = ('22', None)
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_ok, True,
                '')

    def test_check_tag_not_empty(self):
        test_case = TestCase('test-not-empty', None, None, False, None)
        utils = TestCaseUtils()
        expected = ('22', utils.get_expected_tag_non_empty())

        actual_bad = ('22', '')
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_bad, False,
                'TEST --test-not-empty-- *** ERROR *** value mismatch for tag 22: expected non empty value')

        actual_none = ('22', None)
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_none, False,
                'TEST --test-not-empty-- *** ERROR *** value mismatch for tag 22: expected non empty value')

        actual_ok = ('22', 'this tag is not empty')
        self._assert_function_result(test_case._check_tag, test_case, expected, actual_ok, True,
                '')

    def test_check_messages(self):
        test_case = TestCase('test-messages', None, None, False, None)
        expected = simplefix.FixMessage()
        expected.append_pair('1', '1')
        expected.append_pair('2', test_case.get_expected_tag_non_empty())
        expected.append_pair('3', test_case.get_expected_tag_non_existing_value())
        expected.append_pair('4', '4')
        expected.append_pair('5', test_case.get_expected_tag_non_empty())

        empty_tag = simplefix.FixMessage()
        empty_tag.append_pair('1', '1')
        empty_tag.append_pair('2', '')
        empty_tag.append_pair('3', 'not-none')
        empty_tag.append_pair('4', '4')
        empty_tag.append_pair('5', None)

        self._assert_function_result(test_case._check_messages,test_case, expected, empty_tag, False,
                'TEST --test-messages-- *** ERROR *** value mismatch for tag 2: expected non empty value\nTEST --test-messages-- *** ERROR *** value mismatch for tag 3: expected non existing tag has value [not-none]\nTEST --test-messages-- *** ERROR *** value mismatch for tag 5: expected non empty value')
        
        expected = simplefix.FixMessage()
        expected.append_pair('1', '1')
        expected.append_pair('2', test_case.get_expected_tag_non_empty())
        expected.append_pair('3', test_case.get_expected_tag_non_existing_value())
        expected.append_pair('4', '4')
        expected.append_pair('5', test_case.get_expected_tag_non_empty())

    def test_logon_test_case(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        logon_reply = server._session.get_logon_message()
        test_case = TestCase('test-logon', logon, [logon_reply], True, client_session) 
        self.assertTrue(test_case.run())

    def test_logon_test_case_with_test_reply(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port, True))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon.append_pair(simplefix.TAG_ENCRYPTMETHOD, '0')

        test_case = TestCaseServerEcho('test-logon-test-reply', logon, [logon], port, client_session) 
        self.assertTrue(test_case.run())

        ready_to_read, _, _ = select.select([client_session.get_socket()], [], [], 4)
        self.assertEqual(len(ready_to_read), 0)

    def test_logon_test_case_with_test_reply_not_correct(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port, True))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon.append_pair(simplefix.TAG_ENCRYPTMETHOD, '0')

        reply = server._session.get_logon_message()
        test_case = TestCaseServerEcho('test-logon-test-reply-not-correct', logon, [reply], port, client_session) 

        self._assert_function_result(test_case.run, test_case, None, None, False,
                'TEST --test-logon-test-reply-not-correct-- *** ERROR *** value mismatch for tag 56: expected value [Target], actual is [Sender]')

    def test_logon_test_case_with_test_reply_server_does_not_echo(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port, False))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon.append_pair(simplefix.TAG_ENCRYPTMETHOD, '0')

        test_case = TestCaseServerEcho('test-logon-test-reply-not-correct-server_does_not_echo', logon, [logon], port, client_session) 
        self._assert_function_result(test_case.run, test_case, None, None, False,
                'was not received')

    def test_logon_test_case_with_test_reply_bad_echo(self):
        port = self._get_port()
        server = self._start_server(self._get_server_bad_encoding(port, True))

        client = self._connect_client(port)
        self._wait_client_connected(server)
        
        server._session.set_send_bad_echo(True)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon.append_pair(simplefix.TAG_ENCRYPTMETHOD, '0')

        test_case = TestCaseServerEcho('test-logon-test-reply-not-correct-bad-echo', logon, [logon], port, client_session) 
        self._assert_function_result(test_case.run, test_case, None, None, False,
                'was not received')

    def test_bad_tag_in_logon(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        logon_reply = server._session.get_logon_message()
        # change a tag
        logon_reply.remove(simplefix.TAG_SENDER_COMPID)
        logon_reply.append_pair(simplefix.TAG_SENDER_COMPID, 'BAD SENDER COMP ID')
        test_case = TestCase('test-logon', logon, [logon_reply], True, client_session) 
        self._assert_function_result(test_case.run, test_case, None, None, False,
                'TEST --test-logon-- *** ERROR *** value mismatch for tag 49: expected value [BAD SENDER COMP ID], actual is [Sender]')

    def test_bad_incoming_message(self):
        port = self._get_port()
        server = self._start_server(self._get_server_bad_encoding(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        logon_reply = server._session.get_logon_message()
        # change a tag
        test_case = TestCase('test-logon', logon, [logon_reply], True, client_session) 
        self._assert_function_result(test_case.run, test_case, None, None, False,
                'was not received')

    def test_reply_not_received(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        not_handled_msg = client_session.get_msg_header_message()
        not_handled_msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)

        logon_reply = server._session.get_logon_message()
        test_case = TestCase('test-not-handled-msg', not_handled_msg, [logon_reply], True, client_session) 
        self._assert_function_result(test_case.run, test_case,  None, None, False,
                'was not received')

    def test_second_expected_reply_not_ok(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        new_order = client_session.get_msg_header_message()
        new_order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        new_order.append_pair(simplefix.TAG_ORDERQTY, '100')
        new_order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL') 
        new_order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY) 
        new_order.append_pair(simplefix.TAG_CLORDID, 'test-order') 
        new_order.append_pair(simplefix.TAG_PRICE, '1234')

        server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_CLORDID, 'test-order')

        ack = server._session.get_ack_message(new_order)
        fill = server._session.get_full_fill_message(new_order)

        fill.remove(simplefix.TAG_SYMBOL)
        fill.append_pair(simplefix.TAG_SYMBOL, 'BAD-SYMBOL')

        test_case = TestCase('test-bad-reply', new_order, [ack, fill], True, client_session)
        self._assert_function_result(test_case.run, test_case,  None, None, False,
                '*** ERROR *** value mismatch for tag 55: expected value [BAD-SYMBOL], actual is [SYMBOL]')

    def test_second_expected_reply_echo(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port, True))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        new_order = client_session.get_msg_header_message()
        new_order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        new_order.append_pair(simplefix.TAG_ORDERQTY, '100')
        new_order.append_pair(simplefix.TAG_SYMBOL, 'SYMBOL') 
        new_order.append_pair(simplefix.TAG_SIDE, simplefix.SIDE_BUY) 
        new_order.append_pair(simplefix.TAG_CLORDID, 'test-order') 
        new_order.append_pair(simplefix.TAG_PRICE, '1234')

        server._session.set_expected_tag_in_new_orders_for_fills(simplefix.TAG_CLORDID, 'test-order')

        test_case = TestCaseServerEcho('test-second-expected-reply-echo', new_order, [new_order], port, client_session)
        self.assertTrue(test_case.run())
        # verify that there is no remaning data in the tcp socket
        ready_to_read, _, _ = select.select([client_session.get_socket()], [], [], 1)
        self.assertEqual(len(ready_to_read), 0)

    def test_test_suite(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        not_handled_msg = client_session.get_msg_header_message()
        not_handled_msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)

        logon_reply = server._session.get_logon_message()
        test_case = TestCase('test-not-handled-msg', not_handled_msg, [logon_reply], True, client_session) 
        test_case_2 = TestCase('test-excluded', not_handled_msg, [logon_reply], True, client_session) 

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        logon_reply = server._session.get_logon_message()
        test_case_3 = TestCase('test-logon', logon, [logon_reply], True, client_session) 

        suite = TestSuite("TEST-SUITE")
        suite.add_test(test_case)
        suite.add_test(test_case_2)
        suite.add_test(test_case_3)
        suite.run(['test-excluded'])
        self._assert_suite_result(suite, ['test-excluded'], 1, 1, 1, False)

        suite = TestSuite("TEST-SUITE-OK")
        suite.add_test(test_case_3)
        self._assert_suite_result(suite, [], 1, 0, 0, True)

    def test_test_suite_html_report(self):
        if sys.version_info[0] != 2:
            pass
        else:
            port = self._get_port()
            server = self._start_server(self._get_server(port))
            client = self._connect_client(port)
            self._wait_client_connected(server)
            logger = HtmlLogger(TestCaseTests.HTML_LOGGER_LOG_FILE, TestCaseTests.HTML_LOGGER_HTML_FILE, TestCaseTests.HTML_LOGGER_DEFINITION_FILE)

            client_session = self._get_client_session(client)

            not_handled_msg = client_session.get_msg_header_message()
            not_handled_msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)

            logon_reply = server._session.get_logon_message()
            test_case = TestCase('test-not-handled-msg', not_handled_msg, [logon_reply], True, client_session, logger) 
            test_case_2 = TestCase('test-excluded', not_handled_msg, [logon_reply], True, client_session, logger) 

            logon = client_session.get_msg_header_message()
            logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

            logon_reply = server._session.get_logon_message()
            test_case_3 = TestCase('test-logon', logon, [logon_reply], True, client_session, logger) 

            suite = TestSuite("TEST-SUITE", logger)
            suite.add_test(test_case)
            suite.add_test(test_case_2)
            suite.add_test(test_case_3)
            suite.run(['test-excluded'])
            self._compare_files_excluding_time_stamps_msg_len_and_checksum(TestCaseTests.HTML_LOGGER_HTML_FILE, 'test/snapshots/%s' % TestCaseTests.HTML_LOGGER_HTML_FILE)

    def test_test_suite_server_echo(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port, True))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        not_handled_msg = client_session.get_msg_header_message()
        not_handled_msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)
        not_handled_msg_reply = client_session.get_msg_header_message()
        not_handled_msg_reply.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        logon_reply = client_session.get_msg_header_message()
        logon_reply.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        test_case = TestCaseServerEcho('echo-ok', not_handled_msg, [not_handled_msg_reply], port, client_session) 
        test_case_2 = TestCaseServerEcho('echo-bad', not_handled_msg, [logon_reply], port, client_session) 
        test_case_3 = TestCaseServerEcho('echo-bad-excluded', not_handled_msg, [logon_reply], port, client_session) 

        suite = TestSuite("TEST-SUITE")
        suite.add_test(test_case)
        suite.add_test(test_case_2)
        suite.add_test(test_case_3)
        suite.run(['echo-bad-excluded'])
        self._assert_suite_result(suite, ['echo-bad-excluded'], 1, 1, 1, False)

        suite = TestSuite("TEST-SUITE-OK")
        suite.add_test(test_case)
        suite.add_test(test_case)
        suite.run([])
        self._assert_suite_result(suite, [], 2, 0, 0, True)

    def test_test_suite_abort_on_failure(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        not_handled_msg = client_session.get_msg_header_message()
        not_handled_msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)

        logon_reply = server._session.get_logon_message()
        test_case = TestCase('test-not-handled-msg', not_handled_msg, [logon_reply], True, client_session)
        test_case.set_abort_on_failure(True)

        test_case_2 = TestCase('test-excluded', not_handled_msg, [logon_reply], True, client_session) 

        logon = client_session.get_msg_header_message()
        logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)

        logon_reply = server._session.get_logon_message()
        test_case_3 = TestCase('test-logon', logon, [logon_reply], True, client_session) 

        suite = TestSuite("TEST-SUITE")
        suite.add_test(test_case_3)
        suite.add_test(test_case)
        suite.add_test(test_case_2)
        suite.run(['test-excluded'])
        self._assert_suite_result(suite, ['test-excluded'], 1, 1, 0, False)

    def test_check_ignored_tags(self):
        test_case = TestCase('test-ignored_tags', None, None, False, None)
        expected = simplefix.FixMessage()
        expected.append_pair('1', '1')
        expected.append_pair('9', '9')
        expected.append_pair('10', '10')

        actual = simplefix.FixMessage()
        actual.append_pair('1', '1')
        actual.append_pair('9', '99')
        actual.append_pair('10', '100')

        self._assert_function_result(test_case._check_messages,test_case, expected, actual, True, '')

        test_case.set_ignore_tags( [ ] )

        self._assert_function_result(test_case._check_messages,test_case, expected, actual, False, 'TEST --test-ignored_tags-- *** ERROR *** value mismatch for tag 9: expected value [9], actual is [99]\nTEST --test-ignored_tags-- *** ERROR *** value mismatch for tag 10: expected value [10], actual is [100]')
        
        test_case.set_ignore_tags( [ simplefix.TAG_BODYLENGTH ] )
        self._assert_function_result(test_case._check_messages,test_case, expected, actual, False, 'TEST --test-ignored_tags-- *** ERROR *** value mismatch for tag 10: expected value [10], actual is [100]')

    def test_test_suite_generate_new_clordids(self):
        port = self._get_port()
        server = self._start_server(self._get_server(port))
        client = self._connect_client(port)
        self._wait_client_connected(server)

        client_session = self._get_client_session(client)

        not_handled_msg = client_session.get_msg_header_message()
        not_handled_msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_STATUS)

        order = client_session.get_msg_header_message()
        order.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        order.append_pair(simplefix.TAG_CLORDID, 'CLORDID')
        order.append_pair(simplefix.TAG_PRICE, '123')
        order.append_pair(simplefix.TAG_ORDERQTY, '100')
        order.append_pair(simplefix.TAG_ORDTYPE, '2')
        order.append_pair(simplefix.TAG_TIMEINFORCE, '3')

        ack = server._session.get_ack_message(order)

        test_case = TestCase('test-ack', order, [ack], True, client_session) 

        suite = TestSuite("TEST-SUITE-OK")
        suite.add_test(test_case)
        self._assert_suite_result(suite, [], 1, 0, 0, True)

        test_case_clordid = test_case._initial_message.get(simplefix.TAG_CLORDID)
        for msg in test_case._expected_reply:
            self.assertEqual(msg.get(simplefix.TAG_CLORDID), test_case_clordid)

        # run again and check that the clordids are equal
        # ignore tags 17 and 37 as are generated based of the seq num
        test_case.set_ignore_tags([ '17', '37' ])
        self._assert_suite_result(suite, [], 1, 0, 0, True)

        test_case_clordid_2 = test_case._initial_message.get(simplefix.TAG_CLORDID)
        self.assertEqual(test_case_clordid, test_case_clordid_2)
        for msg in test_case._expected_reply:
            self.assertEqual(msg.get(simplefix.TAG_CLORDID), test_case_clordid)

        # set to change clordids on each run
        suite.set_generate_new_clordids(True)

        self._assert_suite_result(suite, [], 1, 0, 0, True)
        test_case_clordid_2 = test_case._initial_message.get(simplefix.TAG_CLORDID)
        self.assertNotEqual(test_case_clordid, test_case_clordid_2)
        for msg in test_case._expected_reply:
            self.assertEqual(msg.get(simplefix.TAG_CLORDID), test_case_clordid_2)

if __name__ == "__main__":
    unittest.main()

