#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import JsonMsgParser, FixSessionServer
import unittest
import simplefix
import os
import sys
from contextlib import contextmanager
if sys.version_info[0] == 2:
    from StringIO import StringIO
else:
    from io import StringIO

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err
class JsonMsgParserTests(unittest.TestCase):
    TEST_FILE_OK_PATH = "./testfile_ok.json"
    TEST_FILE_OK_VALUE_FROM_PATH = "./testfile_ok_value_from.json"
    TEST_FILE_MESSAGE_TAG_BAD_PATH = "./testfile_message_tag_bad.json"
    TEST_FILE_TAGS_TAG_BAD_PATH = "./testfile_tags_tag_bad.json"
    TEST_FILE_MSGTYPE_TAG_BAD_PATH = "./testfile_msgtype_tag_bad.json"
    TEST_FILE_TAG_TAG_BAD_PATH = "./testfile_tag_tag_bad.json"
    TEST_FILE_VALUE_TAG_BAD_PATH = "./testfile_value_tag_bad.json"
    TEST_FILE_OK_UNIQUE_PATH = "./testfile_ok_unique.json"
    TEST_FILE_OK_UNIQUE_SHORT_PATH = "./testfile_ok_unique_short.json"
    TEST_FILE_OK_ENHANCE_FLAG = "./testfile_ok_enhance_flag.json"
    TEST_FILE_OK_ENHANCE_FLAG_FALSE = "./testfile_ok_enhance_flag_false.json"
    TEST_FILE_OK_LAST_CL_ORD_ID = "./testfile_ok_last_cl_ord_id.json"

    def setUp(self):
        json_data_ok = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_OK_PATH, 'w')
        file.write(json_data_ok)
        file.close()

        json_data_ok_value_from = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value_from" : "11" }, \
                                    { "tag" : "11",   "value_from" : "1111" }, \
                                    { "tag" : "21",   "value_from" : "2121" }, \
                                    { "tag" : "1234", "value_from" : "12341234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_OK_VALUE_FROM_PATH, 'w')
        file.write(json_data_ok_value_from)
        file.close()

        json_data_message_tag_bad = '{ "messagexxxxxx": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_MESSAGE_TAG_BAD_PATH, 'w')
        file.write(json_data_message_tag_bad)
        file.close()

        json_data_tags_tag_bad = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tagsxxx" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_TAGS_TAG_BAD_PATH, 'w')
        file.write(json_data_tags_tag_bad)
        file.close()

        json_data_msgtype_tag_bad = '{ "message": \
                            { \
                            "msg_typexxxx": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_MSGTYPE_TAG_BAD_PATH, 'w')
        file.write(json_data_msgtype_tag_bad)
        file.close()

        json_data_tag_tag_bad = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tagxxxxx" : "1",    "value" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_TAG_TAG_BAD_PATH, 'w')
        file.write(json_data_tag_tag_bad)
        file.close()

        json_data_value_tag_bad = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "valuexxxx" : "test_1" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_VALUE_TAG_BAD_PATH, 'w')
        file.write(json_data_value_tag_bad)
        file.close()

        self._session = FixSessionServer('FIX.4.2', 'SENDER', 'TARGET', None)

        json_data_ok_unique = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "##UNIQUE##" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_PATH, 'w')
        file.write(json_data_ok_unique)
        file.close()

        json_data_ok_unique_short = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "##UNIQUE_SHORT##" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_SHORT_PATH, 'w')
        file.write(json_data_ok_unique_short)
        file.close()

        json_data_ok_enhance_flag = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "enhance_default" : "true", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "##UNIQUE_SHORT##" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_OK_ENHANCE_FLAG, 'w')
        file.write(json_data_ok_enhance_flag)
        file.close()

        json_data_ok_enhance_flag_false = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "enhance_default" : "false", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "##UNIQUE_SHORT##" }, \
                                    { "tag" : "11",   "value" : "test_11" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'

        file = open(JsonMsgParserTests.TEST_FILE_OK_ENHANCE_FLAG_FALSE, 'w')
        file.write(json_data_ok_enhance_flag_false)
        file.close()

        file = open(JsonMsgParserTests.TEST_FILE_OK_LAST_CL_ORD_ID, 'w')
        
        json_data_ok_cl_ord_id = '{ "message": \
                            { \
                            "msg_type": "R", \
                            "tags" : \
                            [ \
                                    { "tag" : "1",    "value" : "##UNIQUE_SHORT##" }, \
                                    { "tag" : "20",    "value" : "##UNIQUE##" }, \
                                    { "tag" : "11",   "value" : "##LAST_CLORDID##" }, \
                                    { "tag" : "21",   "value" : "test_21" }, \
                                    { "tag" : "1234", "value" : "test_1234" } \
                            ] \
                            } \
                    }'
        file.write(json_data_ok_cl_ord_id)
        file.close()


    def tearDown(self):
        os.remove(JsonMsgParserTests.TEST_FILE_OK_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_OK_VALUE_FROM_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_SHORT_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_MESSAGE_TAG_BAD_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_MSGTYPE_TAG_BAD_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_TAGS_TAG_BAD_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_TAG_TAG_BAD_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_VALUE_TAG_BAD_PATH)
        os.remove(JsonMsgParserTests.TEST_FILE_OK_ENHANCE_FLAG)
        os.remove(JsonMsgParserTests.TEST_FILE_OK_ENHANCE_FLAG_FALSE)
        os.remove(JsonMsgParserTests.TEST_FILE_OK_LAST_CL_ORD_ID)


    def _assert_function_result(self, function, expected_text):
        with captured_output() as (out, _):
            result = function()
            output = out.getvalue().strip()
        if (len(expected_text) > 0):
            if output.find(expected_text) == -1:
                print('expected: %s' % expected_text.replace('\n', '\\n'))
                print('actual: %s' % output.replace('\n', '\\n'))
            self.assertNotEqual(output.find(expected_text), -1)
        else:
            self.assertEqual(output, expected_text)
        return result

    def test_ok(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_PATH, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        self.assertEqual(msg.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(msg.get(simplefix.TAG_BEGINSTRING), b'FIX.4.2')
        self.assertEqual(msg.get(simplefix.TAG_SENDER_COMPID), b'SENDER')
        self.assertEqual(msg.get(simplefix.TAG_TARGET_COMPID), b'TARGET')
        self.assertEqual(msg.get('1'), b'test_1') 
        self.assertEqual(msg.get('11'), b'test_11') 
        self.assertEqual(msg.get('21'), b'test_21') 
        self.assertEqual(msg.get('1234'), b'test_1234') 

        tags = self._assert_function_result(parser.get_tags, "")
        self.assertNotEqual(tags, None)
        self.assertEqual(tags[0][0], simplefix.TAG_MSGTYPE)
        self.assertEqual(tags[0][1], 'R')
        self.assertEqual(tags[1][0], '1')
        self.assertEqual(tags[1][1], b'test_1')
        self.assertEqual(tags[2][0], '11')
        self.assertEqual(tags[2][1], b'test_11')
        self.assertEqual(tags[3][0], '21')
        self.assertEqual(tags[3][1], b'test_21')
        self.assertEqual(tags[4][0], '1234')
        self.assertEqual(tags[4][1], b'test_1234')

    def test_messages_tag_bad(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_MESSAGE_TAG_BAD_PATH, self._session)
        if sys.version_info[0] == 2:
            msg = self._assert_function_result(parser.parse, "Required tag message not found in {u'messagexxxxxx': {u'msg_type': u'R', u'tags': [{u'tag': u'1', u'value': u'test_1'}, {u'tag': u'11', u'value': u'test_11'}, {u'tag': u'21', u'value': u'test_21'}, {u'tag': u'1234', u'value': u'test_1234'}]}}")
        else:
            msg = self._assert_function_result(parser.parse, "Required tag message not found in {'messagexxxxxx': {'msg_type': 'R', 'tags': [{'tag': '1', 'value': 'test_1'}, {'tag': '11', 'value': 'test_11'}, {'tag': '21', 'value': 'test_21'}, {'tag': '1234', 'value': 'test_1234'}]}}")
        self.assertEqual(msg, None)

        if sys.version_info[0] == 2:
            tags = self._assert_function_result(parser.parse, "Required tag message not found in {u'messagexxxxxx': {u'msg_type': u'R', u'tags': [{u'tag': u'1', u'value': u'test_1'}, {u'tag': u'11', u'value': u'test_11'}, {u'tag': u'21', u'value': u'test_21'}, {u'tag': u'1234', u'value': u'test_1234'}]}}")
        else:
            tags = self._assert_function_result(parser.parse, "Required tag message not found in {'messagexxxxxx': {'msg_type': 'R', 'tags': [{'tag': '1', 'value': 'test_1'}, {'tag': '11', 'value': 'test_11'}, {'tag': '21', 'value': 'test_21'}, {'tag': '1234', 'value': 'test_1234'}]}}")
        self.assertEqual(tags, None)

    def test_msgtype_tag_bad(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_MSGTYPE_TAG_BAD_PATH, self._session)
        if sys.version_info[0] == 2:
            msg = self._assert_function_result(parser.parse, "Required tag msg_type not found in {u'msg_typexxxx': u'R', u'tags': [{u'tag': u'1', u'value': u'test_1'}, {u'tag': u'11', u'value': u'test_11'}, {u'tag': u'21', u'value': u'test_21'}, {u'tag': u'1234', u'value': u'test_1234'}]}")
        else:
            msg = self._assert_function_result(parser.parse, "Required tag msg_type not found in {'msg_typexxxx': 'R', 'tags': [{'tag': '1', 'value': 'test_1'}, {'tag': '11', 'value': 'test_11'}, {'tag': '21', 'value': 'test_21'}, {'tag': '1234', 'value': 'test_1234'}]}")
        self.assertEqual(msg, None)

        if sys.version_info[0] == 2:
            tags = self._assert_function_result(parser.parse, "Required tag msg_type not found in {u'msg_typexxxx': u'R', u'tags': [{u'tag': u'1', u'value': u'test_1'}, {u'tag': u'11', u'value': u'test_11'}, {u'tag': u'21', u'value': u'test_21'}, {u'tag': u'1234', u'value': u'test_1234'}]}")
        else:
            tags = self._assert_function_result(parser.parse, "Required tag msg_type not found in {'msg_typexxxx': 'R', 'tags': [{'tag': '1', 'value': 'test_1'}, {'tag': '11', 'value': 'test_11'}, {'tag': '21', 'value': 'test_21'}, {'tag': '1234', 'value': 'test_1234'}]}")
        self.assertEqual(tags, None)

    def test_tags_tag_bad(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_TAGS_TAG_BAD_PATH, self._session)
        if sys.version_info[0] == 2:
            msg = self._assert_function_result(parser.parse, "Required tag tags not found in {u'tagsxxx': [{u'tag': u'1', u'value': u'test_1'}, {u'tag': u'11', u'value': u'test_11'}, {u'tag': u'21', u'value': u'test_21'}, {u'tag': u'1234', u'value': u'test_1234'}], u'msg_type': u'R'}")
        else:
            msg = self._assert_function_result(parser.parse, "Required tag tags not found in {'msg_type': 'R', 'tagsxxx': [{'tag': '1', 'value': 'test_1'}, {'tag': '11', 'value': 'test_11'}, {'tag': '21', 'value': 'test_21'}, {'tag': '1234', 'value': 'test_1234'}]}")
        self.assertEqual(msg, None)

        if sys.version_info[0] == 2:
            tags = self._assert_function_result(parser.parse, "Required tag tags not found in {u'tagsxxx': [{u'tag': u'1', u'value': u'test_1'}, {u'tag': u'11', u'value': u'test_11'}, {u'tag': u'21', u'value': u'test_21'}, {u'tag': u'1234', u'value': u'test_1234'}], u'msg_type': u'R'}")
        else:
            tags = self._assert_function_result(parser.parse, "Required tag tags not found in {'msg_type': 'R', 'tagsxxx': [{'tag': '1', 'value': 'test_1'}, {'tag': '11', 'value': 'test_11'}, {'tag': '21', 'value': 'test_21'}, {'tag': '1234', 'value': 'test_1234'}]}")
        self.assertEqual(tags, None)

    def test_tag_tag_bad(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_TAG_TAG_BAD_PATH, self._session)
        msg = self._assert_function_result(parser.parse, "Required tag tag not found in {")
        self.assertEqual(msg, None)

        tags = self._assert_function_result(parser.parse, "Required tag tag not found in {")
        self.assertEqual(tags, None)

    def test_value_tag_bad(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_VALUE_TAG_BAD_PATH, self._session)
        if sys.version_info[0] == 2:
            msg = self._assert_function_result(parser.parse, "Value for one (and only one) of the items in [['value', 'value_from']] should be found in {u'valuexxxx': u'test_1', u'tag': u'1'}")
        else:
            msg = self._assert_function_result(parser.parse, "Value for one (and only one) of the items in [['value', 'value_from']] should be found in")

        self.assertEqual(msg, None)

        tags = self._assert_function_result(parser.get_tags, "Value for one (and only one) of the items in [['value', 'value_from']] should be found in {")
        self.assertEqual(tags, None)
    
    def test_unique(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_PATH, self._session)
        msg1 = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg1, None)

        msg2 = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg2, None)
        
        self.assertTrue(msg1.get('1') != None)
        self.assertTrue(msg1.get('1') != '')

        self.assertTrue(msg2.get('1') != None)
        self.assertTrue(msg2.get('1') != '')

        self.assertNotEqual(msg1.get('1'), msg2.get('1'))

        tags1 = self._assert_function_result(parser.get_tags, "")
        self.assertNotEqual(tags1, None)

        tags2 = self._assert_function_result(parser.get_tags, "")
        self.assertNotEqual(tags2, None)

        value_tags1 = None
        for tag in tags1:
            if tag[0] == '1':
                value_tags1 = tag[1]

        value_tags2 = None
        for tag in tags2:
            if tag[0] == '1':
                value_tags2 = tag[1]

        self.assertNotEqual(value_tags1, None)
        self.assertNotEqual(value_tags1, '')
        self.assertNotEqual(value_tags2, None)
        self.assertNotEqual(value_tags2, '')
        self.assertNotEqual(value_tags2, value_tags1)

    def test_unique_short(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_SHORT_PATH, self._session)
        msg1 = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg1, None)

        msg2 = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg2, None)
        
        self.assertTrue(msg1.get('1') != None)
        self.assertTrue(msg1.get('1') != '')

        self.assertTrue(msg2.get('1') != None)
        self.assertTrue(msg2.get('1') != '')

        self.assertNotEqual(msg1.get('1'), msg2.get('1'))

        tags1 = self._assert_function_result(parser.get_tags, "")
        self.assertNotEqual(tags1, None)

        tags2 = self._assert_function_result(parser.get_tags, "")
        self.assertNotEqual(tags2, None)

        value_tags1 = None
        for tag in tags1:
            if tag[0] == '1':
                value_tags1 = tag[1]

        value_tags2 = None
        for tag in tags2:
            if tag[0] == '1':
                value_tags2 = tag[1]

        self.assertNotEqual(value_tags1, None)
        self.assertNotEqual(value_tags1, '')
        self.assertNotEqual(value_tags2, None)
        self.assertNotEqual(value_tags2, '')
        self.assertNotEqual(value_tags2, value_tags1)

        parser_long = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_UNIQUE_PATH, self._session)
        msg_long = self._assert_function_result(parser_long.parse, "")
        self.assertTrue(msg_long.get('1') != None)
        self.assertTrue(msg_long.get('1') != '')
        self.assertTrue(len(msg_long.get('1'))>len(msg1.get('1')))

        tags1 = self._assert_function_result(parser_long.get_tags, "")
        self.assertNotEqual(tags1, None)

        value_long_tags1 = None
        for tag in tags1:
            if tag[0] == '1':
                value_long_tags1 = tag[1]

        self.assertNotEqual(value_long_tags1, None)
        self.assertNotEqual(value_long_tags1, '')
        self.assertTrue(len(value_long_tags1)>len(value_tags1))

    def test_ok_value_from(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_VALUE_FROM_PATH, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        self.assertEqual(msg.get(simplefix.TAG_MSGTYPE), b'R')
        self.assertEqual(msg.get(simplefix.TAG_BEGINSTRING), b'FIX.4.2')
        self.assertEqual(msg.get(simplefix.TAG_SENDER_COMPID), b'SENDER')
        self.assertEqual(msg.get(simplefix.TAG_TARGET_COMPID), b'TARGET')
        self.assertEqual(msg.get('1'), b'%s11' % parser.get_tag_from_preffix().encode()) 
        self.assertEqual(msg.get('11'), b'%s1111' % parser.get_tag_from_preffix().encode()) 
        self.assertEqual(msg.get('21'), b'%s2121' % parser.get_tag_from_preffix().encode()) 
        self.assertEqual(msg.get('1234'), b'%s12341234' % parser.get_tag_from_preffix().encode()) 

        tags = self._assert_function_result(parser.get_tags, "")
        self.assertNotEqual(tags, None)
        self.assertEqual(tags[0][0], simplefix.TAG_MSGTYPE)
        self.assertEqual(tags[0][1], 'R')
        self.assertEqual(tags[1][0], '1')
        self.assertEqual(tags[1][1], b'%s11' % parser.get_tag_from_preffix().encode())
        self.assertEqual(tags[2][0], '11')
        self.assertEqual(tags[2][1], b'%s1111' % parser.get_tag_from_preffix().encode())
        self.assertEqual(tags[3][0], '21')
        self.assertEqual(tags[3][1], b'%s2121' % parser.get_tag_from_preffix().encode())
        self.assertEqual(tags[4][0], '1234')
        self.assertEqual(tags[4][1], b'%s12341234' % parser.get_tag_from_preffix().encode())

    def test_enhance_flag(self):
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_ENHANCE_FLAG, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        self.assertTrue(parser.get_enhance_flag())

        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_ENHANCE_FLAG_FALSE, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        self.assertFalse(parser.get_enhance_flag())

    def test_last_cl_ord_id(self):
        self._session._last_cl_order_id = '123'
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_LAST_CL_ORD_ID, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        if sys.version_info[0] == 2:
            self.assertEqual('123', msg.get(simplefix.TAG_CLORDID))
        else:
            self.assertEqual(b'123', msg.get(simplefix.TAG_CLORDID))

        self._session._last_cl_order_id = None
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_LAST_CL_ORD_ID, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        self.assertEqual(None, msg.get(simplefix.TAG_CLORDID))

    def test_recalculate_dynamic_tags(self):
        self._session._last_cl_order_id = '123'
        parser = JsonMsgParser(JsonMsgParserTests.TEST_FILE_OK_LAST_CL_ORD_ID, self._session)
        msg = self._assert_function_result(parser.parse, "")
        self.assertNotEqual(msg, None)
        if sys.version_info[0] == 2:
            self.assertEqual('123', msg.get(simplefix.TAG_CLORDID))
            self.assertEqual('test_21', msg.get('21'))
            self.assertEqual('test_1234', msg.get('1234'))
        else:
            self.assertEqual(b'123', msg.get(simplefix.TAG_CLORDID))
            self.assertEqual(b'test_21', msg.get('21'))
            self.assertEqual(b'test_1234', msg.get('1234'))
        self.assertIsNotNone(msg.get('1'))
        self.assertIsNotNone(msg.get('20'))
        unique1 = msg.get('1')
        unique2 = msg.get('20')

        self._session._last_cl_order_id = '234'
        msg = parser.recalculate_dynamic_tags()
        if sys.version_info[0] == 2:
            self.assertEqual('234', msg.get(simplefix.TAG_CLORDID))
            self.assertEqual('test_21', msg.get('21'))
            self.assertEqual('test_1234', msg.get('1234'))
        else:
            self.assertEqual(b'234', msg.get(simplefix.TAG_CLORDID))
            self.assertEqual(b'test_21', msg.get('21'))
            self.assertEqual(b'test_1234', msg.get('1234'))
        self.assertIsNotNone(msg.get('1'))
        self.assertIsNotNone(msg.get('20'))
        self.assertNotEqual(unique1, msg.get('1'))
        self.assertNotEqual(unique2, msg.get('20'))
        



if __name__ == "__main__":
    unittest.main()

