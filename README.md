[![coverage report](https://gitlab.com/xavi-garcia-mena/pyfixtest/badges/master/coverage.svg)](https://gitlab.com/xavi-garcia-mena/pyfixtest/commits/master)
[![pipeline status](https://gitlab.com/xavi-garcia-mena/pyfixtest/badges/master/pipeline.svg)](https://gitlab.com/xavi-garcia-mena/pyfixtest/commits/master)

[Full coverage report](https://xavi-garcia-mena.gitlab.io/pyfixtest/coverage/)

# Introduction
## What is pyfixtest?
fixtest is a python module for testing FIX servers and clients (it also supports binary protocols with some additional conversion classes to be defined by the user).

Functionalities available in pyfixtest are:
- TCP/IP socket handling (for both server and client applications)
- FIX message parsing
- FIX message sending
- A minimal state to support Order Cancel Requests (F) and Order Cancel/Replace Requests (G)
- Acks for NewOrderSingle (D) messages
- Fills for NewOrderSingle (D) messages
- A default FIX server implementation that replies to the following messages:
    - Logon
    - Heartbeat
    - TestRequest
    - ResendRequest (only to sync sequence numbers)
    - NewOrderSingle
    - CancelOrderRequest
    - CancelOrderReplaceRequest
    - Quote
- A default FIX client implementation that maintains the FIX session (Logon, Heartbeat, TestRequest)
- A mechanism to send user defined messages in both, FIX client and server sessions using json format.
- Enhancement of default messages as per user requirements. 
- User defined initial outgoing sequence number to synchronise and reconnect to another end-point.
- A testing environment to send messages and verify if the reply fits the expectations. 
- A logger to log session messages in text and html format.
- Tag highlighting.

# What is NOT pyfixtest?
Currently pyfixtest is not holding the whole session state, which means it cannot o trading engine features and does not handle order books.
pyfixtest simply replies to messages received following a few rules.
For example, for an incoming NewOrderSingle(D) message the default server session replies with an ExecutionReport with tag 150=0 (Ack). And, when asked by the user, it replies with an ExecutionReport with tag 150=2 (Full fill). 

# Installation 
## Environments with pip available 
pyfixtest depends on [simplefix](https://github.com/da4089/simplefix)

To install simplefix, simply clone the simplefix code with:
```bash
git clone https://github.com/da4089/simplefix
```

and then install with:
```bash
pip install ./ 
```


You can now install pyfixtest with pip.
cd to the pyfixtest main folder and run:
```bash
pip install ./
```
##  Environments with no permissions or pip not available
You can also use pyfixtest without installing (in case you don't have the required permissions or pip is not available).
You can use PYTHONPATH to tell python where to find the modules.
All you need to do is:
```bash
export PYTHONPATH=PATH_TO_SIMPLE_FIX:PATH_TO_PYFIXTEST
```
For a system with simplefix located at */home/user/simplefix* and pyfixtest located at */home/user/pyfixtest* you can do:
```bash
export PYTHONPATH=/home/user/simplefix:/home/user/pyfixtest
```

**This needs to be done for every new terminal you open.**

# pyfixtest default server and client.

pyfixtest comes with a default implementation for server and client.
Server is located at scritps/pyfixtest-server.py
Client is located at scripts/pyfixtest-client.py

If you run the server without any parameters or with the --help parameter you'll get the following:
```bash
$ python scripts/pyfixtest-server.py --help

pyfixtest-server.py --ip=<IP ADDRESS> --port=<PORT> --fix=<FIX VERSION STRING> --sender_comp_id=<VALUE> --target_comp_id=<VALUE> --outgoing_seq_num=<NUMBER>

example: pyfixtest-server.py --ip=127.0.0.1 --port 6575 --fix=FIX.4.2 --sender_comp_id=SENDER --target_comp_id=TARGET --outgoing_seq_num=14

OPTIONAL FLAGS:

--tag_value_for_fills=[TAG,VALUE]

--highlight=[TAG1,TAG2,...,TAGN]

--quote_fill_this_side=1 or 2

TAGS FOR REPLIES DEFINITION:

--on-ack-reply-tags=[FILE_PATH]

--on-fill-reply-tags=[FILE_PATH]

--on-cancel-reply-tags=[FILE_PATH]

--on-replace-reply-tags=[FILE_PATH]

--on-quote-ack-reply-tags=[FILE_PATH]

--on-quote-fill-reply-tags=[FILE_PATH]
PARAMETERS FOR MULTIPLE ORDERS:
    --multiple_nb_orders=NUMBER_OF_MULTIPLE_ORDERS_TO_SEND
    --multiple_interval=INTERVAL_IN_BETWEEN_ORDERS (in seconds)
```

If you run the client with no parameter or with --help you'll get:
```bash
$ python scripts/pyfixtest-client.py --help

pyfixtest-client.py --ip=<IP ADDRESS> --port=<PORT> --fix=<FIX VERSION STRING> --sender_comp_id=<VALUE> --target_comp_id=<VALUE> --outgoing_seq_num=<NUMBER>

example: pyfixtest-client.py --ip=127.0.0.1 --port 6575 --fix=FIX.4.2 --sender_comp_id=SENDER --target_comp_id=TARGET --outgoing_seq_num=14

OPTIONAL FLAGS:

--tag_value_for_fills=[TAG,VALUE]

--highlight=[TAG1,TAG2,...,TAGN]

--quote_fill_this_side=1 or 2

TAGS FOR REPLIES DEFINITION:

--on-ack-reply-tags=[FILE_PATH]

--on-fill-reply-tags=[FILE_PATH]

--on-cancel-reply-tags=[FILE_PATH]

--on-replace-reply-tags=[FILE_PATH]

--on-quote-ack-reply-tags=[FILE_PATH]

--on-quote-fill-reply-tags=[FILE_PATH]
PARAMETERS FOR MULTIPLE ORDERS:
    --multiple_nb_orders=NUMBER_OF_MULTIPLE_ORDERS_TO_SEND
    --multiple_interval=INTERVAL_IN_BETWEEN_ORDERS (in seconds)
```

## Parameters
**--ip**: IP to listen for incoming connections for servers or IP to connect to for clients.

**--port**: Port to listen for incoming connections for servers or Port to connect to for clients.

**--fix**: Definition of FIX protocol to be used. This must be the string used in the FIX message.

*examples*

- FIX.4.2
- FIX.4.4

The string passed is the one used for tag 8 when building the FIX messages.

**--sender_comp_id**: Sender Comp ID to be used by the session.

**--target_comp_ip**: Target Comp ID to be used by the session.

**--outgoing_seq_num**: Outgoing sequence number to start with. (This is normally used to reconnect to already running endpoints in which the expected incoming sequence number is higher than 1).

**--tag_value_for_fills**: Used in server sessions. This indicates the server when we want to get a full fill for NewOrderSingle. 
For example, if we want to get a Full Fill when Price sent in NewOrderSingle is 1999 we should pass:
*--tag_value_for_fills=[44,1999]*

When receiving a NewOrderSingle message the server with reply with an Ack (ExecutionReport with 150=0) if Price is different to 1999 and it will reply with a Full Fill (ExecutionReport with 150=2) if Price send in NewOrderSingle is 1999.

**--highlight**: Highlights the tags described.
*example: --highlight=[150,11,44]*

**--quote_fills_this_side**: Added for multiside quotes. Indicates which side will be filled when receiving a new Quote.
## Enhance or replace default server replies parameters
Default replies from server can be enhanced (add or remove tags) or completely replaced.
This is done defining a json file that will describe the message.
The format for a message in json is:
```json
{ "message": 
  { 
    "msg_type": "8", 
    "enhance_default" : "true", 
    "tags" : 
    [ 
      { "tag" : "1",     "value" : "##UNIQUE_SHORT##" }, 
      { "tag" : "11",    "value" : "test_11" }, 
       { "tag" : "21",   "value" : "test_21" }, 
       { "tag" : "1234", "value_from" : "40" } 
    ] 
   } 
 }
```
All the possible options for the json messages will be described later in this document.

**--on-ack-reply-tags**: Used to re-define the default message sent by the server when sending an Ack.

**--on-fill-reply-tags**: Used to re-define the default message sent by the server when sending a Full Fill.

**--on-cancel-reply-tags**: Used to re-define the default message sent by the server when replying to a OrderCancelRequest.

**--on-replace-reply-tags**: Used to re-define the default message sent by the server when replying to a OrderCancelReplaceRequest.

**--on-quote-ack-reply-tags**: Used to re-define the default message sent by the server when sending a Quote Ack message.

**--on-quote-fill-reply-tags**: Used to re-define the default message sent by the server when sending a Quote Fill message.

**--multiple_nb_orders**: Used for sending multiple orders. This parameter specifies the number of orders to send.

**--multiple_interval**: Used for sending multiple orders. This parameter specifies the interval to wait in between orders. It is in seconds, so for example, set to 0.001 to wait 1 millisecond in between order

# Customizing your own server.
If the available customization with json files for the replies is not enough you can always create your own server!

You can inherit from the class [FixSessionServer](pyfixtest/fixsessionserver.py) and reimplement anything you need from it.
For example, let's say that you need to add tag 123 to the default ack message, but you prefer doing it with your own code instead of using the json files.

First, let's check what the pyfixtest-server.py code has. (We've ommited the parts for parsing and setting the command line arguments, but you can check 
the whole source code [here](scripts/pyfixtest-server.py))

```python
from pyfixtest import FixServer, FixSessionServer, ScriptUtils

server = FixServer(ip, port, fix, sender_comp_id, target_comp_id, outgoing_seq_num)
server.start()
```

It basically instantiates a FixServer object. 

By default, FixServer objects use the default session implementation FixSessionServer, but you can instruct the server to instantiate your own.

```python
from pyfixtest import FixServer, FixSessionServer, StdOutLogger # StdOutLogger is the default logger that just prints to stdout

class MySession(FixSessionServer):
  def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        FixSessionServer.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)

server = FixServer(ip, port, fix, sender_comp_id, target_comp_id, outgoing_seq_num)
server.set_fix_session_class(MySession) # In this line you're instructing the server to instantiate your own session
server.start()
```

With this, the server would instantiate your session, but it doesn nothing special, as it's not reimplementing anything nor adding any extra functionality.
Let's add tag 123 to the default ack method.

```python
from pyfixtest import FixServer, FixSessionServer, StdOutLogger # StdOutLogger is the default logger that just prints to stdout

class MySession(FixSessionServer):
  def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        FixSessionServer.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)

  def get_ack_message(self, order):
    ack = FixSessionServer.get_ack_message(self, order) # we just get the default ack message from the default session
    # and now we add the new tag
    ack.append_pair('123', 'SOME_VALUE')
    return ack # Don't forget to return the ack message!

server = FixServer(ip, port, fix, sender_comp_id, target_comp_id, outgoing_seq_num)
server.set_fix_session_class(MySession) # In this line you're instructing the server to instantiate your own session
server.start()
```

That's it. Now when the server sends an ack it will send the same values that the default session sends, plus 123=SOME_VALUE.

Now let's go a step further. 
We need to, instead of sending just the ack when a new order single is received, also send an extra message. Right before sending the ack.

Let's have a look to the default implementation of this in the default server session:
```python
    def on_new_order_single(self, order):
        ack = self.get_ack_message(order)
        self.send_message(ack)
        if self._expected_tag_value_for_fills != None and self._has_expected_tag_value(order, self._expected_tag_value_for_fills):
            fill = self.get_full_fill_message(order)
            self.send_message(fill)
            self._last_cl_order_id = None
        else:
            self._open_orders.append(order)
            self._last_cl_order_id = order.get(simplefix.TAG_CLORDID)
```
That's the default implementation for an new order single.
It just gets an ack_message (calling the same method we reimplemented in the previous example), and sends it.
After that... depending if the tag for fills is defined and if the value coming in the order for that tag matches the criteria, it gets a full fill message and sends it. 
In the case that the order is not filled it adds the order to a list of open orders (in order to be able to handle cancel and replace requests later).

Now, let's create our new message and send it right before sending the ack.
Let's say we have to send a special execution report message, with the following tags: 150=0|58=THIS_IS_AN_EXAMPLE|11=(ID FROM NEW ORDER SINGLE)

```python
from pyfixtest import FixServer, FixSessionServer, StdOutLogger # StdOutLogger is the default logger that just prints to stdout

class MySession(FixSessionServer):
  def __init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num=1, logger=StdOutLogger()):
        FixSessionServer.__init__(self, fix_version_string, sender_comp_id, target_comp_id, socket, outgoing_seq_num, logger)

  def get_ack_message(self, order):
    ack = FixSessionServer.get_ack_message(self, order) # we just get the default ack message from the default session
    # and now we add the new tag
    ack.append_pair('123', 'SOME_VALUE')
    return ack # Don't forget to return the ack message!

  def get_my_new_message(self, order):
    # This is implemented in the default session.
    # Returns a message with tags 8 and comp ids set according to the values you passed to the server.
    # In our case: fix='FIX.4.2'
    #              sender_comp_id = 'SENDER'
    #              target_comp_id = 'TARGET'
    msg = self.get_msg_header_message()  
    msg.append_pair('150', '0')
    msg.append_pair('58', 'THIS_IS_AN_EXAMPLE')
    # gets the value of tag 11 from the new order single
    msg.append_pair('11', order.get('11'))
    return msg

  def on_new_order_single(self, order):
      msg = self.get_my_new_message(order) # Constructs the new message
      self.send_message(msg)               # And sends it
      ack = self.get_ack_message(order)
      self.send_message(ack)
      if self._expected_tag_value_for_fills != None and self._has_expected_tag_value(order, self._expected_tag_value_for_fills):
          fill = self.get_full_fill_message(order)
          self.send_message(fill)
          self._last_cl_order_id = None
      else:
          self._open_orders.append(order)
          self._last_cl_order_id = order.get(simplefix.TAG_CLORDID)

ip='127.0.0.1'
port=1234
fix='FIX.4.2'
sender_comp_id = 'SENDER'
target_comp_id = 'TARGET'
outgoing_seq_num = 1
server = FixServer(ip, port, fix, sender_comp_id, target_comp_id, outgoing_seq_num)
server.set_fix_session_class(MySession) # In this line you're instructing the server to instantiate your own session
server.start()
```

The send_message method is located in the [FixSessionBase](pyfixtest/fixsessionbase.py), which is inherited by FixSessionServer.
That method adds tags 52 (SendingTime), 34 (SequenceNumber) and finally increments the session sequence number. 
Tags 9 and 10 are also automatically added. So you don't need to calculate the message lenght or the Checksum.


# Tests Automation

pyfixtest also offers a few utilities for running tests in the shape of test cases and test suites.
The idea is to automate tests and get restults so they can be recreated to offer a continuous integration tool.

Tests are based on 2 man classes:
* TestCase
* TestSuite

Both are located at the [testcase.py](pyfixtest/testcase.py) file.

## TestCase
TestCase defines an object subject to be tested.

Let's have a look to the __init__ method:
```python
def __init__(self, name, initial_message, expected_reply, initiate_test, session, logger=StdOutLogger(), timeout=2)
```
### Parameters explained
* **name:** It's the name of the test case. User defined (for example: test-logon-message)
* **initial_message:** it's the FixMessage that the test case will send to the session when the test is executed
* **expected_reply:** it's a list of messages or single FixMessage that the test case will expect and will verify. It's important to note that ONLY the fix tags described in the expected_reply will be checked. 
* **initiate_test:** True means the test case will initiate the test by sending the initial_message. There is the possibility to run a test case in passive mode. In that mode, the test case will just wait for a message to be received and will check that reply agains the expected one.
* **session:** Fix session in which the test case will execute. 
* **logger:** Logger for the session. As you can see it defaults to stdout logger.
* **timeout:** Number of seconds the test case will wait for a message from the session. If the timeout is reached and no message was received it will consider the test case as a failure. 

### TestCase example
Let's show a basic example now.
We need to send a FIX Logon message with the following values:
* Sender Comp ID = test_sender
* Target Comp ID = test_target
And the server will reply with the comp ids swapped plus an extra tag '1234' equal to 'SUCESS' to indicate the logon was successful.

The first thing that we need to do is create a FixSessionClient instance, that connects to a server session.
We have to do this using the socket module from python.

If the server is listening to port 1999, we'd do the following:
```python
client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
  client_sock.connect((self._gateway.getIp(), int(self._gateway.getPort())))
except:
  # CHANGE THIS TO WHATEVER YOU NEED TO DO IN CASE OF CONNECTION ERROR
  print('***** CONNECTION ERROR *****\n')
  sys.exit(2)
print('Connected')
session = FixSessionClient('FIX.4.2', 'test_sender', 'test_target', client_sock)
```

We have our session connected to the server. And we can start created test cases.
We can create the Logon message by adding all the required tags ourselves:
```python
logon = FixMessage()
# this is the simplefix way
# but we could do it by the tag name and tag values. 
# Just remember Values are strings or bytes (depending on the python version)
# in simplefix!!!
# logon.append_pair('35', 'A')
msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.2', header=True)
msg.append_pair(simplefix.TAG_SENDER_COMPID, 'test_sender', header=True)
msg.append_pair(simplefix.TAG_TARGET_COMPID, 'test_target', header=True)
logon.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON, header=True)
logon.append_pair(simplefix.TAG_ENCRYPTMETHOD, '0')
# we add this the tag-value way as an example
logon.append_pair('108', '30') # 108 is the hearbeat interval
```

This could be tedious, though, because we have to add tags manually and some of them are well known by the session.
Comp ids, begin string.... we already passed them to the session.
You can get a simple logon message by calling the **get_logon_message** already existing in the FixSessionBase class and pyfixtest will
automatically return a logon message with begin string, comp ids and the basic logon tags set for you.

Just checkout the session classes for getters like this already implemented.
```python
# Using pyfixtest get_logon_message
logon = session.get_logon_message()
```

Now we have the logon message. And we need to construct the reply we're expected to receive.
We're interested on a Logon message with tag 1234=SUCCESS.
Please note the session base class has a method to return a basic message, that fills begin string and comp ids.
But in the case of responses, the comp ids will be swapped. 
We can create a helper funtion to swap the comp ids and use the base session classes utilities to make our lifes easier.
```python

def swap_comp_ids(message):
  sender = message.get(simplefix.TAG_SENDER_COMPID)
  target = message.get(simplefix.TAG_TARGET_COMPID)
  message.remove(simplefix.TAG_SENDER_COMPID)
  message.remove(simplefix.TAG_TARGET_COMPID)
  message.append_pair(simplefix.TAG_SENDER_COMPID, target, header=True)
  message.append_pair(simplefix.TAG_TARGET_COMPID, sender, header=True)
  return message

logon_response = session.get_msg_header_message()
logon_reponse = swap_comp_ids(logon_reponse)
logon_response.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON, header=True)

# At this point we have a response that will check for the following values:
# 8=FIX.4.2
# 49=test_target
# 56=test_sender
# 35=A
# Let's add our special tag:
logon_response.append_pair('1234', 'SUCCESS')
```

And we're ready to go.

Let's combine all.
```python

def swap_comp_ids(message):
  sender = message.get(simplefix.TAG_SENDER_COMPID)
  target = message.get(simplefix.TAG_TARGET_COMPID)
  message.remove(simplefix.TAG_SENDER_COMPID)
  message.remove(simplefix.TAG_TARGET_COMPID)
  message.append_pair(simplefix.TAG_SENDER_COMPID, target, header=True)
  message.append_pair(simplefix.TAG_TARGET_COMPID, sender, header=True)
  return message

client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
  client_sock.connect(('127.0.0.1', 1234)) # Change this to fit the IP/port of your server!
except:
  # CHANGE THIS TO WHATEVER YOU NEED TO DO IN CASE OF CONNECTION ERROR
  print('***** CONNECTION ERROR *****\n')
  sys.exit(2)
print('Connected')
session = FixSessionClient('FIX.4.2', 'test_sender', 'test_target', client_sock)

# Using pyfixtest get_logon_message
logon = session.get_logon_message()

logon_response = session.get_msg_header_message()
logon_reponse = swap_comp_ids(logon_response)
logon_response.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON, header=True)

# At this point we have a response that will check for the following values:
# 8=FIX.4.2
# 49=test_target
# 56=test_sender
# 35=A
# Let's add our special tag:
logon_response.append_pair('1234', 'SUCCESS')

test_case_logon = TestCase('my-logon-test-case-expecting-1234-SUCCESS', logon, [logon_response], True, session)

# run will return True if test case was successful, False otherwise
test_case_logon.run()
```

### TestCase helpers to build messages
pyfixtest also offers a utilities class **TestCaseUtilities** located at [test_case.py](pyfixtest/test_case.py) to help create FixMessages from raw output.

```python
def parse_fix_message_from_raw(self, raw_message, convert_separator_to_soh, remove_tags_34_and_52=False)
```

Parameters are:

* **raw_message:** The raw FIX message
* **convert_separator_to_soh:** To convert the human readable format separator (normally character '|') to SOH byte (byte 0x01)
* **remove_tags_34_and_52:** To remove tags 34 and 52 as we're going to reuse the messages and the sequence number and sending time of our new message will change.

Example:
```python
utils = TestCaseUtils()
raw_msg = b'8=FIX.4.2|9=55|35=A|49=Target|56=Sender|34=2|52=20181008-14:20:51.740|10=170|'
logon_msg = utils.parse_fix_message_from_raw(raw_msg, True, True)
```

That code will return a FixMessage with the tags described in the raw_msg. And will change the '|' to byte 0x01 and will remove tags 34 and 52.
That way we can reuse and build test cases from FIX logs, for example. 

For example: you could write your our python program that reads the FIX output from previous executions and create a list of test-cases from it. 
On the client side, pyfixtest prints RECV and SENT as the preffix for received and sent messages. So you could parse SENT messages to create the test-case initial message and use the RECV messages to build the expected ones.

### A TestCase expecting more than one message as reply.
It is pretty comon that when we send a new order single message in FIX the server replies with an ack message followed by another execution report (cancelling the order or filling it partially or in full).
That's why a TestCase is able to expect more than 1 message and check them all.
```python
# Test Fill
utils = TestCaseUtils()
new_order_single = utils.parse_fix_message_from_raw('8=FIX.4.2|9=312|35=D|49=DUMMY_TG_XAVI|56=DUMMY_CL_XAVI|34=32|52=20211203-13:44:54.205|11=20211203-13:44:54.205607|1=FILL|48=H5E=GBX|40=P|21=1|47=D|18=M|44=35.35|1724=5|110=1000|581=P|22=5|38=3000|54=2|59=4|100=BLOXD|10448=100000046|10450=100000050|10109=002858099|6|10=198|', True, True)

ack = utils.parse_fix_message_from_raw('8=FIX.4.2|9=291|35=8|34=34|52=20211203-13:44:54.209150|49=DUMMY_CL_XAVI|56=DUMMY_TG_XAVI|20=0|22=5|39=0|40=P|54=2|59=4|150=0|14=0|44=35.35|60=20211203-13:44:54.208825|11=20211203-13:44:54.205607|17=MY_17|48=H5EG.DE|37=tag37-35|55=H5EG.DE|100=BLOXD|198=1263000000000', True, True)
fill = utils.parse_fix_message_from_raw('8=FIX.4.2|9=328|35=8|34=35|52=20211203-13:44:54.210468|49=DUMMY_CL_XAVI|56=DUMMY_TG_XAVI|20=0|22=5|39=2|40=P|54=2|59=4|150=2|145.35|44=35.35|60=20211203-13:44:54.210072|1=FILL|11=20211203-13:44:54.205607|17=tag17-36|48=H5EG.DE|30=BLOXD|37=tag37-36|55=H5EG.DE|100=BL031=246|76=BLOXD|10=182|', True, True)

# REMEMBER THAT THE TEST CASE ONLY CHECKS THE EXISTING TAGS.
# IF, FOR EXAMPLE, YOU'RE ONLY INTERESTING TO CHECKING THAT THE FILL HAS 150=2 AND YOU DON'T CARE ABOUT THE REST...
# JUST BUILD A SIMPLE MESSAGE WITH TAG 150 ADDED.

# Construct the TestCase expecing an ack first and then a fill
test_case_fill = TestCase('my-logon-test-fill', new_order_single, [ack, fill], True, session)
```

## TestCaseServerEcho

TestCaseServerEcho is a special TestCase that is useful to test if the message received by the server is what we expected to send.
Let's say you're sending a NewOrderSingle message to the server side and you want to ensure that a special tag is sent with the expected value and automate a test for checking that.

The FixSessionServer in pyfixtest has a test socket that simply echoes what it received. 
That way, when the server receives the NewOrderSingle message it will echo the received message over that test socket and the client (the test case in this case) will be able to read it and verify if that special tag was sent as expected.

The test socket is disabled by default, but you can enable it in you our implementation or in the default one.
This code shows how to enable it in the default [pyfixtest-server.py](scripts/pyfixtest-server.py)
```python
# last 2 lines of scripts/pyfixtest-server.py
server.set_create_test_connection(True)
server.start()
```

After enablin the test socket you will see an extra line for every message received in the server's console.
```bash
TEST SOCKET SENT: 8=FIX.4.2|9=60|35=A|49=TARGET|56=SENDER|34=1|52=20211207-10:46:57.053|98=0|10=070|
RECV: A    8=FIX.4.2|9=60|35=A|49=TARGET|56=SENDER|34=1|52=20211207-10:46:57.053|98=0|10=070|
SENT: A    8=FIX.4.2|9=60|35=A|49=SENDER|56=TARGET|34=1|52=20211207-10:46:57.055|98=0|10=072|

TEST SOCKET SENT: 8=FIX.4.2|9=174|35=D|49=TARGET|56=SENDER|34=2|52=20211207-10:47:00.525|1=test_1|11=20211207-10:47:00.524952|21=1|54=1|44=1999|38=1000|45=VOD.L|55=VOD.L|40=2|59=3|60=20211207-10:47:00.524995|10=140|
RECV: D    8=FIX.4.2|9=174|35=D|49=TARGET|56=SENDER|34=2|52=20211207-10:47:00.525|1=test_1|11=20211207-10:47:00.524952|21=1|54=1|44=1999|38=1000|45=VOD.L|55=VOD.L|40=2|59=3|60=20211207-10:47:00.524995|10=140|
SENT: 8    8=FIX.4.2|9=153|35=8|49=SENDER|56=TARGET|34=2|52=20211207-10:47:00.525|37=tag37-2|17=tag17-2|20=0|55=VOD.L|54=1|11=20211207-10:47:00.524952|150=0|39=0|151=1000|14=0|6=0|10=228|
```

As you can see the TEST SOCKET SENT and RECV lines are the same. The server is simply echoing the received message.

**The test socket is a UDP socket, using the same port you passed to the server when you instantiated it.**


The initial message in TestCaseServerEcho is the message that the test case will send to the server.
The expected message is the message that we expect to receive from the test socket ie. the message that we sent echoed by the server.
```python
# we're expecting the server to receive 1234=TEST
expected = FIXMessage()
expected.append_pair('1234', 'TEST')

# server port is the port the server is listening to
test_case = TestCaseServerEcho('echo-test', message_initial, [expected], server_port, client_session)
test_case.run()
```

## TestSuite
The test suite class it's just a list of test cases that will be executed in the same order they were added to the suite.
You can, for example, also exclude tests (by their name) from a TestSuite.

Test suits have also a name so they can be identified.

```python
suite = TestSuite('Test-Logon-and-Fill')
suite.add_test(test_case_logon)
suite.add_test(test_case_fill)
suite.run()

# For running excluding the test_case_fill TestCase...
suite.run(exclude=['my-logon-test-fill'])
```

TestSuite will return True if all the tests were successfull, False otherwise.

## Full running example with HTML Logger

pyfixtest also offer a HTML logger that creates HTML reports for test suites.

TestSuite and TestCases classes add the messages send/received along with the result in the HTML while they are executed.

You can see a working example running a TestSuite that generates a HTML report [here](examples/test-suite.py)

Code is as follows:
```python
import simplefix
import socket
import sys
from pyfixtest import TestCase, TestCaseUtils, TestSuite, FixSessionClient, HtmlLogger

def swap_comp_ids(message):
  sender = message.get(simplefix.TAG_SENDER_COMPID)
  target = message.get(simplefix.TAG_TARGET_COMPID)
  message.remove(simplefix.TAG_SENDER_COMPID)
  message.remove(simplefix.TAG_TARGET_COMPID)
  message.append_pair(simplefix.TAG_SENDER_COMPID, target, header=True)
  message.append_pair(simplefix.TAG_TARGET_COMPID, sender, header=True)
  return message


# Create a HTML logger
# Important the Third parameter is the definition of how the report should be generated.
# But pyfixtest is not using it. 
# It's a json file describing the tags to be shown in the report and used for other tools that use
# pyfixtest as their base engine.
# For this example, we just pass a basic definition (provided with the example)
logger = HtmlLogger('./test.log', './test.html', 'examples/html_logger_definition.json')

# Connect to the server
client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
  client_sock.connect(('127.0.0.1', 1234))
except:
  # CHANGE THIS TO WHATEVER YOU NEED TO DO IN CASE OF CONNECTION ERROR
  print('***** CONNECTION ERROR *****\n')
  sys.exit(2)
print('Connected')

# Create the client session, passing the socket
session = FixSessionClient('FIX.4.2', 'TARGET', 'SENDER', client_sock)

# Using pyfixtest get_logon_message
logon = session.get_logon_message()

logon_response = session.get_msg_header_message()
logon_reponse = swap_comp_ids(logon_response)
logon_response.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON, header=True)

# Create the TestCase... passing the logger (HTMLLogger), otherwise defaults to StdOutLogger
test_case_logon = TestCase('logon', logon, [logon_response], True, session, logger)

# We use TestCaseUtils to parse raw fix messages
utils = TestCaseUtils()

# Create new order single test case
new_order_1_raw = b'8=FIX.4.2|9=173|35=D|49=TARGET|56=SENDER|34=2|52=20211209-09:39:49.947|1=test_1|11=20211209-09:39:49.947410|21=1|54=1|44=200|38=1000|45=VOD.L|55=VOD.L|40=2|59=3|60=20211209-09:39:49.947453|10=141|'
new_order_1 = utils.parse_fix_message_from_raw(new_order_1_raw, True, True)

# We're just going to check for a few tags
# 150=0
# 39=0
# 11=order's 11
# 54=order's 54
ack_1 = simplefix.FixMessage()
ack_1.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_EXECUTION_REPORT)
ack_1.append_pair('150', '0')
ack_1.append_pair('39', '4') # This will report an error!!
ack_1.append_pair('11', new_order_1.get('11'))
ack_1.append_pair('54', new_order_1.get('54'))


# Create the TestCase... passing the logger (HTMLLogger), otherwise defaults to StdOutLogger
test_case_new_order_ack = TestCase('new-order-test', new_order_1, [ack_1], True, session, logger)

# Create the TestSuite (Passing the HTMLLogger instance)
suite = TestSuite('test-suite-example', logger)
suite.add_test(test_case_logon)
suite.add_test(test_case_new_order_ack)
suite.run()
```

In order to run the TestSuite do the following in your terminal:
```bash
$ cd pyfixtest
$ export PYTHONPATH=`pwd`:`pwd`/examples
$ python examples/test-suite.py 
```

It will show the result in the console, and will also create the test.log and test.html files.
```bash
********************************
 TEST SUITE  test-suite-example
********************************

--------------------------------
 TEST logon
--------------------------------

SENT: A    8=FIX.4.2|9=60|35=A|49=TARGET|56=SENDER|34=1|52=20211209-10:59:57.238|98=0|10=081|
RECV: A    8=FIX.4.2|9=60|35=A|49=SENDER|56=TARGET|34=1|52=20211209-10:59:57.239|98=0|10=082|
-------------------------------- OK
--------------------------------
 TEST new-order-test
--------------------------------

SENT: D    8=FIX.4.2|9=173|35=D|34=2|52=20211209-10:59:57.240|49=TARGET|56=SENDER|1=test_1|11=20211209-09:39:49.947410|21=1|54=1|44=200|38=1000|45=VOD.L|55=VOD.L|40=2|59=3|60=20211209-09:39:49.947453|10=120|
RECV: 8    8=FIX.4.2|9=153|35=8|49=SENDER|56=TARGET|34=2|52=20211209-10:59:57.240|37=tag37-2|17=tag17-2|20=0|55=VOD.L|54=1|11=20211209-09:39:49.947410|150=0|39=0|151=1000|14=0|6=0|10=005|
TEST --new-order-test-- *** ERROR *** value mismatch for tag 39: expected value [4], actual is [0]
-------------------------------- ERROR
Tests OK       (1/2)
Tests FAILED   (1/2)
Tests EXCLUDED (0/2)
********************************
```

The HTML generate looks like this:


![HTMLReport](examples/html-report.png)


The TestCase and TestSuite classes call the following functions from HTMLLogger

- **add_suite_header**: Adds the HTML header with the name of the TestSuite
- **add_suite_footer**: Adds the HTML footer. Basically ends the HTML tags that add_suite_header left open
- **add_test_name**: Adds the HTML tags for a TestCase name
- **add_ok_result**: Adds the HTML tags for a successful TestCase
- **add_error_result**: Adds the HTML tags for TestCase with mismatches
- **begin_test_log**: Adds the HTML tags for beginning a new TestCase report
- **end_test_log**: Ends the HTML tags that begin_test_log left open
- **add_sent_message**: Adds the message sent by the TestCase
- **add_received_message**: Adds a message received by the TestCase
- **add_error_message**: Adds the error message if the TestCase was not successful
- **add_test_messages**: Called passing the initial and expected messages. The default implementation does nothing but it's called to offer more flixibility to write your own logger

You can inspect the default implementation of the HTMLLogger [here](pyfixtest/logger.py).

If the functionality offered is not fitting your needs you can create your own inheriting from the default one and reimplementing any of the methods called by the TestCase or TestSuite classes.

The test-orders project, for example, reimplements the default logger from pyfixtest and uses the html report definition file. 

You can have a look to that as an example.