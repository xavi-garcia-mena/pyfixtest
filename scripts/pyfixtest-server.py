#! /usr/bin/env python
########################################################################
# pyfixtest
# Copyright (C) 2018, Xavi Garcia <xavi.garcia@neueda.com>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
########################################################################
from pyfixtest import FixServer, FixSessionServer, ScriptUtils

script_utils = ScriptUtils('pyfixtest-server.py')
ip, port, fix, sender_comp_id, target_comp_id, outgoing_seq_num = script_utils.parse_options()

server = FixServer(ip, port, fix, sender_comp_id, target_comp_id, outgoing_seq_num)
if script_utils.highlight != None:
    server.set_highlighted_tags(script_utils.highlight)
if script_utils.tag_value_for_fills != None:
    server.set_expected_tag_in_new_orders_for_fills(script_utils.tag_value_for_fills[0], script_utils.tag_value_for_fills[1])
if script_utils.on_ack_reply_tags != None:
    server.set_on_ack_reply_tags(script_utils.on_ack_reply_tags, script_utils.on_ack_reply_tags_enhance_default)
if script_utils.on_fill_reply_tags != None:
    server.set_on_fill_reply_tags(script_utils.on_fill_reply_tags, script_utils.on_fill_reply_tags_enhance_default)
if script_utils.on_cancel_reply_tags != None:
    server.set_on_cancel_reply_tags(script_utils.on_cancel_reply_tags, script_utils.on_cancel_reply_tags_enhance_default)
if script_utils.on_replace_reply_tags != None:
    server.set_on_replace_reply_tags(script_utils.on_replace_reply_tags, script_utils.on_replace_reply_tags_enhance_default)
if script_utils.on_quote_ack_reply_tags != None:
    server.set_on_quote_ack_reply_tags(script_utils.on_quote_ack_reply_tags, script_utils.on_quote_ack_reply_tags_enhance_default)
if script_utils.on_quote_fill_reply_tags != None:
    server.set_on_quote_fill_reply_tags(script_utils.on_quote_fill_reply_tags, script_utils.on_quote_fill_reply_tags_enhance_default)
if script_utils.quote_side != None:
    server.set_quote_fill_only_side(script_utils.quote_side)
server.start()
